package com.niz;

import java.util.Iterator;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.BinaryHeap;
import com.badlogic.gdx.utils.IntArray;
import com.badlogic.gdx.utils.IntMap.Entry;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.Pool;
import com.badlogic.gdx.utils.Pools;
import com.niz.components.Components;
import com.niz.components.DurationComponent;
import com.niz.graphs.Graph;

public class Entity implements Json.Serializable{
	private static final int TIMED_COUNT = 256;

	private static final String TAG = "Entity";

	public Bus bus = new Bus(this);
	
	public static IntPool intPool = new IntPool();
	public static Pool<PeriodicUpdateNode> periodicPool = Pools.get(PeriodicUpdateNode.class);
	
	//public ObjectMap<Component, Component> components = new ObjectMap<Component, Component>();
	public BinaryHeap<PeriodicUpdateNode> periodicComponents = new BinaryHeap<PeriodicUpdateNode>();
	public ObjectMap<Component, DurationComponent> durations = new ObjectMap<Component, DurationComponent>();
	
	
	
	
	private ObjectMap<Component, Graph> dataGraph = new ObjectMap<Component, Graph>();
	
	public Array<Draw> draw = new Array<Draw>(true, 1);//stack for draw objects
	public int tick, stateTick;
	public int id;
	public Plane plane;
	public State state;
	
	private int currentTickGroupBits;
	/*public void draw(){
		//for (int i = 0; i < draw.size; i++){
		//	draw.get(i).draw(plane);
		//}
		if (draw.size < 1){
		
			return;
		}
		draw.get(0).draw(plane,  this);
	}*/
	
	public void tick(){
		/*Iterator<Component> iter = subscribers[SUB_TICK].iterator();
		while (iter.hasNext()){
			Component c = iter.next();
			c.tick(this);
		}*/
		
		//periodic
		PeriodicUpdateNode n = (PeriodicUpdateNode) periodicComponents.peek();
		while (n.getValue() < tick){
			PeriodicUpdate c = n.update;
			int t = c.updatePeriodic(this, n.data);
			
			periodicComponents.pop();
			
			if (t == PeriodicUpdate.END_CODE){
				periodicPool.free(n);
			} else {
				n.setValue(t+c.getPeriodicInterval(n.data));
				periodicComponents.add(n);
			}
			
			
			
			n = (PeriodicUpdateNode) periodicComponents.peek();
		}
	}


	public void addComponent(Component c, IntArray values){
		bus.subscribe(c, values);		
	}
	/*private void setDuration(Component c, int duration){
		addComponent(Components.cDuration, duration);
	}*/
	/*private void addComponent(Component c, int... ints) {
		IntArray data = bus.dataPool.obtain();
		data.clear();
		data.addAll(ints);
		bus.subscribe(c, data);
		//addComponent(c, data);
	}*/

	/*public BusData addComponent(Component c){
		//components.put(c, c);
		/*BusData data = bus.subscribe(c);
		if (c instanceof PeriodicUpdate){
			addComponentPeriodic((PeriodicUpdate) c);
		}
		return data;
	}*/
	public void addComponentPeriodic(PeriodicUpdate pu){
		PeriodicUpdateNode n = periodicPool.obtain();
		n.setValue(pu.getPeriodicInterval(n.data)+tick);
		n.update = pu;
		//n.data.add
		periodicComponents.add(n);
		//periodicComponents.put(c, pu);
	}
	/*public void removeComponent(Component c){
		if (components.containsKey(c)){
			components.remove(c);
			bus.unsubscribe(c);
		}
		//periodicUpdates remove themselves while ticking. 
	}*/

/*	public StateFrameInfo getStateFrameInfo(){
		return draw.get(0).model.getStateFrameInfo();
			
		
	}
	public LayerInfo getLayerInfo(){
		
	}*/

	/*public void changeState(State s) {
		this.state = s;
	}
	private int stateData = 0;//new IntArray();

	
	public int getStateData() {
		return stateData;
		
	}
	public void setStateData(int data) {
		stateData = data;
		
	}*/

	@Override
	public void read(Json json, JsonValue jsonData) {
		for (JsonValue entry = jsonData.child(); entry != null; entry = entry.next()){
			String name = jsonData.child().name();
			Component c = Components.get(name);;
			String vals = jsonData.child.asString();
			
			
			
			IntArray data = intPool.obtain();
			c.valuesFrom(vals, data);//json.readValue(IntArray.class, jsonData);
			IntArray values = data;
			
			//setIntData(c, values);
			addComponent(c, values);
		}
		
		
	}
	
	
	

	@Override
	public void write(Json json) {
		Iterator<Entry<BusData>> iter = bus.components.entries().iterator();
		while (iter.hasNext()){
			Entry<BusData> e = iter.next();
			String s = e.value.dataToString();
			json.writeValue(e.value.c.name, s);
		}
		
	}


	/*public void addTickGroupBits(int tickBits) {
		
		if (tickBits <0 ) return;
		
		int oldBits = currentTickGroupBits;
		currentTickGroupBits |= (1<<tickGroup);
		Gdx.app.log(TAG, "add tick"+tickGroup);// + (0&2)+" g"+currentTickGroupBits +( 0 &(1<<tickGroup)));
		if (currentTickGroupBits != oldBits){
			
			SynthEngine.producer.addEntity(this, tickGroup);
		}
	}*/
	
	public void addTickGroupBits(int bits){
		setTickGroup(currentTickGroupBits | bits);
	}


	public void setTickGroup(int newBits) {
		if (newBits <0 ) return;
		Gdx.app.log(TAG, "set tick"+newBits);
		if (newBits != currentTickGroupBits){
			int xor = newBits ^ currentTickGroupBits, bit = 0;
			while (xor > 0 && bit < 32){
				if ((xor & 1) == 1){
					if ((currentTickGroupBits & (1<<(bit))) == 0){
						SynthEngine.producer.addEntity(this, bit+1);
						Gdx.app.log(TAG, "add");
					} else {
						SynthEngine.producer.removeEntity(this, bit);
						Gdx.app.log(TAG, "remove");
					}
				}
				xor >>= 1;
				bit++;
			}
		}
		currentTickGroupBits = newBits;
	}
	
}
