package com.niz.threading;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.BinaryHeap;
import com.badlogic.gdx.utils.IntMap;
import com.badlogic.gdx.utils.IntMap.Values;
import com.niz.Entity;
import com.niz.EntityMessage;
import com.niz.Messages;
import com.niz.UIBatch;
import com.niz.decals.DecalBatch;

public class Producer {
/*
 * manages lists(groups) of entities, wakes up/manages threads
 * 
 * 
 * 
 * 
 */
	
	public static final int GROUP_PHYSICS = 1;
	public static final int GROUP_TICK = 2;
	public static final int GROUP_GRAPHS = 4;
	public static final int GROUP_BUCKETS = 3
			, GROUP_DRAW_UI = 5;
	private static final String TAG = "producer";;
	Array<Array<IntMap<Entity>>> groupEntities = new Array<Array<IntMap<Entity>>>();
	Array<Array<Values<Entity>>> groupValues = new Array<Array<Values<Entity>>>();
	private int numberOfThreads;
	private int[][] threadTotals;
	//private int[] groupTotals;
	int[] nextThread;
	int pausedTotal=0;
	private TickThread[] thread;
	public static int QUEUE_MAX = 10;
	//EntityMessage[] queue = new EntityMessage[QUEUE_MAX];
	Array<Array<Processable>> tickProcessables = new Array<Array<Processable>>();//indexed by order
	private int tickProgress, tickMax = 0, tickProgressFine = 0;
	private int timeTick, maxTimeTick;
	//private boolean[] pause = new boolean[QUEUE_MAX];
	private Array<IntMap<Entity>> allGroupEntities = new Array<IntMap<Entity>>();
	private Array<Values<Entity>> allGroupValues = new Array<Values<Entity>>();;
	private IdleProcessor[] idleProcessors;
	/*public void addToQueue(EntityMessage m, boolean pause){
		queue[size] = m;
		this.pause[size] = pause;
		size++;
	}*/
	public void addToQueue(EntityMessage m, int group){
		Array<Processable> a = new Array<Processable>();
		for (int i = 0; i < numberOfThreads; i++){
			GroupMessageProcessor p = new GroupMessageProcessor();
			Values<Entity> vals = groupValues.get(group).get(i);
			p.set(vals, m);
			
			a.add(p);
		}
		tickProcessables.add(a);
		tickMax++;
	}
	public void init(int numberOfGroups, int numberOfThreads, UIBatch uiBatch){
		idleProcessors = new IdleProcessor[numberOfThreads];
		this.numberOfThreads = numberOfThreads;
		for (int i = 0; i < numberOfGroups; i++){
			groupEntities.add(new Array<IntMap<Entity>>());
			groupValues.add(new Array<Values<Entity>>());
			
			allGroupEntities.add(new IntMap<Entity>());
			allGroupValues.add(new Values<Entity>(allGroupEntities.peek()));
			Gdx.app.log(TAG, "va;"+allGroupEntities.size);
			for (int t = 0; t < numberOfThreads; t++){
				IntMap<Entity> map = new IntMap<Entity>();
				Values<Entity> vals = new Values<Entity>(map);
				groupEntities.get(i).add(map);
				groupValues.get(i).add(vals);
			}
		}
		
		threadTotals = new int[numberOfGroups][numberOfThreads];
		nextThread = new int[numberOfGroups];
		thread = new TickThread[numberOfThreads];
		for (int i = 0; i < numberOfThreads; i++){
			DelegatingRunnable run = new DelegatingRunnable(this);
			thread[i] = new TickThread(run, i);
			run.setThread(thread[i]);
			idleProcessors[i] = new IdleProcessor();
			run.set(idleProcessors[i]);
			thread[i].start();
			//thread[i].onPause();
			//pausedTotal++;
			
		}
		Gdx.app.log(TAG, "init"+allGroupEntities.size);
		//groupTotals = new int[numberOfGroups];
		queueEntityMessages(uiBatch);
	}
	
	
	
	private void queueEntityMessages(UIBatch uiBatch) {
		
		addToQueue(Messages.physicsPre.obtain(), GROUP_PHYSICS);
		addToQueue(Messages.collide.obtain(), GROUP_PHYSICS);
		addToQueue(Messages.tick.obtain(), GROUP_TICK);
		addToQueue(Messages.graphTick.obtain(), GROUP_GRAPHS);
		addToQueue(Messages.physicsPost.obtain(), GROUP_PHYSICS);
		addToQueue(Messages.buckets.obtain(), GROUP_BUCKETS);
		
		this.addDrawProcess(new UIRenderer(), Messages.drawUI.set(uiBatch), GROUP_DRAW_UI);
	}
	public void nextFrame(){//pauses all threads and sets flag so they
		//Gdx.app.log(TAG, "next frame"+tickProgress);
		maxTimeTick++;
		wakeUpThreads();
	}


	private void wakeUpThreads() {
		for (int i = 0; i < numberOfThreads; i++){
			if (thread[i].isPaused()) thread[i].onResume();
		}
		
	}
	private Values<Entity> getValues(int group, int threadID){
		return groupValues.get(group).get(threadID);
	}
	
	public void addEntity(Entity e, int group){
		Gdx.app.log(TAG, "add e"+e.id+","+group);
		int thread = nextThread[group];
		threadTotals[group][thread]++;
		/*nextThread++;
		nextThread%=numberOfThreads;
		if (threadTotals[group][thread]<threadTotals[group][nextThread])
			nextThread = thread;*/
		int lowest = Integer.MAX_VALUE, lowIndex = 0;
		for (int i = 0; i < numberOfThreads; i++){
			if (threadTotals[group][i] < lowest){
				lowest = threadTotals[group][i];
				lowIndex = i;
			}
		}
		nextThread[group] = lowIndex;
		
		
		groupEntities.get(group).get(thread).put(e.id, e);
		allGroupEntities.get(group).put(e.id, e);
	}
	
	public void removeEntity(Entity e, int group){
		Gdx.app.log(TAG, "remove e"+e.id+","+group);
		allGroupEntities.get(group).remove(e.id);
		for (int i = 0; i < numberOfThreads; i++){
			if (groupEntities.get(group).get(i).containsKey(e.id)){
				groupEntities.get(group).get(i).remove(e.id);
				return;
			}
		
		}
	}

	public synchronized void pauseThread(int threadID) {
		thread[threadID].onPause();
		if (++pausedTotal == numberOfThreads){
			for (int i = 0; i < numberOfThreads; i++){
				thread[i].onResume();
			}
		}
	}
	Array<DrawProcessable> drawProcesses = new Array<DrawProcessable>();
	
	public void draw(Camera camera, DecalBatch batch, UIBatch uiBatch){
		
		for (int i = 0; i < drawProcesses.size; i++){
			DrawProcessable p = drawProcesses.get(i);
			Gdx.app.log(TAG, "draw");
			p.pre(camera, batch, null);
			p.process();
			p.post(camera, batch, uiBatch);
		}
	}

	public void addDrawProcess(DrawProcessable p, EntityMessage mess, int group){
		//DrawProcessable p = new DrawProcessable();
		
		p.set(allGroupValues.get(group), mess);
		drawProcesses.add(p);
	}
	
	public synchronized void groupMessageFinished(TickThread thread) {
		//Gdx.app.log(TAG, "group message finished"+tickProgress);
		//either give it new Entities or pause/resume
		if (tickProgress < tickProcessables.size &&
				tickProgressFine < tickProcessables.get(tickProgress).size){//if there are more p's in the queue, resuse this thread
			Processable p = tickProcessables.get(tickProgress).get(tickProgressFine); 
			thread.run.set(p);
			tickProgressFine++;
		//	Gdx.app.log(TAG, "grmsg reuse");
			return;
		}
		
		if (tickProgress >= tickMax){//done ticking, set p to chunk or whatever processable
			//or loop if another tick needs to happen
			if (timeTick < maxTimeTick){//start the next tick
				timeTick++;
				tickProgress = 0;
				tickProgressFine = 0;
				//Gdx.app.log(TAG, "grmsg retick");
			} else {
				//Gdx.app.log(TAG, "grmsg idle");
				thread.run.set(getNextProcess(thread.threadID));
			}
			
		}
		
		//otherwise it will pause the thread/tick them over
		synchronized(this){
			thread.onPause();
			//Gdx.app.log(TAG, "grmsg pause"+pausedTotal);
			if (++pausedTotal == numberOfThreads){//reset
				
				//Gdx.app.log(TAG, "grmsg unpause/reset"+pausedTotal);
				pausedTotal = 0;
				tickProgress++;
				tickProgressFine = 0;
				for (int i = 0; i < numberOfThreads; i++){
					//Processable p = tickProcessables.get(tickProgress).get(tickProgressFine); 
					this.thread[i].run.set(getNextProcess(thread.threadID));
					this.thread[i].onResume();
					
					
				}
			}
		}
	}
	
	BinaryHeap<ProcessableNode> heap = new BinaryHeap<ProcessableNode>();
	public synchronized Processable getNextProcess(int threadID) {
		//Gdx.app.log(TAG, "next");
		if (tickProgress < tickProcessables.size &&
				tickProgressFine < tickProcessables.get(tickProgress).size){//if there are more p's in the queue, resuse this thread
			Processable p = tickProcessables.get(tickProgress).get(tickProgressFine); 
			//thread.run.set(p);
			tickProgressFine++;
			
			return p;
		}
		if (timeTick < maxTimeTick){
			tickProgress = 0;
			tickProgressFine = 0;
			timeTick++;
		}
		
		
		
		
		if (heap.size == 0) return idleProcessors[threadID];
		Processable p = heap.pop().p;
		return p;
	}
	
	
	public void addAsset(Processable p) {
		heap.add(ProcessableNode.obtain().set(p));
		
	}
	
	
	
	
}
