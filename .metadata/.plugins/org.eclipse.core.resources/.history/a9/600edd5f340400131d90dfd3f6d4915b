package com.niz;



import java.util.Iterator;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.IntArray;
import com.badlogic.gdx.utils.IntMap;
import com.badlogic.gdx.utils.IntMap.Entries;
import com.badlogic.gdx.utils.IntMap.Entry;
import com.badlogic.gdx.utils.IntMap.Values;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.Pool;
import com.badlogic.gdx.utils.Pools;
import com.badlogic.gdx.utils.reflect.ClassReflection;
import com.badlogic.gdx.utils.reflect.Method;


public class Bus {
private static final int OVERRIDE_MAX = 32;
private static final String TAG = "bus";
private ObjectMap<Class<?>, Array<BusMethod>> map = new ObjectMap<Class<?>, Array<BusMethod>>();
ObjectMap.Entries<Class<?>, Array<BusMethod>> mapEntries = new ObjectMap.Entries<Class<?>, Array<BusMethod>>(map);
ObjectMap.Values<Array<BusMethod>> mapValues = new ObjectMap.Values<Array<BusMethod>>(map);
//

public IntMap<BusData> components = new IntMap<BusData> ();
public Values<BusData> componentValues = new Values<BusData>(components);
public Pool<BusData> busDataPool = Pools.get(BusData.class);
public Pool<Array<BusData>> busComponentArrayPool = new BAP();
public class BAP extends Pool<Array<BusData>>{
	@Override
	protected Array<BusData> newObject() {
		return new Array<BusData>();
	}
}
private int nextHash;
private IntMap<Object> removeQ = new IntMap<Object>();
Entries<Object> removeEntries = new Entries<Object>(removeQ);

private BusData[] overrideData = new BusData[OVERRIDE_MAX]; 
private IntMap<Array<BusData>> overridden = new IntMap<Array<BusData>>();
Entries<Array<BusData>> overriddenEntries = new Entries<Array<BusData>>(overridden);;
private int overrideBits=0;

//public Array<Component> overriddenComponents = new Array<Component>();
//public Array<IntArray> overriddenData = new Array<IntArray>();
//link components to intarrays
//private ObjectMap<Object, Array<IntArray>> dataInt = new ObjectMap<Object,  Array<IntArray>>();
	private Entity parent;
	public Bus(Entity e){
		parent = e;
	}

	public void publish(BaseMessage message){
		Class<?> cl = message.getClass();
		Gdx.app.log(TAG, "publishing "+cl);
		Array<BusMethod> m = map.get(cl);
		if (m == null) return;
		for (int i = 0; i < m.size; i++){
			m.get(i).call(message);
		}
		
	}
	
	/*public void unsubscribe(Component c){
		Method[] m = ClassReflection.getDeclaredMethods(c.getClass());//c.getClass().getDeclaredMethods();
		for (int i = 0; i < m.length; i++){
			Class<?>[] params = m[i].getParameterTypes();
			if (params.length == 1){
				if (params[0].isInstance(BaseMessage.class)){
					remove(params[0], m[i], c);
				}
			}
		}
	}*/
	public int subscribe(Component c){
		
		return subscribe(c, dataPool.obtain());
	}
	//public static BaseMessage baseMessage = new BaseMessage("com.niz.Bus.BaseMessage");
	public int subscribe(Component c, IntArray data){
		
		int hash = nextHash++;
		BusData bd = busDataPool.obtain().set(c, data, hash);
		if (c.overrideIndex != 0){
			if (overrideData[c.overrideIndex]== null ){
				
				overrideData[c.overrideIndex] = bd;
			} else if (overrideData[c.overrideIndex].c.priority > c.priority) {//
				override(bd);
				return hash;
			} else {//override
				override(overrideData[c.overrideIndex]);
				remove(overrideData[c.overrideIndex].hash);
				
				overrideData[c.overrideIndex] = bd;
			}
		}
		//Method[] m = ClassReflection.getDeclaredMethods(c.getClass());
		
		//c.getClass().getDeclaredMethods();
		/*for (int i = 0; i < m.length; i++){
			Class<?>[] params = m[i].getParameterTypes();
			if (params.length == 2){
				if (params[0].isInstance(BaseMessage.class) && params[1].isInstance(BusData.class)){
					add(params[0], m[i], bc);
				}
			}
		}*/
		for (int i = 0; i < c.busMethods.size; i++){
			add(c.busMethods.get(i), bd);
		}
		
		components.put(hash, bd);
		parent.addTickGroupBits(c.tickGroupBits);
		addSibling(bd);
		return hash;
	}
	
	

	private void override(BusData bc) {
		//look for array first
		if (overridden.get(bc.c.overrideIndex) == null){
			overridden.put(bc.c.overrideIndex, busComponentArrayPool.obtain());
		}
		overridden.get(bc.c.overrideIndex).add(bc);
		
	}

	public void remove(int hash) {
		removeQ.put(hash, null);
	}
	public void removeByName(String name){
		Iterator<Array<BusMethod>> i = map.values().iterator();
		while (i.hasNext()){
			Array<BusMethod> a = i.next();
			Iterator<BusMethod> ia = a.iterator();
			while (ia.hasNext()){
				BusMethod busItem = ia.next();
				if (busItem.bd.c.name.equals(name)){
					remove(busItem.bd.hash);
				}
			}
		}
	
	}
	public void removeByTag(String... name){
		Iterator<Array<BusMethod>> i = map.values().iterator();
		while (i.hasNext()){
			Array<BusMethod> a = i.next();
			Iterator<BusMethod> ia = a.iterator();
			while (ia.hasNext()){
				BusMethod busItem = ia.next();
				if (busItem.bd.c.name.equals(name)){
					remove(busItem.bd.hash);
				}
			}
		}
		removeQ.clear();
	}
	
	
	public void updateRemovals(){
		if (removeQ.size == 0) return;
		//Iterator<Array<BusItem>> mapEntries = map.values().iterator();
		mapValues.reset();
		while (mapValues.hasNext()){
			Array<BusMethod> a = mapValues.next();
			Iterator<BusMethod> ia = a.iterator();
			while (ia.hasNext()){
				BusMethod busItem = ia.next();
				
				if (removeQ.containsKey(busItem.bd.hash)){
					ia.remove();
					busItemPool.free(busItem);
				}
			}
		}
		//go thru every component and do remnoveSiblings
		componentValues.reset();
		while (componentValues.hasNext()){
			BusData bd = componentValues.next();
			
			removeEntries.reset();
			while (removeEntries.hasNext()){
				Entry<Object> e = removeEntries.next();
				bd.removeSibling(components.get(e.key));
			}
		}
		//free data and busComponents
		//removeQ.e
		//Entries<Object> removeEntries = ent;//removeQ.entries();
		removeEntries.reset();
		
		while (removeEntries.hasNext()){
			Entry<Object> e = removeEntries.next();
			int key = e.key;
			BusData bc = components.remove(key);
			if (bc.c.overrideIndex != 0){
				//TODO
				//check if its the primary
				if (overrideData[bc.c.overrideIndex].hash == bc.hash){
					Array<BusData> a = overridden.get(bc.c.overrideIndex); 
					overrideData[bc.c.overrideIndex] = a.pop();
				}
				//else remove from the stack the stack
				else {
					overridden.get(bc.c.overrideIndex).removeValue(bc, true);
				}
				
				
			}
			dataPool.free(bc.ints);
			busDataPool.free(bc);
		}
		recalculateTickGroups();
		removeQ.clear();
	}
	
	private void addSibling(BusData bd){
		//assume it's just subscribed, check all other c's and add refs
		componentValues.reset();
		while (componentValues.hasNext()){
			BusData data = componentValues.next();
			data.addSibling(bd);
		}
	}
	
	

	private void recalculateTickGroups() {
		int newBits = 0;
		synchronized (componentValues){
			componentValues.reset();
			while (componentValues.hasNext()){
				BusData data = componentValues.next();
				if (data.c.tickGroupBits == -1) continue;
				newBits &= (1<<data.c.tickGroupBits);
			}
			parent.setTickGroup(newBits);
		}
		
	}

	private void free(Array<BusMethod> remove) {
		remove.clear();
		busItemArrayPool.free(remove);
		
	}
	private void add(BusMethod meth, BusData busData){
		if (map.containsKey(meth.clas)){
			
		}else {
			map.put(meth.clas, busItemArrayPool.obtain());
			
		}
		Array<BusMethod> arr = map.get(meth.clas);
		BusMethod item = busItemPool.obtain();
		item.set(meth.clas, meth.m);
		item.setBusData(busData);
		arr.add(item);
	}
	/*private void add(Class<?> messageClass, Method method, BusData bc) {
		if (map.containsKey(messageClass)){
			
		}else {
			map.put(messageClass, busItemArrayPool.obtain());
			
		}
		Array<BusMethod> arr = map.get(messageClass);
		BusMethod item = busItemPool.obtain();
		item.set(method, bc);
		
		arr.add(item);
		//components.get(messageClass).add(c);
		//dataInt.get(messageClass).add(data);
	}*/
	private Array<IntArray> getIntArray() {
		return dataArrayPool.obtain();
	}
	private static class BusItemPool extends Pool<BusMethod>{

		@Override
		protected BusMethod newObject() {
			return new BusMethod();
		}
		
	}
	
	private static class BusItemArrayPool extends Pool<Array<BusMethod>>{

		@Override
		protected Array<BusMethod> newObject() {
			return new Array<BusMethod>();
		}
		
	}
	private static class DataArrayPool extends Pool<Array<IntArray>>{

		@Override
		protected Array<IntArray> newObject() {
			return new Array<IntArray>();
		}
		
	}
	
	private static BusItemPool busItemPool = new BusItemPool();
	private static BusItemArrayPool busItemArrayPool = new BusItemArrayPool();
	private static DataArrayPool dataArrayPool = new DataArrayPool();

	IntPool dataPool = new IntPool();

	public void clear(){
		for (int i = 0; i < OVERRIDE_MAX; i++){
			overrideData[i] = null;
		}
	}
	

	public static class BaseMessage{
		//int group = -1;
		
		//String className;
		//Class cl;
		/*public BaseMessage(String className){
			try {
				cl = ClassReflection.forName(className);
			} catch (ReflectionException e) {
				
				e.printStackTrace();
			}
		}*/
	}


	public BusData getDataForSibling(int hash) {
		// TODO Auto-generated method stub
		return null;
	}

	/*public IntArray getIntData(Component c) {
		return dataInt.get(c.getClass())
	}*/


}
