package com.niz;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.IntArray;
import com.badlogic.gdx.utils.IntMap;
import com.badlogic.gdx.utils.IntMap.Entries;
import com.niz.Messages.TouchDownMessage;
import com.niz.components.Components;
import com.niz.decals.DecalBatch;
import com.niz.threading.FontProcessor;
import com.niz.threading.Producer;

public class SynthEngine implements ApplicationListener, InputProcessor {
	private String TAG = "engine";
	public static final float TICK_DELTA = 1f/120f, DRAW_DELTA = 1f/30f;
	public float tickAccumulator = 0, drawAccumulator = 0;
	
	private DecalBatch batch;
	private GL20 gl;
		
	private Map map;
	private UIBatch uiBatch;// = new UIBatch();
	private SpriteBatch spriteBatch;
	
	public int numberOfThreads = 1;
	private String[] initFiles = {"menu"};
	public static Producer producer;
	
	public OrthographicCamera camera;
	
	public Sprite sprite;
	
	public SynthEngine(int cores){
		numberOfThreads = cores;
	}
	
	public void preInit(){
		Data.requestData("menu");
		//animations, etc
		
	}
	
	public void postInit(){
		
		//map.loadPlane(Data.getPlane("menu"));
		
		for (int i = 0; i < initFiles.length; i++){
			map.loadFrom(initFiles[i]);
		}
		
		
		//Component.setStaticValues();
		CorneredSprite.makeLookup();
		
	}
	@Override
	public void create() {		
		float w = Gdx.graphics.getWidth();
		float h = Gdx.graphics.getHeight();

		//batch = new DecalBatch(new CameraGroupStrategy(null));
		uiBatch = new UIBatch();
		Messages.init();
		producer = new Producer();
		producer.init(10, numberOfThreads);
		map = new Map(numberOfThreads);
		Components.init();
		map.init();
		spriteBatch = new SpriteBatch();
		//spriteBatch.
		//Plane displayPlane = Data.getPlane("initial");
		//map.addPlane(displayPlane);
		//set up rendering
		
		//producer.addAsset(FontProcessor.obtain().set("font16"));
		FontProcessor fp = FontProcessor.obtain().set("font16");
		fp.process();
		fp.onFinished(producer, null);
		
		Gdx.graphics.requestRendering();
		
		preInit();
		
		sprite = new Sprite(new Texture(Gdx.files.internal("data/test.png")));
		sprite.setPosition(0, 0);
		sprite.setSize(.25f, .25f);
		
		Gdx.input.setInputProcessor(this);
		
		
		
		float uiXRes = Gdx.graphics.getWidth() / (float)Gdx.graphics.getHeight();
		
		camera = new OrthographicCamera(uiXRes*2, 2f);
		
		 uiBatch = new UIBatch();
	        // opengl uses -1 to 1 for coords and also is inverted
		 uiBatch.createMesh(new float[] { -0.5f, -0.5f, Color.toFloatBits(0, 0, 255, 255),
                 0.5f, -0.5f, Color.toFloatBits(0, 255, 0, 255),
                 0f, 0.5f, Color.toFloatBits(255, 0, 0, 255) });
		
	}
	
	
	
	public void set(BlockDefold[]  blockDefs){
		//Block.setDefs(blockDefs);
	}

	@Override
	public void dispose() {
		batch.dispose();
		uiBatch.dispose();
		//texture.dispose();
	}

	@Override
	public void render() {	
		processTouchDowns();
		float delta = Gdx.graphics.getDeltaTime();
		tickAccumulator += delta;
		drawAccumulator += delta;
		while (tickAccumulator > TICK_DELTA){
			tickAccumulator -= TICK_DELTA;
			producer.nextFrame();
			//Gdx.app.log(TAG, "nextframe");
		}
		if (drawAccumulator > DRAW_DELTA){
			drawAccumulator -= DRAW_DELTA;
			//producer.draw(gl, camera, batch);
			//Gdx.app.log(TAG, "draw");
		}
		Gdx.graphics.getGL20().glClearColor(0.2f, 0.2f, 0.2f, 1);
	    Gdx.graphics.getGL20().glClear(GL10.GL_COLOR_BUFFER_BIT);
	    
		camera.update();
		//Gdx.app.log(TAG, "draw"+map.uiPlane.getCamera().viewportWidth);
		uiBatch.circle(0,0,.5f,Color.WHITE);
		
		
	    uiBatch.drawMesh(camera);
		
		
	}

	private void processTouchDowns() {
		touchEntries.reset();
		while (touchEntries.hasNext()){
			IntArray a = touchEntries.next().value;
			Entity e = map.getEntity(a.get(3));
			TouchDownMessage message = Messages.touchDown.obtain();
			message.set(a);
			e.bus.publish(message);
			a = null;
			message.set(a);
			Messages.touchDown.free(message);
		}
		
	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public boolean keyDown(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}
	
	private IntMap<IntArray> touchMap = new IntMap<IntArray>();
	private Entries<IntArray> touchEntries = new Entries<IntArray>(touchMap);
	//hash is pointer id, intarray is x y button entityid
	private Vector3 tmpV = new Vector3();
	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		//collide
		//Gdx.app.log(TAG, "touchd"+pointer);
		Entity e = map.touchCollide(screenX, screenY);
		if (e == null) return false;
		if (touchMap.containsKey(pointer))
			Entity.intPool.free(touchMap.remove(pointer));
		IntArray a = Entity.intPool.obtain(4);
		a.set(0, screenX);
		a.set(1, screenY); 
		a.set(2, button);
		a.set(3, e.id);
		touchMap.put(pointer, a);		
		//e.bus.publish(Messages.touchDown.obtain());
		return true;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		IntArray a = touchMap.get(pointer);
		if (a == null) return false;
		Gdx.app.log(TAG, "touchUp"+pointer+touchMap.get(pointer));
		Entity e = map.getEntity(a.get(3));
		//tmpV.set(screenX, screenY, 0);
		//camera.unproject(tmpV);
		e.bus.publish(Messages.touchUp.obtain().set(a));
		
		touchMap.remove(pointer);
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		//Gdx.app.log(TAG, "touchdrag"+pointer+touchMap.get(pointer));
		IntArray a = touchMap.get(pointer);
		if (a == null) return false;
		a.set(0, screenX);
		a.set(1, screenY);
		//Entity e = map.getEntity(a.get(3));
		//tmpV.set(screenX, screenY, 0);
		
		//camera.unproject(tmpV);
		//e.bus.publish(Messages.touchDrag.obtain().set(tmpV.x, tmpV.y, tmpV.z, 3, pointer));
		
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}
}
