LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)
 
LOCAL_MODULE    := verlet
LOCAL_C_INCLUDES := fixedpt fixedpt/boost fixedpt/stl 
 
LOCAL_CFLAGS := $(LOCAL_C_INCLUDES:%=-I%) -O2 -Wall -D__ANDROID__
LOCAL_CPPFLAGS := $(LOCAL_C_INCLUDES:%=-I%) -O2 -Wall -D__ANDROID__
LOCAL_LDLIBS := -lm
LOCAL_ARM_MODE  := arm
 
LOCAL_SRC_FILES := com.niz.verlet.PhysicsEngine.cpp\
	memcpy_wrap.c
 
include $(BUILD_SHARED_LIBRARY)
