#include <com.niz.verlet.PhysicsEngine.h>

//@line:23

		#include <fixed.h>
		
		
	
	
	
// Sample code for physics simulation
	class ParticleSystem {
		//
		
		static const int NUM_PARTICLES = 200
		, NUM_STICKS = 50
		, NUM_SPRINGS = 50
		, NUM_ANGLES = 50;
		int particleProgress, stickProgress, springProgress, angleProgress;
		;
		int particleIndex[NUM_PARTICLES], stickIndex[NUM_STICKS], springIndex[NUM_SPRINGS], angleIndex[NUM_ANGLES];
		int particleID, stickID, springID, angleID;
		Fixed data[NUM_PARTICLES*8]
		, sticks[NUM_STICKS*4]
		, springs[NUM_SPRINGS*5]
		, angles[NUM_ANGLES * 4]
		;
		Fixed fTimeStep// = 1.0/120.0
		, fTimeStep2;// = fTimeStep*fTimeStep;
		int ACTIVE_PARTICLES;
	public:
		void TimeStep();
		void setup(float timeStep, int particleCount );
		int addParticle(float x, float y, float fx, float fy, float g, float w);
		ParticleSystem();
		int addStickConstraint(int p1, int p2, float length);
		int addSpring(int p1, int p2, Fixed &len, Fixed &stiffness);
		void getParticlePosition(int &id, int* val);
		void setParticle(int id, int x, int y, int fx, int fy);
	private:
		void Verlet();
		void SatisfyConstraints();
		void AccumulateForces();

	//(constructors, initialization etc. omitted)
	};
	
	
	
	Fixed gravity_x = 0, gravity_y = -10;
	ParticleSystem systems[16];
	int systemCount;
	static int rollingID = 0;
	
	
	Fixed tx, ty, dx, dy, sqrt, invW1, invW2;
	Fixed deltaLength, diff, half = 0.5, dot, tmp;
	static int posRet[8];
	// Verlet integration step
	void ParticleSystem::Verlet() {
		for(int i=0; i<ACTIVE_PARTICLES; i++) {
			
			//x += x-oldx+a*fTimeStep*fTimeStep;
			
			
			int d = i*8;
			Fixed x = data[d++]
			,y = data[d++]
			,ox = data[d++]
			,oy = data[d++]
			,ax = data[d++]
			,ay = data[d++]
			,tx = x
			,ty = y;
			
			//x += x-ox+ax*fTimeStep2;
			//y += y-oy+ay*fTimeStep2;
			d = i*8;
			data[d++] += x-ox+ax*fTimeStep2; 
			data[d++] += y-oy+ay*fTimeStep2;
			data[d++] = tx;
			data[d++] = ty;
		}
	}
	// This function should accumulate forces for each particle
	void ParticleSystem::AccumulateForces()
	{
	// All particles are influenced by gravity
		for(int i=0; i<NUM_PARTICLES; i++){
			data[i*8+4] += gravity_x;
			data[i*8+5] += gravity_y;
		}
	}
	
	Fixed sqrtDotGuess(Fixed &dx, Fixed &dy, Fixed &guess, Fixed &guess2){
		dot = dx*dx+dy*dy;	
		sqrt = guess - (guess2-dot)/(guess * 2.0);
		return sqrt;	
	}  
	
	// Here constraints should be satisfied
	void ParticleSystem::SatisfyConstraints() {
		for (int i = 0; i < NUM_STICKS; i++){
			int d = i*6;
			int p1 = sticks[d++]
			,p2 = sticks[d++];
			Fixed & len = sticks[d++]
			,&len2 = sticks[d++]
			,&x1 = data[particleIndex[p1]]
			,&y1 = data[particleIndex[p1]+1]
			,&x2 = data[particleIndex[p2]]
			,&y2 = data[particleIndex[p2]+1];
			dx = x2-x1;
			dy = y2-y1;
			//Vector3 delta = x2-x1;
			deltaLength = sqrtDotGuess(dx, dy, len, len2);//sqrt(delta*delta);
			diff = (deltaLength-len)/deltaLength;
			tx = dx*half*diff;
			ty = dy*half*diff;
			x1 += tx;
			y1 += ty; 
			x2 -= tx;
			y2 -= ty;

		}
		
	}
	
	
	
	
	
	void ParticleSystem::TimeStep() {
		AccumulateForces();
		Verlet();
		SatisfyConstraints();
	}
	
	ParticleSystem::ParticleSystem(){
	
	}
	
	void ParticleSystem::setup(float timeStep, int particleCount){
		fTimeStep = timeStep;
		fTimeStep2 = fTimeStep*2.0;
		//NUM_PARTICLES = particleCount;
		ACTIVE_PARTICLES = 0;
		//indexTable = new int[NUM_PARTICLES]
	}
	
	int ParticleSystem::addParticle(float x, float y, float fx, float fy, float g, float w){
		//find index
		 //progress is how full the data is, index is next index 
		int count = 0;
		//loop to find free id
		while (count < NUM_PARTICLES && particleIndex[particleID] != -1){
			particleID++;
			if (particleID >= NUM_PARTICLES)particleID = 0;
			count++;
		}
		 
		this->particleIndex[particleID] = this->particleProgress++;
		int d = this->particleProgress*8;
		data[d++] = x;
		data[d++] = y;
		data[d++] = x;
		data[d++] = y;
		data[d++] = fx;
		data[d++] = fy;
		data[d++] = g;
		data[d++] = w;
		return this->particleID;
	
	}
	
	int ParticleSystem::addSpring(int p1, int p2, Fixed &len, Fixed &stiffness){
		//find index
		 //progress is how full the data is, index is next index 
		int count = 0;
		//loop to find free id
		while (count < NUM_SPRINGS && springIndex[springID] != -1){
			springID++;
			if (springID >= NUM_SPRINGS)springID = 0;
			count++;
		}
		 
		this->springIndex[springID] = this->springProgress++;
		int d = this->springProgress*5;
		data[d++] = p1;
		data[d++] = p2;
		data[d++] = len;
		data[d++] = len*len;
		data[d++] = stiffness;
		
		return this->springID;
	
	}
	
	
	int ParticleSystem::addStickConstraint(int p1, int p2, float len){
		//progress is how full the data is, index is next index 
		int count = 0;
		//loop to find free id
		while (count < NUM_SPRINGS && stickIndex[springID] != -1){
			stickID++;
			if (stickID >= NUM_STICKS)stickID = 0;
			count++;
		}
		 
		this->stickIndex[stickID] = this->stickProgress++;
		int d = this->stickProgress*4;
		data[d++] = p1;
		data[d++] = p2;
		data[d++] = len;
		data[d++] = len*len;
		
		
		return this->stickID;
	
	}
	
	void ParticleSystem::getParticlePosition(int &id, int* val){
	for (int i = 0; i < 8; i++)
		val[i] = this->data[particleIndex[id]*8+i];
		
	}
	
	void ParticleSystem::setParticle(int id, int x, int y, int fx, int fy){
		int d = particleIndex[id*8];
		data[d++] = x;
		data[d++] = y;
		d+=2;
		data[d++] = fx;
		data[d++] = fy;
		}
		 JNIEXPORT void JNICALL Java_com_niz_verlet_PhysicsEngine_stepJNI(JNIEnv* env, jobject object) {


//@line:261

		systems[0].TimeStep();
	
	

}

JNIEXPORT jint JNICALL Java_com_niz_verlet_PhysicsEngine_addParticle(JNIEnv* env, jobject object, jfloat x, jfloat y, jfloat fx, jfloat fy, jfloat g, jfloat w) {


//@line:267

		return systems[0].addParticle(x,y,fx,fy,g,w);



}

JNIEXPORT void JNICALL Java_com_niz_verlet_PhysicsEngine_getParticlePositionJNI(JNIEnv* env, jobject object, jint id, jintArray obj_v) {
	int* v = (int*)env->GetPrimitiveArrayCritical(obj_v, 0);


//@line:274

	
		systems[0].getParticlePosition(id, v);
		
	
	
	env->ReleasePrimitiveArrayCritical(obj_v, v, 0);

}

JNIEXPORT void JNICALL Java_com_niz_verlet_PhysicsEngine_setParticleDataJNI(JNIEnv* env, jobject object, jint id, jint x, jint y, jint fx, jint fy) {


//@line:301

		systems[0].setParticle(id, x, y, fx, fy);
	
	

}

JNIEXPORT void JNICALL Java_com_niz_verlet_PhysicsEngine_clearSystems(JNIEnv* env, jobject object) {


//@line:310

		for (int i = 0; i < 16; i++){
			delete &systems[i];
		
		}
		systemCount = 0;
	
	
	

}

