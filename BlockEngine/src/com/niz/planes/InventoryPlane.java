package com.niz.planes;

import com.niz.Map;
import com.niz.components.Components;

public class InventoryPlane extends UIPlane {
	public InventoryPlane(Map map) {
		super(map);
		
	}

	@Override
	public void init(){
		
		for (int i = 0; i < uiInvGridXCount; i++){
			for (int j = 0; j < uiInvGridYCount; j++){
				createEntity("inv"+i+"-"+j)
				.addComponent(
						Components.cPosition, (int)(i*uiInvGridOffset), (int)(j*uiInvGridOffset)
						)
				.addComponent(
						Components.cDrawInvButton, i + (int)(j*uiInvGridOffset)
						);
				
			}
		}
	}
}
