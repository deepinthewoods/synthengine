package com.niz.planes;

import com.niz.Data;
import com.niz.Entity;
import com.niz.Map;
import com.niz.components.Components;
import com.niz.components.ui.NewButton;
import com.niz.verlet.PhysicsEngine;

public class MenuPlane extends UIPlane{
	public MenuPlane(Map map){
		super(map);
		name =  "menu plane";
	}
	
	
	@Override
	public void init(){
		int hash = Data.addString("Make a New Game");
		Entity e = createEntity("new_game_button")
		.addComponent(
				Components.cPosition, 0, 0, 0, 0
				)
		.addComponent(Components.cBody, 0, 0, 0, 0, 0, 0, 0) ;
		e.addComponent(
				Components.cDrawTextButton, hash
				, 0 //style
				, 2 ////color
				, 3 //text color
				, 0 //font
				, 0 //scale
				)
		.addComponent(Components.cCollide2d, 0)
		.addComponent(Components.cTouchablePlaneDeferring, 0, 0, 0, 0, 0, 0, 0, 0)
		.addComponent(Components.cNewGameButton, 0, 0, 0)
		.addComponent(Components.cPhysics, 0, 0, 0, 0, 0, 0, 0, 0, 0)
		//.addComponent(Components., ints)
		
		;
		
		
		addEntity(e);
		
		Entity e2 = createEntity("plane controller")
				.addComponent(
						Components.cPosition, 0, 0
						)
				.addComponent(Components.cBody, 0
						, PhysicsEngine.ONE*100 //w
						, PhysicsEngine.ONE*100 //h
						, 0, 0, 0, 0) 
				
				.addComponent(Components.cCollide2d, 0)
				.addComponent(Components.cTouchablePlaneDeferring, 0, 0, 0, 0, 0, 0, 0, 0)
				.addComponent(Components.cCameraMover, 0)
				.addComponent(Components.cSwipeDrag, 0, 0, 0, 2)
				;
						
		addEntity(e2);
				
				//*/
	}
	
}
