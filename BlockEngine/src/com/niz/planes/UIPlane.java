package com.niz.planes;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.IntArray;
import com.niz.Component;
import com.niz.Entity;
import com.niz.Map;
import com.niz.Plane;
import com.niz.UIBatch;
import com.niz.components.Components;
import com.niz.verlet.PhysicsEngine;

public class UIPlane extends Plane {
	



	private static final String TAG = "ui plane";
	private static final int BELT_BUTTON_COUNT = 8;
	public static float uiXRes, uiXCm, uiYCm, uiXPix, uiYPix, onePixX, onePixY
	, uiBeltHeight, uiInvGridYCount, uiInvGridXCount, uiInvGridOffset;
	
	public static float dragMinDistance;
	
	public UIPlane(Map map) {
		super(map);
		
	}
	
	@Override
	public void init(){

		
		
		
		
		uiBeltHeight = .15f;
		
		
		uiXPix = Gdx.graphics.getWidth();
		uiYPix = Gdx.graphics.getHeight();
		
		uiXRes = Gdx.graphics.getWidth() / (float)Gdx.graphics.getHeight();
		uiXCm = Gdx.graphics.getWidth() / (float)Gdx.graphics.getPpcX();
		uiYCm = Gdx.graphics.getHeight() / (float)Gdx.graphics.getPpcY();
		
		onePixX = uiXRes/uiXPix;
		onePixY = 1f/uiYPix;
		OrthographicCamera cam = new OrthographicCamera(
				uiXPix
				,uiYPix);
		
		
		
		Gdx.app.log(TAG, "ui x "+uiXRes);
		Gdx.app.log(TAG, "ui pix x "+onePixX);
		cam.position.set(0,0,0);
		setCamera(cam);
		
		//belt
		float[] beltX = new float[BELT_BUTTON_COUNT];
		for (int i = 0; i < beltX.length; i++){
			beltX[i] = uiXRes/BELT_BUTTON_COUNT * i + (uiXRes/BELT_BUTTON_COUNT/2);
		}
		
		float screenWidth = uiXRes - uiBeltHeight;
		//left half 
		float leftCentre = screenWidth/4;
		//right half
		float rightCentre = (screenWidth/4)*3;
		//mode buttons on r
		int horizButtonCount = (int)(1f/uiBeltHeight);
		float[] rightY = new float[horizButtonCount-1];
		for (int i = 0; i < horizButtonCount-1; i++){
				rightY[i] = -.5f+uiBeltHeight*i;
		}
		
		//inv grid
		uiInvGridYCount = horizButtonCount;
		uiInvGridXCount = (screenWidth/2f)/uiBeltHeight;
		uiInvGridOffset = (1f-uiBeltHeight)/uiInvGridYCount;
		
		dragMinDistance = .5f*uiXPix/uiXCm;
		
		setDefaults();
		
		
		MenuPlane mp = new MenuPlane(map);
		mp.setCamera(cam);
		addChild(mp);
		
		
	}
	
	

	public void setDefaults(){
		float scale =  onePixY;
		//while (scale*16f < 1f/uiYCm ){
		//	scale *= 2;
			//UIBatch.fontScale = scale;
			
		//}
		
		
	}
	
}
