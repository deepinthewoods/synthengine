package com.niz.planes;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.niz.Chunk;
import com.niz.Entity;
import com.niz.Map;
import com.niz.Plane;
import com.niz.components.Components;

public class PlatformerPlane extends Plane {

	public PlatformerPlane(Map map) {
		super(map);
		//1d plane of blocks
		chunkBitsX = 7;
		chunkBitsY = 5;
		chunkBitsZ = 1;
		size.set(1000000, 100000000, 1);
		name = "2d platformer plane";
	}
	
	public void init(){
		Chunk c = new Chunk();
		c.setTestA();
		addChunk(c);
		
		
		Entity e = createEntity("Chunk controller")
				//.addComponent(
				//		Components.cPosition, 0, 0, 0, 0
				//		)
				//.addComponent(Components.cBody, 0, 0, 0, 0, 0, 0, 0) ;
				//e.addComponent(
				//		Components.cDrawPolygon
				//		, 0 //style
				//		, 2 ////color
						
				//		)
				//.addComponent(Components.cCollide2d, 0)
				//.addComponent(Components.cTouchablePlaneDeferring, 0, 0, 0, 0, 0, 0, 0, 0)
				//.addComponent(Components.cNewGameButton, 0, 0, 0)
				//.addComponent(Components.cPhysics, 0, 0, 0, 0, 0, 0, 0, 0, 0)
				.addComponent(Components.cChunkController);
				
				
				
		//addEntity(e);
		OrthographicCamera cam = new OrthographicCamera();
		cam.setToOrtho(false, 30, 20);
		setCamera(cam);
		
	}
	
	

}
