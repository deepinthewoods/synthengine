package com.niz.states;

import com.niz.Entity;
import com.niz.State;

public class LinkedState extends State {
	public State next, prev;
	
	public int ticks;
	public LinkedState(State next, State prev){
		this.next = next;
		this.prev = prev;
	}
	/*
	 * check for timeup and change states
	 */
	@Override
	public void tick(Entity act){
		if (act.stateTick >= ticks) act.changeState(next);
		else if (act.stateTick < 0) act.changeState(prev);
	}
}
