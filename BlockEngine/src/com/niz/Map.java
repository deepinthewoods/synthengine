package com.niz;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.IntMap;
import com.badlogic.gdx.utils.IntMap.Entries;
import com.niz.planes.BlockPlane2d;
import com.niz.planes.PlatformerPlane;
import com.niz.planes.UIPlane;
import com.niz.verlet.IPhysics;
import com.niz.verlet.JPhysicsEngine;

public class Map {
	private static final String TAG = "Map";
	public static int THREAD_GROUP_TOTAL = 16;
	private IntMap<Plane> planes = new IntMap<Plane>();
	private Entries<Plane> planeEntries = new Entries<Plane>(planes);
	
	private IntMap<Entity> namedEntities = new IntMap<Entity>();
	//Contains and manages streaming of all the Entities and Planes 
	private OrthographicCamera orthoCam;
	private PerspectiveCamera perspCam;
	private Camera camera;//will be current
	public String savePath;
	//private Producer producer;
	private int numberOfThreads;
	Plane uiPlane, mainPlane;
	public Map(int numberOfThreads){
		this.numberOfThreads = numberOfThreads;
		//ui plane
		
		
		

		
		
		
		
	}
	public void addPlane(Plane mp) {
		planes.put(Data.stringHash(mp.name), mp);
		mp.init();
		
	}
	public void init(){
		uiPlane = new UIPlane(this);
		uiPlane.name = "ui";
		addPlane(uiPlane);
		screenStack.add(uiPlane);
		
		mainPlane = new BlockPlane2d(this);
		
	}
	public Plane getPlane(int id){
		return planes.get(hash(id));
	}

	private int hash(int id) {
		return id;
	}

	Array<Plane> screenStack = new Array<Plane>();
	public IPhysics physics = new JPhysicsEngine();
	

	public Entity touchCollide(int screenX, int screenY) {
		
		//menu camera
		Entity e = null;
		//collide with all entities in the menu plane
		int i = 0;
		Plane p;// = screenStack.get(i);
		//e = p.touchCollide(screenX, screenY);
		while (e == null && i < screenStack.size){
			p = screenStack.get(i++);
			e = p.touchCollide(screenX, screenY);
			//Gdx.app.log(TAG, "touchcollide"+p.name);
		}
		
		return e;
		
	}
	public Entity getEntity(int key){
		Entity e;
		e = getNamedEntity(key);
		if (e == null){
			planeEntries.reset();
			while (planeEntries.hasNext()){
				Plane p = planeEntries.next().value;
				e = p.getEntity(key);
				if (e != null)
					return e;
			}
		}
		
		return e;
	}
	
	
	public Entity getNamedEntity(int key) {
		if (namedEntities.containsKey(key)){
			
		}
		return namedEntities.get(key);
	}
	public void removeNamedEntity(Entity e) {
		namedEntities.remove(e.id);
	}
	public void tick() {
		// TODO Auto-generated method stub
		
	}
	public void startNewPlatformerGame() {
		planes.clear();
		namedEntities.clear();
		//1-deep chunks, 512x32
		Plane platformerPlane = new PlatformerPlane(this);
		//physics
		addPlane(platformerPlane);
		
	}
	public Entries<Plane> getPlanes() {
		return this.planeEntries;
		
	}
	
	
	
}
