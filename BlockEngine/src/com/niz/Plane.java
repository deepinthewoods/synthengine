package com.niz;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.IntMap;
import com.badlogic.gdx.utils.IntMap.Values;
import com.badlogic.gdx.utils.LongMap;
import com.badlogic.gdx.utils.Pool;
import com.badlogic.gdx.utils.Pools;
import com.niz.threading.Messages;
import com.niz.threading.Messages.CollideQueryMessage;
import com.niz.threading.Messages.TouchDragMessage;
import com.niz.threading.Messages.TouchDragStopMessage;
import com.niz.threading.Messages.TouchFlingMessage;

public class Plane {
	
	//private Camera camera;
	//private Entity cameraEntity;
	
	private LongMap<Chunk> chunks = new LongMap<Chunk>();//TODO change back to IntMap and check performance
	private LongMap.Values<Chunk> chunkValues = new LongMap.Values<Chunk>(chunks);
	private LongMap<Chunk> loadingChunks = new LongMap<Chunk>();
	private LongMap.Values<Chunk> loadingChunkValues = new LongMap.Values<Chunk>(loadingChunks);
	
	public Map map;
	public Plane parent = this;
	public Array<Plane> children = new Array<Plane>();
	//TODO layers!
	private IntMap<Entity> entities = new IntMap<Entity>()
			//, agents = new IntMap<Entity>()
			//, particles = new IntMap<Entity>()
			;
	
	private Values<Entity> entityValues = new Values<Entity>(entities)
		//	, agentValues = new Values<Entity>(agents)
		//	, particleValues = new Values<Entity>(particles)
			;
	
	
	
	
	
	public static Pool<Entity> entityPool = Pools.get(Entity.class); 
	public String name;
	private Vector3 tmpV = new Vector3(), tmpW = new Vector3();;
	//settings 
	private Camera camera;;
	//bounds
	//private int boundsX1, boundsX2, boundsY1, boundsY2, boundsZ1, boundsZ2;
	//background type
	public Vector offset = new Vector(), size = new Vector();
	//chunk bits xyz
	protected int chunkBitsX = 8;
	protected int chunkBitsY = 8;
	protected int chunkBitsZ = 8;
	
	protected int blobBitsX = 1;
	protected int blobBitsY = 1;
	protected int blobBitsZ = 1;
	protected int blobMaskX, blobMaskY, blobMaskZ;
	public int blobTotal;
	//private int offsetX, offsetY, offsetZ;
	private Matrix4 transform = new Matrix4();
	
	
	private int nextEntityHash;
	//default camera/game mode
	public int collideType = 0;
	public static final int COLLIDE_TYPE_2D=0;
	private static final String TAG = "plane";
	
	
	
	public Plane(Map map){
		this.map = map;
		transform.setToTranslation(0, 0, 0);
	}
	
	public Plane setChunkBits(int bx, int by, int bz){
		chunkBitsX = bx;
		chunkBitsY = by;
		chunkBitsZ = bz;
		return this;
	}
	public Plane setBlobBits(int bx, int by, int bz){
		blobBitsX = bx;
		blobBitsY = by;
		blobBitsZ = bz;
		blobMaskX = (1<<bx)-1;
		blobMaskY = (1<<by)-1;
		blobMaskZ = (1<<bz)-1;
		blobTotal = 1<<bx+by+bz;
		return this;
	}
	
	public Plane set(String name){
		this.name = name;
		this.savePath = map.savePath + "/"+name;
		return this;
	}
	
	public void setCamera(Camera cam){
		camera = cam;
	}

	protected void addChild(Plane p) {
		children.add(p);
		p.parent = this;
		p.setCamera(camera);
		map.addPlane(p);
		map.screenStack.add(p);
	}
	
	public Entity touchCollide(int screenX, int screenY) {
		Gdx.app.log(TAG, "touch collide");
		entityValues.reset();
		tmpV.set(screenX, screenY, 0);
		camera.unproject(tmpV);
		tmpW.set(screenX, screenY, 1);
		camera.unproject(tmpW);
		CollideQueryMessage message = Messages.obtainCollideQuery(collideType);
		message.answered = false;
		while (entityValues.hasNext()){
			Entity e = entityValues.next();
			message.set(e, tmpV, tmpW);
			e.bus.publish(message);
			Gdx.app.log(TAG, "collide message"+e.name+message.answered);
			if (message.answered) return message.ret; 
		}
		return null;
	}
	
	public void touchDrag(int screenX, int screenY, int prevX, int prevY) {
		entityValues.reset();
		tmpV.set(screenX, screenY, 0);
		camera.unproject(tmpV);
		
		tmpW.set(prevX, prevY, 0);
		camera.unproject(tmpW);
		
		TouchDragMessage message = Messages.touchDrag.obtain();
		message.answered = false;
		message.set(null, tmpV, tmpW);
		publishAll(message);
	}

	public void fling(int x, int y, int firstx, int firsty) {
		entityValues.reset();
		tmpV.set(x, y, 0);
		camera.unproject(tmpV);
		tmpW.set(firstx, firsty, 0);
		camera.unproject(tmpW);
		TouchFlingMessage message = Messages.fling.obtain();
		message.answered = false;
		message.set(null, tmpV, tmpW);
		publishAll(message);
		
	}

	public void stopDrag(int x, int y) {
		entityValues.reset();
		tmpV.set(x, y, 0);
		camera.unproject(tmpV);
		
		TouchDragStopMessage message = Messages.touchDragStop.obtain();;
		message.answered = false;
		message.set(null, tmpV);
		publishAll(message);
		
	}



	public synchronized void addEntity(Entity e){
		e.id = nextEntityHash;
		entities.put(nextEntityHash++, e);
		Gdx.app.log(TAG, "addE "+e.id);
	}
	
	public void publishGroup(int group, EntityMessage m){
		
	}
	
	public void publishAll(EntityMessage m){
		
		Gdx.app.log(TAG, "publishAll"+m);
		entityValues.reset();
		while (entityValues.hasNext()){
			Entity e = entityValues.next();
			e.bus.publish(m);
		}
	}
	
	
	public int getBlock(int x, int y, int z){
		Chunk c = chunks.get(getKey(x>>chunkBitsX,y>>chunkBitsY, z>>chunkBitsZ));
		if (c == null) return 0;
		return c.getBlock(x, y, z);
	}

	private long getKey(int x, int y, int z) {
		return x+(y<<20)+(z<<42);
	}



	
	
	//private AtomicQueue<Chunk> saveQ = new AtomicQueue<Chunk>(64);
	/*private Chunk saveQ;
	public void tickSave(){
		if (saveQ != null){
			if  (saveQ.tickSave()) saveQ = null;
		} else {//put new chunk in q
			//iterate through chunks, find first stable modified chunk and add
			//Chunk selectChunk;
			Iterator<Chunk> i = chunks.values();
			while (i.hasNext()){
				Chunk c = i.next();
				if (c.modified){
					if (!c.hasNoUpdates()){
						saveQ = c;
						return;
					}
				}
			}
			//TODO pause thread here
		}
	}*/
	
	public void clear(){
		chunks.clear();
		/*Iterator<Entity> i = entities.values();
		while (i.hasNext()){
			Entity e = i.next();
			map.removeEntity(e);
		}*/
		entities.clear();
	}

	public Entity createEntity() {
		Entity e = entityPool.obtain();
		e.plane = this;
		e.name = "";
		return e;
	}
	public Entity createEntity(String name){
		Entity e = entityPool.obtain();
		e.plane = this;
		e.name = name;
		return e;
	}

	public Entity getEntity(int key) {
		return entities.get(key);
	}

	public void init() {}

	public Camera getCamera() {
		return camera;
	}

	public void addChunk(Chunk c) {
		long key = c.getHash();
		chunks.put(key, c);
	}

	public com.badlogic.gdx.utils.LongMap.Values<Chunk> getChunks() {
		return chunkValues;
	}

	BlockPositionMap chunkLoadQueue = DesktopBlockMap.obtain();
	
	public String savePath;
	
	public void queueChunkLoads(int limit) {
		int x=0, y=0, z=0, total=0;
		Gdx.app.log(TAG, "queue chunk loads"+name);
		while (total < limit){
			if (!chunks.containsKey(Chunk.getHash(x, y, z)) && !chunkLoadQueue.contains(x, y, z)){
				chunkLoadQueue.add(x, y, z);
				total++;
			}
			x++;
			y++;
			z++;	
		}
	}

	public Chunk getNextLoad(ChunkPool chunkPool) {
		int next = chunkLoadQueue.pop();
		int x = BlockPositionMap.x(next), y = BlockPositionMap.y(next), z = BlockPositionMap.z(next);
		Chunk c = chunkPool.obtain().initChunk(x, y, z, this);
		//look for file
		if (!chunks.containsKey(Chunk.getHash(x,y,z))){
			
		}
		//else start generating, and set int[] to appropriate thing
		return null;
	}

	
	
	public Chunk getChunkByBlobOffset(int x, int y, int z, int progress) {
		int cx= x>>(blobBitsX+chunkBitsX)
				, cy= y>>(blobBitsY+chunkBitsY)
				, cz= z>>(blobBitsZ+chunkBitsZ);
		cx += progress&blobMaskX;
		cy += ((progress>>blobBitsX)&blobMaskY);
		cz += ((progress>>blobBitsY+blobBitsX)&blobMaskZ);
		return null;
	}
	
	//private IntMap<Blob> blobs = new IntMap<Blob>();
	
	
	
}
