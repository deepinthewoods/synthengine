package com.niz;

import com.badlogic.gdx.utils.IntArray;
import com.badlogic.gdx.utils.Pool;

public class IntPool extends Pool<IntArray> {

	@Override
	protected IntArray newObject() {
		
		return new IntArray();
	}
	
	public IntArray obtain(int size){
		IntArray a = obtain();
		a.clear();
		for (int i = 0; i < size; i++)
			a.add(0);
		return a;
	}
}
