package com.niz.threading;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g3d.decals.DecalBatch;
import com.niz.UIBatch;

public class DecalRenderer extends DrawProcessable {

	public DecalRenderer(int i) {
		super(i);
		// TODO Auto-generated constructor stub
	}

	

		 
		 
		
		 
	

	@Override
	public void post(Camera camera, com.niz.decals.DecalBatch batch, UIBatch uiBatch, SpriteBatch spriteBatch) {
		batch.flush();
	}

	@Override
	public void pre(Camera camera, com.niz.decals.DecalBatch batch,
			UIBatch uiBatch) {
		 
		 Gdx.gl.glEnable(GL20.GL_DEPTH_TEST);
		 Gdx.gl.glClearDepthf(1.0f);
		 Gdx.gl.glDepthFunc( GL20.GL_LEQUAL );
		 Gdx.gl.glDepthMask( true );
		 Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
		 Gdx.gl10.glAlphaFunc(GL20.GL_GREATER, 0);
	}

	

}
