package com.niz.threading;

import com.badlogic.gdx.utils.PauseableThread;

public class TickThread extends PauseableThread {
	public DelegatingRunnable run;
	int threadID;
	public StringBuilder stringBuilder = new StringBuilder();
	public TickThread(DelegatingRunnable runnable, int id) {
		super(runnable);
		run = runnable;
		threadID = id;
	}

}
