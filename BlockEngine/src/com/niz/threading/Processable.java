package com.niz.threading;

public interface Processable {
public boolean process();
public void onFinished(Producer producer, TickThread thread);


}
