package com.niz.threading;

import com.badlogic.gdx.utils.Pool;
import com.badlogic.gdx.utils.Pools;
import com.niz.Chunk;

public class ChunkProcessable implements Processable{
	private static Pool<ChunkProcessable> pool = Pools.get(ChunkProcessable.class);
	protected Chunk c;
	public ChunkProcessable obtain(){
		return pool.obtain();
	}
	public void free(){
		pool.free(this);
	}
	
	@Override
	public boolean process() {
		
		if (c.updateProcessing()){
			
		}
		return true;
		
	}

	@Override
	public void onFinished(Producer producer, TickThread thread) {
		if (producer.needsProcessorImmediately()){
			producer.holdProcessable(this);
			thread.run.set(producer.getNextProcess(thread.threadID));
		} else if (c.isFinished()){
			thread.run.set(producer.getNextProcess(thread.threadID));
			free();
		}
	}
	public void set(Chunk chunk) {
		c = chunk;
		
	}

}
