package com.niz.threading;

import javax.annotation.processing.Processor;

import com.badlogic.gdx.utils.Array;

public class SequentialProcessor implements Processable{
	int progress = 0;
	Array<Processable> list = new Array<Processable>();
	Processable active, lowPriority;
	
	@Override
	public boolean process(){
		if (active.process()){
			progress++;
			if (progress >= list.size){
				
				return true;
			}
			active = list.get(progress);
			return false;
		}
		return false;
	}
	
	public void addProcessable(Processable p){
		list.add(p);
	}
	
	public void reset(){
		progress = 0;
		active = list.get(0);
	}
	
	@Override
	public void onFinished(Producer producer, TickThread thread) {
		active.onFinished(producer, thread);
		
	}

	
	

}
