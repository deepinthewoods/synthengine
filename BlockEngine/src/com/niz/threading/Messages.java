package com.niz.threading;

import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.IntArray;
import com.badlogic.gdx.utils.Pool;
import com.badlogic.gdx.utils.Pools;
import com.niz.Component;
import com.niz.Entity;
import com.niz.EntityMessage;
import com.niz.Plane;
import com.niz.UIBatch;
import com.niz.Bus.BaseMessage;
import com.niz.verlet.IPhysics;
import com.niz.verlet.PhysicsEngine;


public class Messages {
	public static class OnSpawnMessage extends EntityMessage {

	}
	public static Pool<OnSpawnMessage> onSpawn = new Pool<OnSpawnMessage>(){
		@Override
		protected OnSpawnMessage newObject() {
			return new OnSpawnMessage();
		}
		
	};
	
	public static class PhysicsPreMessage extends EntityMessage{

		//public IPhysics physics;
	}
	public static Pool<PhysicsPreMessage> physicsPre = new Pool<PhysicsPreMessage>(){
		@Override
		protected PhysicsPreMessage newObject() {
			return new PhysicsPreMessage();
		}
		
	};
	public static class PhysicsPostMessage extends EntityMessage{

		//public IPhysics physics;
	}
	
	public static Pool<PhysicsPostMessage> physicsPost = new Pool<PhysicsPostMessage>(){
		@Override
		protected PhysicsPostMessage newObject() {
			return new PhysicsPostMessage();
		}
		
	};
	public static class PhysicsMessage extends EntityMessage{}
	
	public static Pool<PhysicsMessage> physics = new Pool<PhysicsMessage>(){
		@Override
		protected PhysicsMessage newObject() {
			return new PhysicsMessage();
		}
		
	};
	
	public static class CollideMessage extends EntityMessage{}
	
	public static Pool<PhysicsMessage> collide = new Pool<PhysicsMessage>(){
		@Override
		protected PhysicsMessage newObject() {
			return new PhysicsMessage();
		}
		
	};
	
	public static class TickMessage extends EntityMessage{}
	public static Pool<TickMessage> tick = new Pool<TickMessage>(){
		@Override
		protected TickMessage newObject() {
			return new TickMessage();
		}
		
	};
	
	public static class GraphTickMessage extends EntityMessage{}
	public static Pool<GraphTickMessage> graphTick = new Pool<GraphTickMessage>(){
		@Override
		protected GraphTickMessage newObject() {
			return new GraphTickMessage();
		}
		
	};
	
	public static class BucketsMessage extends EntityMessage{}
	public static Pool<BucketsMessage> buckets = new Pool<BucketsMessage>(){
		@Override
		protected BucketsMessage newObject() {
			return new BucketsMessage();
		}
		
	};
	
	public static class TickCollMessage extends EntityMessage{}
	public static Pool<TickCollMessage> tickColl = new Pool<TickCollMessage>(){
		@Override
		protected TickCollMessage newObject() {
			return new TickCollMessage();
		}
		
	};
	public static class TouchMessage extends EntityMessage{
		//float x, y;
		//int pointer, button;
		
		/*public TouchMessage set(float x, float y, int button, int pointer){
			this.x = x;
			this.y = y;
			
			this.pointer = pointer;
			this.button = button;
			return this;
		}*/
		IntArray data;
		public TouchMessage set(IntArray a){
			this.data = a;
			return this;
		}
		
	};
	public static class TouchDownMessage extends EntityMessage{
		public IntArray data;
		public TouchDownMessage set(IntArray a){
			this.data = a;
			return this;
		}
		
		
	}
	public static Pool<TouchDownMessage> touchDown = new Pool<TouchDownMessage>(){
		
		@Override
		protected TouchDownMessage newObject() {
			return new TouchDownMessage();
		}
		
	};
	
	public static class TouchUpMessage extends EntityMessage{

		public IntArray data;
		public TouchUpMessage set(IntArray a){
			this.data = a;
			return this;
		}}
	public static Pool<TouchUpMessage> touchUp = new Pool<TouchUpMessage>(){
		@Override
		protected TouchUpMessage newObject() {
			return new TouchUpMessage();
		}
		
	};
	
	public static class TouchFlingMessage extends EntityMessage{
		public boolean answered;

		IntArray data;
		public TouchFlingMessage set(IntArray a){
			this.data = a;
			return this;
		}
		public Vector3 v, w;
		
		public EntityMessage set(Entity ent, Vector3 tmpV, Vector3 tmpW) {
			
			v = tmpV;
			w = tmpW;
			return this;
		}}
	public static Pool<TouchFlingMessage> fling = new Pool<TouchFlingMessage>(){
		@Override
		protected TouchFlingMessage newObject() {
			return new TouchFlingMessage();
		}
		
	};
	
	public static class TouchDragStopMessage extends EntityMessage{
		public boolean answered;
		Entity e;
		IntArray data;
		public TouchDragStopMessage set(IntArray a){
			this.data = a;
			return this;
		}
		public Vector3 v;
		public EntityMessage set(Entity ent, Vector3 tmpV) {
			e = ent;
			v = tmpV;
			
			return this;
		}
	}
	public static Pool<TouchDragStopMessage> touchDragStop = new Pool<TouchDragStopMessage>(){
		@Override
		protected TouchDragStopMessage newObject() {
			return new TouchDragStopMessage();
		}
		
	};
	
	
	
	
	public static class TouchCancelMessage extends EntityMessage{}
	public static Pool<TouchCancelMessage> touchCancel = new Pool<TouchCancelMessage>(){
		@Override
		protected TouchCancelMessage newObject() {
			return new TouchCancelMessage();
		}
		
	};
	
	public static class ButtonMessage extends EntityMessage{}
	public static Pool<ButtonMessage> button = new Pool<ButtonMessage>(){
		@Override
		protected ButtonMessage newObject() {
			return new ButtonMessage();
		}
		
	};
	public static class TouchDragMessage extends EntityMessage{
		public boolean answered;
		Entity e;
		public Vector3 v, w;
//public IntArray data = new IntArray();
		public EntityMessage set(Entity ent, Vector3 tmpV, Vector3 tmpW) {
			e = ent;
			v = tmpV;
			w = tmpW;
			return this;
		}
	}

	public static Pool<TouchDragMessage> touchDrag = new Pool<TouchDragMessage>(){
		@Override
		protected TouchDragMessage newObject() {
			return new TouchDragMessage();
		}
		
	};
	
	
	
	
	public static class CollideQueryMessage extends EntityMessage{
		public boolean answered;
		public Vector3 v= new Vector3(), w = new Vector3();
		public Entity ret, e;
		public CollideQueryMessage set(Entity e, Vector3 v, Vector3 w){
			this.v.set(v);
			this.w.set(w);;
			answered = false;
			this.e = e;
			return this;
		}
	}
	public static Pool<CollideQueryMessage> collideQuery = Pools.get(CollideQueryMessage.class);
	public static CollideQueryMessage obtainCollideQuery(int collideType) {
		switch (collideType){
		case Plane.COLLIDE_TYPE_2D:return collideQuery.obtain();
		}
		return null;
	}
	
	
	public static void init(){
		//Component.registerMessageGroup(TouchDownMessage.class, Producer.GROUP_TOUCH);
		Component.registerMessageGroup(DrawUIMessage.class, Producer.GROUP_DRAW_UI);
		Component.registerMessageGroup(PhysicsPreMessage.class, Producer.GROUP_PHYSICS);
	//addToQueue(Messages.collide.obtain(), GROUP_PHYSICS);
		Component.registerMessageGroup(TickMessage.class, Producer.GROUP_TICK);
		//addToQueue(Messages.graphTick.obtain(), GROUP_GRAPHS);
		//addToQueue(Messages.physicsPost.obtain(), GROUP_PHYSICS);
		//addToQueue(Messages.buckets.obtain(), GROUP_BUCKETS);
	}
	
	
	public static class DrawUIMessage extends EntityMessage{
		public UIBatch batch;
		public DrawUIMessage set(UIBatch batch){
			this.batch = batch;
			return this;
		}
	}
	public static DrawUIMessage drawUI = new DrawUIMessage();
	
	public static class DrawMessage extends EntityMessage{
		
	}
	public static DrawMessage draw = new DrawMessage();
	
	
	public static class ClickMessage extends EntityMessage{
		
	}
	
	public static class ClickPool extends Pool<ClickMessage>{

		@Override
		protected ClickMessage newObject() {
			// TODO Auto-generated method stub
			return new ClickMessage();
		}
		
	}
	public static Pool<ClickMessage> click = new ClickPool();
	
	
	
	
	
	
	
}
