package com.niz.threading;

import com.badlogic.gdx.Gdx;

public class IdleProcessor implements Processable{
	private static final String TAG = "IdleProcessor";
	@Override
	public boolean process(){
		//Gdx.app.log(TAG, "idle");
		
		return true;
	}

	@Override
	public void onFinished(Producer producer, TickThread thread) {
		Processable p = producer.getNextProcess(thread.threadID);
		if (p != null){
			thread.run.set(p);
		}else {
			try {
				thread.sleep(5);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		
	}

}
