package com.niz.threading;

import java.io.DataOutputStream;
import java.io.IOException;

import com.niz.Chunk;
import com.niz.Plane;
import com.niz.compress.ChunkCompress;
import com.niz.compress.SimpleRLECompressor;

public class BlobCompressor implements Processable {
	//TODO cancel compression/saving if a chunk has been modified
	//private static final int BLOB_SIZE = 8;
	//private static final int BLOB_BITS = 1;
	ChunkCompress compressor = new SimpleRLECompressor();;
	boolean compressing;
	int x,y,z, progress;
	DataOutputStream stream;
	Plane plane;
	public BlobCompressor set(int x, int y, int z, DataOutputStream stream, Plane plane){
		this.x = x;
		this.y = y;
		this.z = z;
		this.stream = stream;
		this.plane = plane;
		compressor.startFileWrite(stream);
		progress = 0;
		
		writeFileAffix(stream);
		queueNextChunk();
		return this;
	}
	
	private void writeFileAffix(DataOutputStream stream) {
		
	}

	private void queueNextChunk() {
		Chunk chunk = plane.getChunkByBlobOffset(x, y, z, ++progress);
		while (chunk.modified && progress < plane.blobTotal){
			chunk = plane.getChunkByBlobOffset(x, y, z, ++progress);
		}
		if (progress < plane.blobTotal){
			compressor.startCompression(chunk.getBlockArray());
			compressing = true;
		} else{
			compressing = false;
		}
		
		
	}

	@Override
	public boolean process() {
		if (compressing){
			if (compressor.processCompression()){
				compressing = false;
				return false;
			}
			return false;
		} else {//writing file
			if (compressor.processFileWrite() == -1){
				
				if (progress >= plane.blobTotal){
					return true;
				}
				queueNextChunk();
				return false;
			} else {
				
				return false;
			}
			
			
		}
		
		
		
		//return false;
	}

	@Override
	public void onFinished(Producer producer, TickThread thread) {
		try {
			stream.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
