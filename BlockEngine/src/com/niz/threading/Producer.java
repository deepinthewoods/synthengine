package com.niz.threading;

import java.util.Comparator;
import java.util.concurrent.ConcurrentLinkedQueue;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.BinaryHeap;
import com.badlogic.gdx.utils.IntMap;
import com.badlogic.gdx.utils.IntMap.Entries;
import com.badlogic.gdx.utils.IntMap.Values;
import com.badlogic.gdx.utils.Pool;
import com.badlogic.gdx.utils.Pools;
import com.niz.Chunk;
import com.niz.Entity;
import com.niz.EntityMessage;
import com.niz.Map;
import com.niz.Plane;
import com.niz.UIBatch;
import com.niz.decals.DecalBatch;
import com.niz.threading.Messages.PhysicsPostMessage;
import com.niz.threading.Messages.PhysicsPreMessage;

public class Producer {
/*
 * manages lists(groups) of entities, wakes up/manages threads
 * 
 * 
 * 
 * 
 */
	
	public static final int GROUP_PHYSICS = 1;
	public static final int GROUP_TICK = 5;
	public static final int GROUP_GRAPHS = 4;
	public static final int GROUP_BUCKETS = 3
			, GROUP_DRAW_UI = 7;
	private static final String TAG = "producer";;
	Array<Array<IntMap<Entity>>> groupEntities = new Array<Array<IntMap<Entity>>>();
	Array<Array<Values<Entity>>> groupValues = new Array<Array<Values<Entity>>>();
	private int numberOfThreads;
	private int[][] threadTotals;
	//private int[] groupTotals;
	int[] nextThread;
	int pausedTotal=0;
	private TickThread[] thread;
	public static int QUEUE_MAX = 10;
	//EntityMessage[] queue = new EntityMessage[QUEUE_MAX];
	Array<Array<GroupMessageProcessor>> tickProcessables = new Array<Array<GroupMessageProcessor>>();//indexed by order
	private int tickProgress, tickMax = 10, tickProgressFine = 0;
	private int timeTick, maxTimeTick;
	//private boolean[] pause = new boolean[QUEUE_MAX];
	private Array<IntMap<Entity>> allGroupEntities = new Array<IntMap<Entity>>();
	private Array<Values<Entity>> allGroupValues = new Array<Values<Entity>>();;
	private IdleProcessor[] idleProcessors;
	public ChunkProcessable[] chunkProcessors;// = new Array<ChunkProcessable>();

	private Map map;
	public void setStatics(Map map){
		this.map = map;
	}
	
	//this is a test comment
	public void init(int numberOfGroups, int numberOfThreads, UIBatch uiBatch){
		
		idleProcessors = new IdleProcessor[numberOfThreads];
		this.numberOfThreads = numberOfThreads;
		for (int i = 0; i < numberOfGroups; i++){
			groupEntities.add(new Array<IntMap<Entity>>());
			groupValues.add(new Array<Values<Entity>>());
			
			allGroupEntities.add(new IntMap<Entity>());
			allGroupValues.add(new Values<Entity>(allGroupEntities.peek()));
			//Gdx.app.log(TAG, "va;"+allGroupEntities.size);
			for (int t = 0; t < numberOfThreads; t++){
				IntMap<Entity> map = new IntMap<Entity>();
				Values<Entity> vals = new Values<Entity>(map);
				groupEntities.get(i).add(map);
				groupValues.get(i).add(vals);
			}
		}
		chunkProcessors = new ChunkProcessable[numberOfThreads];
		threadTotals = new int[numberOfGroups][numberOfThreads];
		nextThread = new int[numberOfGroups];
		thread = new TickThread[numberOfThreads];
		for (int i = 0; i < numberOfThreads; i++){
			DelegatingRunnable run = new DelegatingRunnable(this);
			thread[i] = new TickThread(run, i);
			run.setThread(thread[i]);
			idleProcessors[i] = new IdleProcessor();
			run.set(idleProcessors[i]);
			thread[i].start();
			//thread[i].onPause();
			//pausedTotal++;
			chunkProcessors[i] = new ChunkProcessable();
			
		}
		//Gdx.app.log(TAG, "init"+allGroupEntities.size);
		//groupTotals = new int[numberOfGroups];
		queueEntityMessages(uiBatch);
	}
	
	
	//IPhysics physics = new JPhysicsEngine();
	private void queueEntityMessages(UIBatch uiBatch) {
		//physics.init();
		
		PhysicsPreMessage pre = Messages.physicsPre.obtain();
		//pre.physics = physics;
		addToQueue(pre, GROUP_PHYSICS);
		
		addToQueue(Messages.collide.obtain(), GROUP_PHYSICS);
		addToQueue(Messages.tick.obtain(), GROUP_TICK);
		addToQueue(Messages.graphTick.obtain(), GROUP_GRAPHS);
		
		PhysicsPostMessage post = Messages.physicsPost.obtain();
		//post.physics = physics;
		addToQueue(post, GROUP_PHYSICS);
		addToQueue(Messages.buckets.obtain(), GROUP_BUCKETS);
		
		this.addDrawProcess(Messages.drawUI.set(uiBatch), GROUP_DRAW_UI);
	}
	
	public void nextFrame(Map map){//pauses all threads and sets flag so they
		//Gdx.app.log(TAG, "next frame"+tickProgress);
		maxTimeTick++;
		wakeUpThreads();
		
	}


	private void wakeUpThreads() {
		for (int i = 0; i < numberOfThreads; i++){
			if (thread[i].isPaused()) thread[i].onResume();
		}
		
	}
	private Values<Entity> getValues(int group, int threadID){
		return groupValues.get(group).get(threadID);
	}
	
	/*public void addToQueue(EntityMessage m, boolean pause){
		queue[size] = m;
		this.pause[size] = pause;
		size++;
	}*/
	public void addToQueue(EntityMessage m, int group){
		Array<GroupMessageProcessor> a = new Array<GroupMessageProcessor>();
		for (int i = 0; i < numberOfThreads; i++){
			GroupMessageProcessor p = new GroupMessageProcessor(i);
			IntMap<Entity> vals = groupEntities.get(group).get(i);
			p.set(vals, m);
			
			a.add(p);
		} 
		Gdx.app.log(TAG, "queue group "+group);
		tickProcessables.add(a);
		//tickMax++;
	}
	
	
	public void addDrawProcess(EntityMessage mess, int group){
		//DrawProcessable p = new DrawProcessable();
		//Array<Processable> a = new Array<Processable>();
		//Array<DrawProcessable> a = new Array<DrawProcessable>();
		UIRenderer p = new UIRenderer(0);
		for (int i = 0; i < numberOfThreads; i++){
			
			Values<Entity> vals = groupValues.get(group).get(i);
			//p.set(vals, mess);
			
			p.addValues(vals);
		}
		p.setMessage(mess);
		drawProcesses.add(p);
		//tickProcessables.add(a);
		
		//p.set(allGroupValues.get(group), mess);
		
	}
	public void addEntity(Entity e, int group){
		//Gdx.app.log(TAG, "add e"+e.id+","+group);
		int thread = nextThread[group];
		threadTotals[group][thread]++;
		/*nextThread++;
		nextThread%=numberOfThreads;
		if (threadTotals[group][thread]<threadTotals[group][nextThread])
			nextThread = thread;*/
		int lowest = Integer.MAX_VALUE, lowIndex = 0;
		for (int i = 0; i < numberOfThreads; i++){
			if (threadTotals[group][i] < lowest){
				lowest = threadTotals[group][i];
				lowIndex = i;
			}
		}
		nextThread[group] = lowIndex;
		
		
		groupEntities.get(group).get(thread).put(e.id, e);
		allGroupEntities.get(group).put(e.id, e);
	}
	
	public void removeEntity(Entity e, int group){
		//Gdx.app.log(TAG, "remove e"+e.id+","+group);
		allGroupEntities.get(group).remove(e.id);
		for (int i = 0; i < numberOfThreads; i++){
			if (groupEntities.get(group).get(i).containsKey(e.id)){
				groupEntities.get(group).get(i).remove(e.id);
				return;
			}
		
		}
	}

	public synchronized void pauseThread(int threadID) {
		thread[threadID].onPause();
		if (++pausedTotal == numberOfThreads){
			for (int i = 0; i < numberOfThreads; i++){
				thread[i].onResume();
			}
		}
	}
	Array<DrawProcessable> drawProcesses = new Array<DrawProcessable>();
	
	public void draw(Camera camera, DecalBatch batch, UIBatch uiBatch, SpriteBatch spriteBatch){
		for (int i = 0; i < drawProcesses.size; i++){
			DrawProcessable p = drawProcesses.get(i);
			//for (int c = 0; c < numberOfThreads; c++){
			p.pre(camera, batch, null);
				//p.setValues(allGroupValues.get(c));
				
				
				//Gdx.app.log(TAG, "draw"+uiBatch.idx);
				
			p.process();
				
			//}
			p.post(camera, batch, uiBatch, spriteBatch);
		}
	}

	/*public void groupMessageFinished(TickThread thread) {
		//Gdx.app.log(TAG, "group message finished"+tickProgress);
		//either give it new Entities or pause/resume
		synchronized(this){
			if (tickProgress < tickProcessables.size &&
					tickProgressFine < tickProcessables.get(tickProgress).size){//if there are more p's in the queue, resuse this thread
				GroupMessageProcessor p = tickProcessables.get(tickProgress).get(tickProgressFine); 
				thread.run.set(p);
				p.reset();
				tickProgressFine++;
				//Gdx.app.log(TAG, "grmsg reuse");
				return;
			}
			
			if (tickProgress >= tickMax){//done ticking, set p to chunk or whatever processable
				//or loop if another tick needs to happen
				if (timeTick < maxTimeTick){//start the next tick
					timeTick++;
					tickProgress = 0;
					tickProgressFine = 0;
					map.physics.step();;
				//	Gdx.app.log(TAG, "grmsg retick");
				} else {
				//	Gdx.app.log(TAG, "grmsg idle");
					thread.run.set(getNextProcess(thread.threadID));
				}
				
			}
		
		//otherwise it will pause the thread/tick them over
		
			thread.onPause();
			//Gdx.app.log(TAG, "grmsg pause"+pausedTotal);
			if (++pausedTotal == numberOfThreads){//reset
				
				//Gdx.app.log(TAG, "grmsg unpause/reset"+pausedTotal);
				pausedTotal = 0;
				tickProgress++;
				tickProgressFine = 0;
				for (int i = 0; i < numberOfThreads; i++){
					//Processable p = tickProcessables.get(tickProgress).get(tickProgressFine); 
					this.thread[i].run.set(getNextProcess(thread.threadID));
					this.thread[i].onResume();
					
					
				}
			}
		}
	}*/
	
	BinaryHeap<ProcessableNode> heap = new BinaryHeap<ProcessableNode>();
	//heap holds stuff that needs to be processed ie chunkupdates, 
	
	
	
	ConcurrentLinkedQueue<Processable> inProgress = new ConcurrentLinkedQueue<Processable>()
			;
	ConcurrentLinkedQueue<Chunk> chunkQueue = new ConcurrentLinkedQueue<Chunk>();
	
	
	
	
	public synchronized Processable getNextProcess(int threadID) {
		//");
		
		if (tickIsFinishing){
			
		} else {
			if (tickProgress < tickProcessables.size 
					){//if there are more p's in the queue, resuse this thread
				GroupMessageProcessor p = tickProcessables.get(tickProgress).get(tickProgressFine); 
				//thread.run.set(p);
				tickProgressFine++;
				if (tickProgressFine < tickProcessables.get(tickProgress).size){
					
				} else {
					tickProgressFine = 0;
					tickProgress++;
					tickIsFinishing = true;
					//tickProcessedTotal = 0;
				}
				p.reset();
				//Gdx.app.log(TAG, "getnextp start" + tickProgress + " fine "+tickProgressFine);
				return p;
			}
			
			if (timeTick < maxTimeTick){
				tickProgress = 0;
				tickProgressFine = 0;
				tickProcessedTotal =0;
				timeTick++;
				map.physics.step();;
				return getNextProcess(threadID);//RECURSION will only go one level 
			//	Gdx.app.log(TAG, "next pr");
			}//else if (draw queued){predraw}
		}
		

		if (chunkQueue.isEmpty()){
			tryQueueChunkUpdates();
			//Processable p = queueChunkUpdates();//TODO this needs to know that all chunks are processed
			//if (p != null) return p;
			//
		}
		
		if (!inProgress.isEmpty()){
			Processable p = inProgress.poll();
			return p;
		}
		
		
		if (!chunkQueue.isEmpty()){
			ChunkProcessable p = chunkProcessors[threadID];
			p.set(chunkQueue.poll());
			//if (p != null) return p;
		}
		//Gdx.app.log(TAG, "getnextp middle"+threadID);

		if (heap.size > 0){
			ProcessableNode popped = heap.pop();
			Processable p = popped.p;
			popped.free();
			//Gdx.app.log(TAG, "getnextp END"+threadID);
			return p;
		}
		//Gdx.app.log(TAG, "getnextp null"+threadID);
		return idleProcessors[threadID];
		//return null;
		
	}
	
	private Array<Chunk> tmpChunks = new Array<Chunk>();
	private Comparator<Chunk> chunkCameraDistanceComparator = new Comparator<Chunk>(){

		@Override
		public int compare(Chunk arg0, Chunk arg1) {
			return arg0.distanceFromCamera() - arg1.distanceFromCamera();
		}
		
	};
	private int tickProcessedTotal = 0;
	boolean tickIsFinishing = false;
	
	private synchronized void tryQueueChunkUpdates() {
		//find [threadcount] closest chunks requiring updating, return closest	
		for (int i = 0; i < numberOfThreads; i++){
			if (chunkProcessors[i].c == null) return;
		}
		int maxReturnSize = numberOfThreads*2;
		tmpChunks.clear();
		Entries<Plane> planes = map.getPlanes();
		planes.reset();
		while (planes.hasNext()){
			Plane p = planes.next().value;
			com.badlogic.gdx.utils.LongMap.Values<Chunk> pChunks = p.getChunks();
			pChunks.reset();
			while (pChunks.hasNext()){
				Chunk c = pChunks.next();
				if (c.needsUpdate()){
					tmpChunks.add(c);
				}
			}
		}
		
		tmpChunks.sort(chunkCameraDistanceComparator);
		tmpChunks.truncate(maxReturnSize);
		
		for (int i = 0; i < tmpChunks.size; i++){
			//chunkProcessors[i].set(tmpChunks.get(i));
			//heap.add(ProcessableNode.obtain().set(chunkProcessors[i]));
			chunkQueue.add(tmpChunks.get(i));
		}
		
		//if (tmpChunks.size < 1){
		//	tryQueueChunkLoads();
		//}
		
	}
	
	/*private void tryQueueChunkLoads() {
		Entries<Plane> planes = map.getPlanes();
		planes.reset();
		int //limit = numberOfThreads,
				progress = 0;
		while (planes.hasNext()){// && progress < limit){
			Plane p = planes.next().value;
			while (p.hasNextLoad()){
				addChunkLoad(p.getNextLoad());
			}
		}
		
	}*/
	/*private static Pool<ChunkLoadProcessor> chunkLoadProcessorPool = Pools.get(ChunkLoadProcessor.class);
	private void addChunkLoad(Chunk c) {
		ChunkLoadProcessor chunkLoader = chunkLoadProcessorPool.obtain();
		chunkLoader.set(c);
		addProcessable(chunkLoader);
		
	}
	private void addProcessable(ChunkLoadProcessor p) {
		heap.add(ProcessableNode.obtain().set(p));
		
	}*/
	public boolean needsProcessorImmediately(){
		if ( (tickProgress < tickProcessables.size &&
				tickProgressFine < tickProcessables.get(tickProgress).size) || timeTick < maxTimeTick)
			return true;
		return false;
	}
	
	public void addAsset(Processable p) {
		heap.add(ProcessableNode.obtain().set(p));
		
	}
	public void holdProcessable(ChunkProcessable chunkProcessable) {
		inProgress.add(chunkProcessable);
		
	}
	public synchronized void onGroupMessageFinished() {
		tickProcessedTotal++;
		if (tickProcessedTotal == numberOfThreads){
			tickIsFinishing = false;
		}
		
	}
	
}
