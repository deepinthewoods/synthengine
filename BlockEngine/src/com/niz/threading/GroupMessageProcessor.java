package com.niz.threading;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.IntMap;
import com.badlogic.gdx.utils.IntMap.Values;
import com.niz.Component;
import com.niz.Entity;
import com.niz.EntityMessage;
/*
 * sends a message to every Entity in it's list.
 */
public class GroupMessageProcessor implements Processable {
	
	private static final String TAG = "groupMessageProcessor";
	private Values<Entity> entityValues;// = new Values<Entity>(entities); 
	private IntMap<Entity> entities;
	private int progress;
	protected EntityMessage m;
	int thread;
	
	public GroupMessageProcessor(int i) {
		thread = i;
		Gdx.app.log(TAG, "grp mes");
	}

	public void set(IntMap<Entity> entities, EntityMessage mess){
		entityValues = new Values<Entity>(entities);
		this.entities = entities;
		m = mess;
		//Gdx.app.log(TAG, "set");
		
	}
	
	public void reset(){
		progress = 0;
		//entityValues.reset();
		//Gdx.app.log(TAG, "reset"+thread+m);
	}
	
	@Override
	public boolean process() {
		
		synchronized (this){
			entityValues.reset();
			//Gdx.app.log(TAG, "  process"+  Component.getMessageGroup(m.getClass()));
			//Gdx.app.log(TAG, "proce res");
			while (entityValues.hasNext()){
				//if (!entityValues.hasNext())
					//Gdx.app.log(TAG, "process"+thread+m);
				
				Entity e = entityValues.next();
				
				e.bus.publish(m);
			}
			
		}
		return true;
	}

	public void setValues(Values<Entity> values) {
		this.entityValues = values;
		
	}

	@Override
	public void onFinished(Producer producer, TickThread thread) {
		producer.onGroupMessageFinished();
		Processable process = producer.getNextProcess(thread.threadID);
		
		thread.run.set(process );
	}
	
	
	
	
	
	
	
	
	
}
