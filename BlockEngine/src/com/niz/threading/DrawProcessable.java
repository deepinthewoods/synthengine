package com.niz.threading;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.IntMap.Values;
import com.niz.Entity;
import com.niz.UIBatch;

public abstract class DrawProcessable extends GroupMessageProcessor {
	
	public DrawProcessable(int i) {
		super(i);
		
	}




	public abstract void pre(Camera camera, com.niz.decals.DecalBatch batch, UIBatch uiBatch);
		
	

	
	public abstract void post(Camera camera, com.niz.decals.DecalBatch batch, UIBatch uiBatch, SpriteBatch spriteBatch);




	




	
	
	
}
