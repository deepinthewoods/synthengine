package com.niz.threading;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.utils.Pool;
import com.niz.Data;
import com.niz.verlet.PhysicsEngine;

public class FontProcessor implements Processable{
	public String fileName;
	public int progress;
	private BitmapFont font;
	
	static class FPool extends Pool<FontProcessor>{
		@Override
		protected FontProcessor newObject() {
			return new FontProcessor();
		}
	};
	private static FPool pool = new FPool();
	public static FontProcessor obtain(){
		return pool.obtain();
	}
	void free(){
		pool.free(this);
	}
	
	public FontProcessor set(String filename){
		font = null;
		fileName = filename;
		progress = 0;
		return this;
	}
	
	@Override
	public boolean process() {
		if (progress == 0){
			FileHandle fontFile = Gdx.files.internal("data/"+fileName+".fnt"), 
					imageFile = Gdx.files.internal("data/"+fileName+".png");
			font = new BitmapFont(fontFile, imageFile, false);
			//float fontScale =PhysicsEngine.ONE*64f;//font.get;
			
			//font.setScale(fontScale);
			font.setColor(Color.WHITE);
			
		}
		return true;
	}
	@Override
	public void onFinished(Producer producer, TickThread thread) {
		//thread.run.set(producer.getNextProcess(thread.threadID));
		Data.addFont(fileName, font);
		free();
		
	}
	
	
}
