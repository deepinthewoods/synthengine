package com.niz.threading;

import com.badlogic.gdx.utils.BinaryHeap;
import com.badlogic.gdx.utils.Pool;
import com.niz.threading.FontProcessor.FPool;

public class ProcessableNode extends BinaryHeap.Node{
	Processable p;
	public ProcessableNode(float value) {
		super(value);
		
	}
	static class FPool extends Pool<ProcessableNode>{
		@Override
		protected ProcessableNode newObject() {
			return new ProcessableNode(1f);
		}
	};
	private static FPool pool = new FPool();
	public static ProcessableNode obtain(){
		return pool.obtain();
	}
	void free(){
		pool.free(this);
	}
	public ProcessableNode set(Processable p2) {
		p = p2;
		return this;
	}
}
