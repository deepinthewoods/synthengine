package com.niz.threading;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.IntMap.Values;
import com.niz.Entity;
import com.niz.EntityMessage;
import com.niz.UIBatch;

public class UIRenderer extends DrawProcessable {

	public UIRenderer(int i) {
		super(i);
		
	}
	private static final String TAG = "ui renderer";
	private Array<Values<Entity>> groups = new Array<Values<Entity>> ();
	public void addValues(Values<Entity> vals) {
		groups.add(vals);
		
	}
	@Override
	public void pre(Camera camera, com.niz.decals.DecalBatch batch,
			UIBatch uiBatch) {
		
	}

	@Override
	public void post(Camera camera, com.niz.decals.DecalBatch batch,
			UIBatch uiBatch, SpriteBatch spriteBatch) {
		uiBatch.drawMesh(camera, spriteBatch);
		//Gdx.app.log(TAG, "batch render"+uiBatch.index);
		
	}

	@Override
	public boolean process() {
		int progress = 0;
		while (progress < groups.size){
			//Gdx.app.log(TAG, "groups");
			groups.get(progress).reset();
			super.setValues(groups.get(progress));
			super.process();
			progress++;
		}
		
		return true;
	}
	public void setMessage(EntityMessage mess) {
		m = mess;
		
	}

}
