package com.niz;

import java.util.Iterator;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.IntMap;
import com.badlogic.gdx.utils.Pool;
import com.badlogic.gdx.utils.Pools;


public class TagCollection {
	public static Pool<Tag> tagPool = Pools.get(Tag.class);
	public static Pool<TagCollection> tagCollectionPool = Pools.get(TagCollection.class);
	public static TagArrayPool tagArrayPool;
	
	private IntMap<Array<Tag>> tags = new IntMap<Array<Tag>>();
	
	//queries for types
	
	public void add(Tag tag){
		int key = getKey(tag.key);
		if (tags.containsKey(key)){
			tags.get(key).add(tag);
		}else {
			Array<Tag> a = tagArrayPool.obtain();
			a.add(tag);
			tags.put(key,  a);
		}
	}
	
	private int getKey(String s) {
		return s.hashCode();
	}

	public Array<Tag> query(int key){
		return tags.get(key);
	}
	
	public synchronized Array<Tag> query(int key, Array<Tag> ret, int... params){
		Array<Tag> a = tags.get(key);
		Iterator<Tag> i = a.iterator();
		while (i.hasNext()){
			Tag tag = i.next();
			boolean changed = false;
			for (int in = 0; in < params.length; in++){
				if (params[in] == -1){
					
				}else if (tag.values.get(in) != params[in]) {
					changed = true;
				}	
			}
			if (!changed){
				ret.add(tag);
			}
		}
		return ret;
	}








	
	
	class TagArrayPool extends Pool<Array<Tag>>{
		@Override
		protected Array<Tag> newObject() {
			return new Array<Tag>();
		}
	}
	
	
}
