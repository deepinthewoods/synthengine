package com.niz;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.IntArray;
import com.badlogic.gdx.utils.IntMap;
import com.badlogic.gdx.utils.IntMap.Entries;
import com.niz.components.Components;
import com.niz.components.Setting;
import com.niz.decals.DecalBatch;
import com.niz.threading.FontProcessor;
import com.niz.threading.Messages;
import com.niz.threading.Producer;
import com.niz.threading.Messages.TouchDownMessage;

public class SynthEngine implements ApplicationListener, InputProcessor {
	private String TAG = "engine";
	public static final float TICK_DELTA = 1f/120f, DRAW_DELTA = 1f/30f;
	public float tickAccumulator = 0, drawAccumulator = 0;
	
	private DecalBatch batch;
	private GL20 gl;
		
	private Map map;
	private UIBatch uiBatch;// = new UIBatch();
	private SpriteBatch spriteBatch;
	
	public int numberOfThreads = 1;
	private String[] initFiles = {"menu"};
	public static Producer producer;
	
	//public OrthographicCamera camera;
	
	public Sprite sprite;
	
	public static int uiTick = 0;
	
	public SynthEngine(int cores){
		numberOfThreads = cores;
	}
	
	public void preInit(){
		Data.requestData("menu");
		//animations, etc
		
	}
	
	public void postInit(){
		
		//map.loadPlane(Data.getPlane("menu"));
		
		for (int i = 0; i < initFiles.length; i++){
			//map.loadFrom(initFiles[i]);
		}
		
		
		//Component.setStaticValues();
		//CorneredSprite.makeLookup();
		
	}
	
	@Override
	public void create() {		
	//	new JniGenSharedLibraryLoader("libs/verlet-natives.jar").load("verlet");
		
		FontProcessor fp = FontProcessor.obtain().set("font16");
		fp.process();
		fp.onFinished(producer, null);
		
		//batch = new DecalBatch(new CameraGroupStrategy(null));
		uiBatch = new UIBatch();
		
		Messages.init();
		producer = new Producer();
		producer.init(10, numberOfThreads, uiBatch);
		map = new Map(numberOfThreads);
		producer.setStatics(map);
		Components.init();
		map.init();
		spriteBatch = new SpriteBatch();
		
		spriteBatch.setProjectionMatrix(map.uiPlane.getCamera().combined);
		//spriteBatch.
		//Plane displayPlane = Data.getPlane("initial");
		//map.addPlane(displayPlane);
		//set up rendering
		
		//producer.addAsset(FontProcessor.obtain().set("font16"));
		
		
		ColorData.init();
		
		preInit();
		
		sprite = new Sprite(new Texture(Gdx.files.internal("data/test.png")));
		sprite.setPosition(0, 0);
		sprite.setSize(.25f, .25f);
		
		Gdx.input.setInputProcessor(this);
		
		Preferences prefs = Gdx.app.getPreferences("prefs");
		Setting.initPrefs(prefs);
		Component.setStatics(map, producer);
		
	}
	
	
	
	public void set(BlockDefold[]  blockDefs){
		//Block.setDefs(blockDefs);
	}

	@Override
	public void dispose() {
		batch.dispose();
		uiBatch.dispose();
		//texture.dispose();
	}

	@Override
	public void render() {	
		//Gdx.app.log(TAG, "ffffffffffffffffffffffffffffffffffff");
		processTouchDowns();
		float delta = Gdx.graphics.getDeltaTime();
		tickAccumulator += delta;
		drawAccumulator += delta;
		while (tickAccumulator > TICK_DELTA){
			tickAccumulator -= TICK_DELTA;
			producer.nextFrame(map);
			uiTick++;
			//Gdx.app.log(TAG, "nextframe"+uiTick);
		}
		if (drawAccumulator > DRAW_DELTA){
			drawAccumulator -= DRAW_DELTA;
			Gdx.graphics.getGL20().glClearColor(0.2f, 0.2f, 0.2f, 1);
		    Gdx.graphics.getGL20().glClear(GL10.GL_COLOR_BUFFER_BIT);
		   // uiBatch.partialCircle(-PhysicsEngine.ONE/2, PhysicsEngine.ONE/3, PhysicsEngine.ONE/5, .1269f, 5, adjust2);
		    //uiBatch.textButton(0, 0, "TTTTTT", 1
		    //		, PhysicsEngine.ONE/2//max w
		    //		, PhysicsEngine.ONE//max h
		    //		, 12, 0, 0);
		   // uiBatch.drawButton( 0, 0
		   //		, PhysicsEngine.ONE/3//width
		   //		, PhysicsEngine.ONE/3//height
		    //		, adjust, 0, false);
		    
		   // uiBatch.drawButton( 0, PhysicsEngine.ONE/3
		   // 		, PhysicsEngine.ONE//width
		    //		, PhysicsEngine.ONE/8//height
		    //		, 2, adjust, false);
		    //uiBatch.drawButton(-PhysicsEngine.ONE/3, 0
		    //		, PhysicsEngine.ONE//width
		    //		, PhysicsEngine.ONE/8//height
		    //		, adjust, 3, false);
			producer.draw(map.uiPlane.getCamera(), batch, uiBatch, spriteBatch);
			//Gdx.app.log(TAG, "DRAWDRAWDRAW");
			
			
			
			
		}
		

		//uiPlane.getCamera().update();
		//Gdx.app.log(TAG, "draw"+map.uiPlane.getCamera().viewportWidth);
		//uiBatch.partialCircle(0,0,.051135f, .915f, Color.WHITE);
		
		
		
		
	}

	private void processTouchDowns() {
		touchEntries.reset();
		while (touchEntries.hasNext()){
			IntArray a = touchEntries.next().value;
			Entity e = map.getEntity(a.get(3));
			TouchDownMessage message = Messages.touchDown.obtain();
			message.set(a);
			e.bus.publish(message);
			a = null;
			Messages.touchDown.free(message);
		}
		
	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}
	int adjust, adjust2;
	@Override
	public boolean keyDown(int keycode) {
		switch (keycode){
		case Keys.L:adjust++;
		Gdx.app.log(TAG, "adjust"+adjust);
		break;
		case Keys.O:adjust--;
		Gdx.app.log(TAG, "adjust"+adjust);
		break;
		case Keys.K:adjust2++;
		Gdx.app.log(TAG, "adjust"+adjust);
		break;
		case Keys.I:adjust2--;
		Gdx.app.log(TAG, "adjust"+adjust);
		break;
		}
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}
	
	private IntMap<IntArray> touchMap = new IntMap<IntArray>();
	private Entries<IntArray> touchEntries = new Entries<IntArray>(touchMap);
	//hash is pointer id, intarray is x y button entityid
	private Vector3 tmpV = new Vector3();
	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		//collide
		
		Entity e = map.touchCollide(screenX, screenY);
		if (e == null) return false;
		if (touchMap.containsKey(pointer))
			Entity.intPool.free(touchMap.remove(pointer));
		IntArray a = Entity.intPool.obtain(4);
		a.set(0, screenX);
		a.set(1, screenY); 
		a.set(2, button);
		a.set(3, e.id);
		Gdx.app.log(TAG, "touchd"+e.id);
		touchMap.put(pointer, a);		
		//e.bus.publish(Messages.touchDown.obtain());
		return true;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		IntArray a = touchMap.get(pointer);
		if (a == null) return false;
		//Gdx.app.log(TAG, "touchUp"+pointer+touchMap.get(pointer));
		Entity e = map.getEntity(a.get(3));
		//tmpV.set(screenX, screenY, 0);
		//camera.unproject(tmpV);
		e.bus.publish(Messages.touchUp.obtain().set(a));
		Gdx.app.log(TAG, "touchup"+e.id);
		touchMap.remove(pointer);
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		//Gdx.app.log(TAG, "touchdrag"+pointer+touchMap.get(pointer));
		IntArray a = touchMap.get(pointer);
		if (a == null) return false;
		a.set(0, screenX);
		a.set(1, screenY);
	
		
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}
}
