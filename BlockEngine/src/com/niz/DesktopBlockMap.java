package com.niz;

import com.badlogic.gdx.utils.IntIntMap;
import com.badlogic.gdx.utils.IntIntMap.Values;
import com.badlogic.gdx.utils.Pool;
import com.badlogic.gdx.utils.Pools;

public class DesktopBlockMap extends BlockPositionMap {
	
	private IntIntMap map = new IntIntMap();
	static private Pool<DesktopBlockMap> pool = Pools.get(DesktopBlockMap.class);
	
	@Override
	public synchronized void add(int value) {
		map.put(value, value);

	}

	@Override
	public synchronized int pop() {
		Values vals = map.values();
		if (!vals.hasNext()) return -1;
		vals.reset();
		int v = vals.next();
		vals.remove();
		return v;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void free() {
		pool.free(this);
		
	}
	public static BlockPositionMap obtain(){
		return pool.obtain();
	}

	@Override
	public void clear() {
		map.clear();
		
	}

	@Override
	public boolean contains(long hash) {
		return map.containsKey((int) hash);
	}
}
