package com.niz;

public abstract class BlockPositionMap {
	
	public abstract void add(int hash);
	public abstract int pop();
	public abstract boolean isEmpty() ;
	public abstract void free();
	public abstract void clear();
	private static int posMask = 1023;
	public void add(int x, int y, int z) {
		add(x+(y<<10)+(z<<20));		
	}
	public abstract boolean contains(long key);
	
	
	public boolean contains(int x, int y, int z){
		return contains(x+(y<<10)+(z<<20));
	}
	
	public static int x(int i){
		return i & posMask;
	}
	
	public static int y(int i){
		return (i>>10) & posMask;
	}
	
	public static int z(int i){
		return (i>>20) & posMask;
	}
	public static int key(int x, int y, int z) {
		
		return x+(y<<10)+(z<<20);
	}
	
	

	
	
}
