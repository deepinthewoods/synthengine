package com.niz;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.VertexAttribute;
import com.badlogic.gdx.graphics.VertexAttributes.Usage;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.BitmapFont.TextBounds;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.FloatArray;
import com.badlogic.gdx.utils.IntArray;
import com.badlogic.gdx.utils.IntMap;
import com.badlogic.gdx.utils.IntMap.Values;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.ObjectMap.Entries;
import com.badlogic.gdx.utils.ObjectMap.Entry;
import com.niz.planes.UIPlane;
import com.niz.threading.Messages;
import com.niz.threading.Messages.DrawMessage;
import com.niz.verlet.PhysicsEngine;

public class UIBatch {
    private static final int MAX_VERTS = 1000
    		, CIRCLE_TYPES = 8, TEXT_BUTTON_TYPES = 32;
    public static final int edgeSize = 5;//.005f;

    private static final String TAG = "ui batch";
	private IntMap<ObjectMap<Camera, Array<Entity>>> meshes = new IntMap<ObjectMap<Camera, Array<Entity>>>();
	private Mesh mesh;
	//int value is textureID
	private Values<ObjectMap<Camera, Array<Entity>>> meshValues = new Values<ObjectMap<Camera, Array<Entity>>>(meshes);
    private ShaderProgram meshShader;
    private float[] verts = new float[MAX_VERTS];
    float[][] circle = new float[CIRCLE_TYPES][];
    public int index;
    private Array<String> queuedStrings = new Array<String>();
    private IntArray queuedStringData = new IntArray();
    private FloatArray queuedStringPositionData = new FloatArray();
   // private final Matrix4 transformMatrix = new Matrix4();
	//private final Matrix4 projectionMatrix = new Matrix4();
	//private final Matrix4 combinedMatrix = new Matrix4();
   // public static float fontScale =0f;// PhysicsEngine.ONE/128f;
    public UIBatch() {
    	
        createShader();
       // for (int i = 0; i < CIRCLE_TYPES; i++){
        //	circle[i] = initCircle(i+3);
        	//textButtons[i+1] = initHalfCircleButton(i+3);
        //}
       /* for (int i = 0; i < 31; i++){
        	//circle[i] = initCircle(i+3);
        	textButtons[i+1] = initConcaveHalfCircleButton(3+i, 3, .25f);//initHalfCircleButton(i+3, 1);//////
        }*/
        
        circle[0] = initCircle(3, false);
        circle[1] = initCircle(4, false);
        circle[2] = initCircle(5, false);
        circle[3] = initCircle(6, false);
        
        circle[4] = initCircle(3, true);
        circle[5] = initCircle(4, true);
        circle[6] = initCircle(5, true);
        circle[7] = initCircle(6, true);
        
        
        
        textButtons[0] = initTextButton();
        textButtons[1] = initAngledTextButton(.75f);
        textButtons[2] = initAngledTextButton(.5f);
        textButtons[3] = initAngledTextButton(.25f);
        
        textButtons[4] = initDoubleAngledTextButton(55, 3f);
        textButtons[5] = initDoubleAngledTextButton(45, 6);
        textButtons[6] = initDoubleAngledTextButton(30, 6);
        textButtons[7] = initDoubleAngledTextButton(20, 3f);
       
        
        
        textButtons[8] = initConcaveHalfCircleButton(3, 0, .33f);
        textButtons[9] = initConcaveHalfCircleButton(4, 0, .32f);
        textButtons[10] = initConcaveHalfCircleButton(5, 0, .32f);
        textButtons[11] = initConcaveHalfCircleButton(6, 0, .32f);
       
      
       
        
       
        
       
        mesh = createMesh(new float[] { -0.5f, -0.5f, Color.toFloatBits(0, 0, 255, 255),
		                 0.5f, -0.5f, Color.toFloatBits(0, 255, 0, 255),
		                 0f, 0.5f, Color.toFloatBits(255, 0, 0, 255) });
       // Gdx.app.log(TAG, "constructor");
        //throw new GdxRuntimeException("huh");
    }
 
    public Mesh createMesh(float[] vertices) {
       Mesh mesh = new Mesh(false, MAX_VERTS, 0,
                new VertexAttribute(Usage.Position, 2, "a_position")
        ,   new VertexAttribute(Usage.ColorPacked, 4, "a_color")  );
       	return mesh;
    }
    
    public synchronized void queue(Entity e, int textureID){
    	
    	ObjectMap<Camera, Array<Entity>> om = null;
    	if (meshes.containsKey(textureID)){
    		om = meshes.get(textureID);
    	} else {
    		om = new ObjectMap<Camera, Array<Entity>>();
    		meshes.put(textureID, om);
    	}
    	Camera cam = e.plane.getCamera();
    	Array<Entity> arr = null;
    	if (om.containsKey(cam)){
    		arr = om.get(cam);
    	} else {
    		arr = new Array<Entity>();
    		om.put(cam, arr);
    	}
    	arr.add(e);
    }
 
    public void drawMesh(Camera cam, SpriteBatch spriteBatch) {
        // this should be called in render()
    	meshValues.reset();
    	DrawMessage message = Messages.draw;
    	while (meshValues.hasNext()){
    		ObjectMap<Camera, Array<Entity>> objectMap = meshValues.next();
    		Entries vals = objectMap.entries();
    		while (vals.hasNext()){
    			Entry ent = vals.next();
    			Array<Entity> arr = (Array<Entity>) ent.value;
    			for (int i = 0; i < arr.size; i++){
    				Entity e = arr.get(i);
    				e.bus.publish(message);
    			}
    		}
    	}
	       
        meshShader.begin();
	    meshShader.setUniformMatrix("u_projTrans", cam.combined);
	    mesh.setVertices(verts, 0, index);
	    mesh.render(meshShader, GL20.GL_TRIANGLE_STRIP);
	    //mesh.render(meshShader, GL20.GL_LINE_STRIP);
	    meshShader.end();
      
        this.index = 0;
        drawStrings(cam, spriteBatch);
    }
    private void drawStrings(Camera camera, SpriteBatch spriteBatch) {
    	if (queuedStrings.size == 0) return;
    	//camera.viewportWidth = UIPlane.uiXRes*100;
    	//camera.viewportHeight = 100;
    	camera.update();
    	spriteBatch.setProjectionMatrix(camera.combined);
    	spriteBatch.begin();
		while (queuedStrings.size > 0){
			String s  = queuedStrings.pop();
			float scale = queuedStringData.pop();
			int 		fontID = queuedStringData.pop(),
					colorID = queuedStringData.pop();
			float y = queuedStringPositionData.pop(), 
					x = queuedStringPositionData.pop();
			BitmapFont font = Data.getFont(fontID);
			
			font.setColor(ColorData.getColor(colorID));
			font.setScale(scale);
			font.draw(spriteBatch, s, x, y);//x, y);
			//font.setColor(ColorData.getColor(colorID+1));
			//font.draw(spriteBatch, s, .0f, .5f);//x, y);
			
			//Gdx.app.log(TAG, "draw string "+scale+"  "+fontScale*scale +"  bounds w"+font.getBounds(s).width + " x"+x+"  y"+y);
		}
		spriteBatch.end();
		//camera.viewportWidth = UIPlane.uiXRes;
    	//camera.viewportHeight = 1;
	}

	//private Object indexLock = new Object();
    public void circle(float x, float y, float radius, int type, int c){
    	int 
    		a = (c>>>24)& 255
    		, g = (c>>16)& 255
    		, b = (c>>8)& 255
    		, r = c & 255;
    	
    	//index = 0;
    		
    	float col = Color.toFloatBits(r,g,b,a);;
    	Color.toFloatBits(r, g, b, a);
    	verts[index++] = circle[type][0]*radius+x;
		verts[index++] = circle[type][1]*radius+y;
		verts[index++] = Color.WHITE.toFloatBits();;
    	//synchronized (indexLock){
	    	for (int i = 0; i < circle.length-2; i+=3){
	    		verts[index++] = circle[type][i]*radius+x;
	    		verts[index++] = circle[type][i+1]*radius+y;
	    		verts[index++] = Color.WHITE.toFloatBits();;
	    	}
    	//}
	    	verts[index++] = verts[index-2];
	    	verts[index++] = verts[index-2];
	    	verts[index++] = verts[index-2];
    	Gdx.app.log(TAG, "circle("+x+y+radius+ ") "+index);;
    }
    
    public void partialCircle(float x, float y, float r, float delta, int type, int color){
    	float col = ColorData.getColor(color).toFloatBits();;
    	verts[index++] = circle[type][0]*r*(1)+x;
		verts[index++] = circle[type][1]*r*(1)+y;
		verts[index++] = col;
    	for (int i = 0; i < circle[type].length-2; i+=3){
    		boolean inner = (i % 2 == 0);
    		verts[index++] = circle[type][i]*r*(inner?delta:1)+x;
    		verts[index++] = circle[type][i+1]*r*(inner?delta:1)+y;
    		verts[index++] = col;
    		//Gdx.app.log(TAG, "circle("+verts[index-4]+" , "+verts[index-3]);;
    	}
    	verts[index++] = verts[index-4];
    	verts[index++] = verts[index-4];
    	verts[index++] = verts[index-4];
    }
    
    Vector2 v = new Vector2(), w = new Vector2();	
    
    float[][] textButtons = new float[TEXT_BUTTON_TYPES][];
    private float[] initTextButton(){
    	float[] verts = new float[18];
    	int n = 0;
    	verts[n++] = -1f;
    	verts[n++] = -.5f;
    	verts[n++] = Color.RED.toFloatBits();
    	verts[n++] = -1f;
    	verts[n++] = -.5f;
    	verts[n++] = Color.RED.toFloatBits();
    	
    	verts[n++] = -1f;
    	verts[n++] = .5f;
    	verts[n++] = Color.GREEN.toFloatBits();
    	
    	verts[n++] = 1f;
    	verts[n++] = -.5f;
    	verts[n++] = Color.BLUE.toFloatBits();
    	
    	verts[n++] = 1f;
    	verts[n++] = .5f;
    	verts[n++] = Color.ORANGE.toFloatBits();
    	
    	verts[n++] = 1f;
    	verts[n++] = .5f;
    	verts[n++] = Color.ORANGE.toFloatBits();
    	
    	//textButtons[0] = verts;
    	return verts;
    }
    private float[] initAngledTextButton(float depth){
    	float[] verts = new float[24];
    	int n = 0;
    	verts[n++] = -1f;
    	verts[n++] = -.5f;
    	verts[n++] = Color.RED.toFloatBits();
    	verts[n++] = -1f;
    	verts[n++] = -.5f;
    	verts[n++] = Color.RED.toFloatBits();
    	
    	//
    	verts[n++] = 1;
    	verts[n++] = -.5f;
    	verts[n++] = Color.GREEN.toFloatBits();
    	
    	
    	verts[n++] = -depth;
    	verts[n++] = 0;
    	verts[n++] = Color.GREEN.toFloatBits();
    	
    	verts[n++] = depth;
    	verts[n++] = 0;
    	verts[n++] = Color.BLUE.toFloatBits();

    	//
    	verts[n++] = -1f;
    	verts[n++] = .5f;
    	verts[n++] = Color.BLUE.toFloatBits();
    	
    	verts[n++] = 1f;
    	verts[n++] = .5f;
    	verts[n++] = Color.ORANGE.toFloatBits();
    	
    	verts[n++] = 1f;
    	verts[n++] = .5f;
    	verts[n++] = Color.ORANGE.toFloatBits();
    	
    	//textButtons[0] = verts;
    	return verts;
    }
    
    private float[] initDoubleAngledTextButton(float angle, float add){
    	float[] verts = new float[14*3];
    	float d = 1f/(add+2*MathUtils.cosDeg(angle));
    	float e = d*MathUtils.sinDeg(angle);
    	float f = d*MathUtils.cosDeg(angle);
    	e *= 2;
    	Gdx.app.log(TAG, "d"+d+"e"+e+"f"+f);
    	
    	int n = 0;
    	verts[n++] = -1f;
    	verts[n++] = .5f;
    	verts[n++] = Color.RED.toFloatBits();
    	verts[n++] = -1f;
    	verts[n++] = .5f;
    	verts[n++] = Color.RED.toFloatBits();
    	
    	
    	verts[n++] = 1;
    	verts[n++] = .5f;
    	verts[n++] = Color.GREEN.toFloatBits();
    	
    	//1
    	verts[n++] = -1f;
    	verts[n++] = .5f-d;
    	verts[n++] = Color.GREEN.toFloatBits();
    	
    	verts[n++] = 1f;
    	verts[n++] = .5f-d;
    	verts[n++] = Color.BLUE.toFloatBits();
    	
    	//2
    	verts[n++] = -(1f-e);
    	verts[n++] = .5f-d-f;
    	verts[n++] = Color.GREEN.toFloatBits();
    	
    	verts[n++] = 1f-e;
    	verts[n++] = .5f-d-f;
    	verts[n++] = Color.BLUE.toFloatBits();
    	
    	//3
    	verts[n++] = -(1f-e);
    	verts[n++] = -(.5f-d-f);
    	verts[n++] = Color.GREEN.toFloatBits();
    	
    	verts[n++] = 1f-e;
    	verts[n++] = -(.5f-d-f);
    	verts[n++] = Color.BLUE.toFloatBits();
    	
    	//4
    	verts[n++] = -(1f);
    	verts[n++] = -(.5f-d);
    	verts[n++] = Color.GREEN.toFloatBits();
    	
    	verts[n++] = 1f;
    	verts[n++] = -(.5f-d);
    	verts[n++] = Color.BLUE.toFloatBits();
    	
    	
    	//5
    	verts[n++] = -1f;
    	verts[n++] = -.5f;
    	verts[n++] = Color.BLUE.toFloatBits();
    	
    	verts[n++] = 1f;
    	verts[n++] = -.5f;
    	verts[n++] = Color.ORANGE.toFloatBits();
    	
    	verts[n++] = 1f;
    	verts[n++] = -.5f;
    	verts[n++] = Color.ORANGE.toFloatBits();
    	
    	//textButtons[0] = verts;
    	return verts;
    }
    
    
    	/*verts = new float[18];
    	n = 0;
    	
    	verts[n++] = -1f;
    	verts[n++] = -1f;
    	verts[n++] = 0f;
    	
    	verts[n++] = 1f;
    	verts[n++] = -1f;
    	verts[n++] = 0f;
    	
    	verts[n++] = -.5f;
    	verts[n++] = 0f;
    	verts[n++] = 0f;
    	
    	verts[n++] = .5f;
    	verts[n++] = 0f;
    	verts[n++] = 0f;
    	
    	verts[n++] = -1f;
    	verts[n++] = 1f;
    	verts[n++] = 0f;
    	
    	verts[n++] = 1f;
    	verts[n++] = 1f;
    	verts[n++] = 0f;
    	
    	//textButtons[1] = verts;
    	
    }*/
    
    private float[] initCircle(int subdivisions, boolean halfRotate) {
		float[] verts = new float[(subdivisions+1)*6];
		//circle = new float[subdivisions]
		float angle = 360f/subdivisions;
		v.set(0, 1f);
		if (halfRotate)
			v.rotate(angle/2);
		//w.set(0, .5f);
		float color = Color.WHITE.toFloatBits();
		int n = 0;
		//Gdx.app.log(TAG, "circ "+(++n));n--;
		for (int i = 0; i < subdivisions+1; i++){
			color = ((i%3)==0?Color.WHITE.toFloatBits():i%3==2?Color.RED.toFloatBits():Color.BLUE.toFloatBits());
			verts[n++] = v.x;
			verts[n++] = v.y;
			verts[n++] = color;
			
			verts[n++] = v.x;
			verts[n++] = v.y;
			verts[n++] = Color.GREEN.toFloatBits();
			
			v.rotate(angle);
			
		}
		
		return verts;
	}
    
    private float[] initHalfCircleButton(int subdivisions, float offsetX, float height) {
		//float[] verts = new float[(subdivisions/2+4)*6];
		float color = Color.WHITE.toFloatBits();
		v.set(offsetX, height);
		float angle = v.angle()*2;
		angle /= subdivisions;
		angle += -1;
		offsetX *= -1;
		float[] heights = new float[99], widths = new float[99];
		int heightCount = (subdivisions+1)/2;
		boolean needsMiddle = subdivisions % 2 == 0;
		float[] verts = new float[( heightCount*4+3+(needsMiddle?3:0) )*6];
		for (int i = 0; i < heightCount; i++){
			heights[i] = v.y;
			widths[i] = v.x;
			v.rotate(-angle);
		}
		int n = 0;
		verts[n++] = widths[0]+offsetX;
		verts[n++] = heights[0];
		verts[n++] = color;
		for (int i = 0; i < heightCount; i++){	
			color = (i>2?Color.WHITE.toFloatBits():Color.RED.toFloatBits());
			//offsetX = ;
			verts[n++] = (offsetX + widths[i]);//+offsetX;
			verts[n++] = heights[i];
			verts[n++] = color;
			
			verts[n++] = (offsetX + widths[i]);//+offsetX;
			verts[n++] = .5f;
			verts[n++] = Color.GREEN.toFloatBits();
		}
		
		for (int i = 0; i < heightCount; i++){			
			verts[n++] = -(widths[heightCount-1-i]+offsetX);
			verts[n++] = heights[heightCount-1-i];
			verts[n++] = color;
			
			verts[n++] = -(widths[heightCount-1-i]+offsetX);
			verts[n++] = .5f;
			verts[n++] = Color.GREEN.toFloatBits();
		}
		verts[n++] = verts[n-4];
    	verts[n++] = verts[n-4];
    	verts[n++] = verts[n-4];
    	//under side
    	verts[n++] = widths[0]+offsetX;
		verts[n++] = -heights[0];
		verts[n++] = color;
		for (int i = 0; i < heightCount; i++){	
			color = (i>0?Color.WHITE.toFloatBits():Color.RED.toFloatBits());
			verts[n++] = widths[i]+offsetX;
			verts[n++] = -heights[i];
			verts[n++] = color;
			
			verts[n++] = widths[i]+offsetX;
			verts[n++] = -.5f;
			verts[n++] = Color.GREEN.toFloatBits();
		}
		
		for (int i = 0; i < heightCount; i++){			
			verts[n++] = -(widths[heightCount-1-i]+offsetX);
			verts[n++] = -heights[heightCount-1-i];
			verts[n++] = color;
			
			verts[n++] = -(widths[heightCount-1-i]+offsetX);
			verts[n++] = -.5f;
			verts[n++] = Color.GREEN.toFloatBits();
		}
		verts[n++] = verts[n-4];
    	verts[n++] = verts[n-4];
    	verts[n++] = verts[n-4];
		float offsetY = 0;
		if (needsMiddle){//middle patch
			float middleX = 1 - offsetX*2;;
			verts[n++] = middleX;
	    	verts[n++] = offsetY;
	    	verts[n++] = color;
	    	verts[n++] = middleX;
	    	verts[n++] = offsetY;
	    	verts[n++] = color;
	    	
	    	verts[n++] = middleX;
	    	verts[n++] = -offsetY;
	    	verts[n++] = color;
	    	
	    	verts[n++] = -middleX;
	    	verts[n++] = offsetY;
	    	verts[n++] = color;
	    	
	    	verts[n++] = -middleX;
	    	verts[n++] = -offsetY;
	    	verts[n++] = color;
	    	verts[n++] = -middleX;
	    	verts[n++] = -offsetY;
	    	verts[n++] = color;
		}
    	
		
		
		//Gdx.app.log(TAG, "half circ"+verts.length + " "+n);
		return verts;
	}
    
    private float[] initConcaveHalfCircleButton(int subdivisions, int skip, float len) {
    
		
		float angle = 360f/subdivisions, color = Color.WHITE.toFloatBits() ;
		v.set(len, 0).rotate(angle/2f);
		for (int i = 0; i < skip; i++)
			v.rotate(angle);
		float offsetX = 1f-v.x, offsetY = v.y;
		int n = 0;
		
		
		
		float[] heights = new float[99], widths = new float[99];
		int heightCount = (subdivisions-1)/2+1-skip;
		boolean needsMiddle = (subdivisions % 2) %2 == 0;
		float[] verts = new float[( heightCount*4+3+(needsMiddle?3:0) )*6];
		for (int i = 0; i < heightCount; i++){
			heights[i] = v.y;
			widths[i] = v.x;
			v.rotate(angle);
		}
		
		verts[n++] = widths[0]+offsetX;
		verts[n++] = heights[0];
		verts[n++] = color;
		for (int i = 0; i < heightCount; i++){	
			color = (i>0?Color.BLUE.toFloatBits():Color.RED.toFloatBits());
			verts[n++] = widths[i]+offsetX;
			verts[n++] = heights[i];
			verts[n++] = color;
			
			verts[n++] = widths[i]+offsetX;
			verts[n++] = .5f;
			verts[n++] = Color.GREEN.toFloatBits();
		}
		color = Color.WHITE.toFloatBits();
		for (int i = 0; i < heightCount; i++){			
			verts[n++] = -(widths[heightCount-1-i]+offsetX);
			verts[n++] = heights[heightCount-1-i];
			verts[n++] = color;
			
			verts[n++] = -(widths[heightCount-1-i]+offsetX);
			verts[n++] = .5f;
			verts[n++] = Color.GREEN.toFloatBits();
		}
		verts[n++] = verts[n-4];
    	verts[n++] = verts[n-4];
    	verts[n++] = verts[n-4];
		//under side
    	verts[n++] = widths[0]+offsetX;
		verts[n++] = -heights[0];
		verts[n++] = color;
		for (int i = 0; i < heightCount; i++){	
			color = (i>0?Color.WHITE.toFloatBits():Color.RED.toFloatBits());
			verts[n++] = widths[i]+offsetX;
			verts[n++] = -heights[i];
			verts[n++] = color;
			
			verts[n++] = widths[i]+offsetX;
			verts[n++] = -.5f;
			verts[n++] = Color.GREEN.toFloatBits();
		}
		
		for (int i = 0; i < heightCount; i++){			
			verts[n++] = -(widths[heightCount-1-i]+offsetX);
			verts[n++] = -heights[heightCount-1-i];
			verts[n++] = color;
			
			verts[n++] = -(widths[heightCount-1-i]+offsetX);
			verts[n++] = -.5f;
			verts[n++] = Color.GREEN.toFloatBits();
		}
		verts[n++] = verts[n-4];
    	verts[n++] = verts[n-4];
    	verts[n++] = verts[n-4];
		
		if ( needsMiddle){//middle patch
			float middleX = offsetX*2-1;;
			verts[n++] = middleX;
	    	verts[n++] = offsetY;
	    	verts[n++] = color;
	    	verts[n++] = middleX;
	    	verts[n++] = offsetY;
	    	verts[n++] = color;
	    	
	    	verts[n++] = middleX;
	    	verts[n++] = -offsetY;
	    	verts[n++] = color;
	    	
	    	verts[n++] = -middleX;
	    	verts[n++] = offsetY;
	    	verts[n++] = color;
	    	
	    	verts[n++] = -middleX;
	    	verts[n++] = -offsetY;
	    	verts[n++] = color;
	    	verts[n++] = -middleX;
	    	verts[n++] = -offsetY;
	    	verts[n++] = color;
		}
		
		for (int i = 0; i < (subdivisions-1)/2; i++){
			
			
			v.rotate(angle);
		}
		
		//verts[n++] = verts[n-4];
    	//verts[n++] = verts[n-4];
    	//verts[n++] = verts[n-4];
		//Gdx.app.log(TAG, "half circ"+verts.length + " "+n);
		return verts;
	}
    
    private void createShader() {
        // this shader tells opengl where to put things
        String vertexShader =
        		"attribute vec4 a_position;    \n"
                + "attribute vec4 a_color;       \n"
        				
                + "uniform mat4 u_projTrans;\n" //
                + "varying vec4 v_color;         \n"
                + "void main()                   \n"
                + "{                             \n"
                + "   v_color = a_color;         \n"
                + "   gl_Position = u_projTrans * a_position;  \n"
                + "}"
                ;
 
        // this one tells it what goes in between the points (i.e
        // colour/texture)
        String fragmentShader = 
        		  "#ifdef GL_ES                \n"
                + "precision mediump float;    \n"
                + "#endif                      \n"
                + "varying vec4 v_color;       \n"
                + "void main()                 \n"
                + "{                           \n"
                + "  gl_FragColor = v_color;   \n"
                + "} ";
 
        // make an actual shader from our strings
        meshShader = new ShaderProgram(vertexShader, fragmentShader);
 
        // check there's no shader compile errors
        if (meshShader.isCompiled() == false)
            throw new IllegalStateException(meshShader.getLog());
    }
    public void dispose() {
        mesh.dispose();
        meshShader.dispose();
    }

    public void getTextButtonData(BusData data){
    	
    }
   
    public void drawButton(float x, float y, float width, float height, int style, int colorID, boolean paddedX){
    	float[] vs = textButtons[style];
    	float col = ColorData.getColor(colorID).toFloatBits();
    	//height *= 2;
    	boolean stretchX = width > height*2;
    	float multiplier = 2// = ( stretchX?height:height ) /2
    			, additionalWidth = 0// = ( width-multiplier )/2
    			, additionalHeight = 0;// = 0;//Math.abs(height-multiplier/2);
    	if (stretchX){
    		multiplier =( height*2 ) ;
    		additionalWidth =( width - multiplier ) / 2;
    		additionalHeight =(  height - multiplier/2  ) / 2;
    		multiplier /= 2;
    	} else {
    		multiplier = width;
    		additionalWidth =0;
    		additionalHeight =( height - multiplier/2  ) / 2;
    		
    		multiplier /= 2;
    	}
  
		boolean left = vs[0] < 0f, topOrBottom , top; 
		
		 
		 for (int i = 0; i < vs.length-1; i+=3){
			left = vs[i] < 0f; 
			topOrBottom = Math.abs(vs[i+1]) > .499975f;
			top = vs[i+1] > 0f;
			verts[index++] = vs[i]*multiplier+x+ (left?-additionalWidth:additionalWidth);
			verts[index++] = vs[i+1]*multiplier+y + (topOrBottom?(top?additionalHeight:-additionalHeight):0);
			verts[index++] = col;//vs[i+2];//col;
			//Gdx.app.log(TAG, "box "+(verts[index-3])/(float)PhysicsEngine.ONE+" , "+(verts[index-2]/(float)PhysicsEngine.ONE));
			
		}
	
		//verts[index++] = verts[index-4];
	    //verts[index++] = verts[index-4];
	    //verts[index++] = verts[index-4];
    }
    public void drawPolygon(float x, float y, int width, int height, int style,
			int colorID) {
		this.drawButton(x, y, width, height, style, colorID, false);
		
	}

	public void drawTextButton(float x, float y
			, float width, float height, float scale
			, String string
			, int style
			, int colorID, int textColorID, int fontID){
    	queueString(x-width/2+edgeSize/2, y+height/2-edgeSize/2, string, textColorID, fontID, scale);
		
    	drawButton(x, y, width, height, style, colorID, true);
		//Gdx.app.log(TAG, "drawtextbutton "+y);
    }
	public static void textButtonBounds(String string
			, float maxWidth
			, int fontID, Vector3 returnBounds) {
		//int height, width;
		BitmapFont font = Data.getFont(fontID);
		//font.setScale(fontScale);
		TextBounds bounds ;//= font.getBounds(string);
		
		bounds = font.getBounds(string);
		float multiplier =  bounds.height, additionalWidth= bounds.width;
		
		int scale = 1;
	
		while (additionalWidth*scale + multiplier*scale < maxWidth/2-edgeSize*2){// && multiplier*scale < maxHeight/2){
			scale *= 2;
			
		}
		
		returnBounds.set(bounds.width*scale, bounds.height*scale, scale);
		Gdx.app.log(TAG, "textbox "+bounds.width*scale+string+" scale"+scale + "  maxw"+(additionalWidth*scale + multiplier*scale) + "edges"+edgeSize);
		Gdx.app.log(TAG, "textbox    "+returnBounds.x+"   "+returnBounds.y+"      "+returnBounds.z);
 	}

	public void queueString(float x, float y, String string, int textColorID, int fontID, float scale) {
		queuedStrings.add(string);
		queuedStringPositionData.add(x);
		queuedStringPositionData.add(y);
		queuedStringData.add( textColorID );
		queuedStringData.add( fontID );
		queuedStringData.add((int) (scale));
	}

	public void invButton(int x, int y, int index) {
		
	}
 
}