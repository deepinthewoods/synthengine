package com.niz;

import com.badlogic.gdx.utils.Pool;

public class BlockLoc {
	public int x,y,z;
	public static Pool<BlockLoc> pool = new Pool<BlockLoc>(){
	
		@Override
		protected BlockLoc newObject() {
			return new BlockLoc();
		}
		
	};
	
	public void set(int x2, int y2, int z2) {
		x = x2;
		y = y2;
		z = z2;
	}
	
	public void free(){
		pool.free(this);
	}

}
