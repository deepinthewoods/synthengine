/*******************************************************************************
 * Copyright 2011 See AUTHORS file.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package com.niz.decals;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Sort;

/** <p>
 * Minimalistic grouping strategy useful for orthogonal scenes where the camera faces the negative z axis. Handles enabling and
 * disabling of blending and uses world-z only front to back sorting for transparent decals.
 * </p>
 * <p>
 * States (* = any, EV = entry value - same as value before flush):<br/>
 * <table>
 * <tr>
 * <td></td>
 * <td>expects</td>
 * <td>exits on</td>
 * </tr>
 * <tr>
 * <td>glDepthMask</td>
 * <td>true</td>
 * <td>EV | true</td>
 * </tr>
 * <tr>
 * <td>GL_DEPTH_TEST</td>
 * <td>enabled</td>
 * <td>EV</td>
 * </tr>
 * <tr>
 * <td>glDepthFunc</td>
 * <td>GL_LESS | GL_LEQUAL</td>
 * <td>EV</td>
 * </tr>
 * <tr>
 * <td>GL_BLEND</td>
 * <td>disabled</td>
 * <td>EV | disabled</td>
 * </tr>
 * <tr>
 * <td>glBlendFunc</td>
 * <td>*</td>
 * <td>*</td>
 * </tr>
 * <tr>
 * <td>GL_TEXTURE_2D</td>
 * <td>*</td>
 * <td>disabled</td>
 * </tr>
 * </table>
 * </p> */
public class SimpleOrthoGroupStrategy implements GroupStrategy {
	private Comparator comparator = new Comparator();
	private MaterialComparator materialComparator = new MaterialComparator();
	private static final int GROUP_OPAQUE = 0;
	private static final int GROUP_BLEND = 1;

	@Override
	public int decideGroup (Decal decal) {
		return decal.getMaterial().isOpaque() ? GROUP_OPAQUE : GROUP_BLEND;
	}

	@Override
	public void beforeGroup (int group, Array<Decal> contents) {
		if (group == GROUP_BLEND) {
			Sort.instance().sort(contents, comparator);
			Gdx.gl20.glEnable(GL20.GL_BLEND);
			// no need for writing into the z buffer if transparent decals are the last thing to be rendered
			// and they are rendered back to front
			Gdx.gl20.glDepthMask(false);
		} else {
			contents.sort(materialComparator);
			
		}
	}

	@Override
	public void afterGroup (int group) {
		if (group == GROUP_BLEND) {
			Gdx.gl20.glDepthMask(true);
			Gdx.gl20.glDisable(GL10.GL_BLEND);
		}
	}

	@Override
	public void beforeGroups () {
		Gdx.gl20.glEnable(GL10.GL_TEXTURE_2D);
	}

	@Override
	public void afterGroups () {
		Gdx.gl20.glDisable(GL10.GL_TEXTURE_2D);
	}

	class Comparator implements java.util.Comparator<Decal> {
		@Override
		public int compare (Decal a, Decal b) {
			if (a.getZ() == b.getZ()) return 0;
			return a.getZ() - b.getZ() < 0 ? -1 : 1;
		}
	}
	class MaterialComparator implements java.util.Comparator<Decal> {
		@Override
		public int compare (Decal a, Decal b) {
			int hashA = a.getMaterial().textureRegion.getTexture().hashCode() , hashB = b.getMaterial().textureRegion.getTexture().hashCode();
			if (hashA == hashB){
				return a.getZ() - b.getZ() < 0 ? 1 : -1;//opposite of the transparent ones' order
			}
			return hashA
					- hashB < 0?-1:1;
			//if (a.getZ() == b.getZ()) return 0;
			//return a.getZ() - b.getZ() < 0 ? -1 : 1;
		}
	}
	@Override
	public ShaderProgram getGroupShader (int group) {
		if (group == GROUP_BLEND){
			
			
			String vertexShader = "attribute vec4 " + ShaderProgram.POSITION_ATTRIBUTE + ";\n" //
					+ "attribute vec4 " + ShaderProgram.COLOR_ATTRIBUTE + ";\n" //
					+ "attribute vec2 " + ShaderProgram.TEXCOORD_ATTRIBUTE + "0;\n" //
					+ "uniform mat4 u_projectionViewMatrix;\n" //
					+ "varying vec4 v_color;\n" //
					+ "varying vec2 v_texCoords;\n" //
					+ "\n" //
					+ "void main()\n" //
					+ "{\n" //
					+ "   v_color = " + ShaderProgram.COLOR_ATTRIBUTE + ";\n" //
					+ "   v_texCoords = " + ShaderProgram.TEXCOORD_ATTRIBUTE + "0;\n" //
					+ "   gl_Position =  u_projectionViewMatrix * " + ShaderProgram.POSITION_ATTRIBUTE + ";\n" //
					+ "}\n";
				String fragmentShader = "#ifdef GL_ES\n" //
					+ "precision mediump float;\n" //
					+ "#endif\n" //
					+ "varying vec4 v_color;\n" //
					+ "varying vec2 v_texCoords;\n" //
					+  "lowp vec4 v_tex_color;\n"
					+ "uniform sampler2D u_texture;\n" //
					+ "void main()\n"//
					+ "{\n" //
					 + "  v_tex_color = texture2D(u_texture, v_texCoords);\n" 
      				 + "  if (v_tex_color.a < 0.1) discard;\n" 
					+ "  gl_FragColor = v_color * texture2D(u_texture, v_texCoords);\n" //
					+ "}";

			ShaderProgram shader = new ShaderProgram(vertexShader, fragmentShader);
			if (shader.isCompiled() == false) throw new IllegalArgumentException("couldn't compile shader: " + shader.getLog());
			return shader;
		} else {
			String vertexShader = "attribute vec4 a_position;    \n" + 
                    "attribute vec4 a_color;\n" +
                    "attribute vec2 a_texCoord0;\n" + 
                    "uniform mat4 u_worldView;\n" + 
                    "varying vec4 v_color;" + 
                    "varying vec2 v_texCoords;" + 
                    "void main()                  \n" + 
                    "{                            \n" + 
                    "   v_color = vec4(1, 1, 1, 1); \n" + 
                    "   v_texCoords = a_texCoord0; \n" + 
                    "   gl_Position =  u_worldView * a_position;  \n"      + 
                    "}                            \n" ;
			String fragmentShader = "#ifdef GL_ES\n" +
                      "precision mediump float;\n" + 
                      "#endif\n" + 
                      "varying vec4 v_color;\n" + 
                      "varying vec2 v_texCoords;\n" + 
                      "uniform sampler2D u_texture;\n" + 
                      "void main()                              \n" + 
                      "{                                            \n" + 
      				  "  gl_FragColor = v_color * texture2D(u_texture, v_texCoords);\n" +
                      "}";

			ShaderProgram shader = new ShaderProgram(vertexShader, fragmentShader);
			if (shader.isCompiled() == false) throw new IllegalArgumentException("couldn't compile shader: " + shader.getLog());
			return shader;
		}
		
	}
}
