package com.niz.decals;

import com.badlogic.gdx.utils.IntArray;
import com.niz.Entity;
import com.niz.graphs.Node;
import com.niz.graphs.NodeComponent;
import com.niz.threading.Messages.TickMessage;

public class Frame extends NodeComponent {
	public static final int FRAME_ID = 0, OUT_FRAME_ID = 0;
	public Frame() {
		super("frame");
	}
	
	public void tick(TickMessage m, IntArray data, Entity e){
		data.set(FRAME_ID, e.getStateFrameInfo().getFrameIndex(e.stateTick));
	}
	
	public void process(Node node) {
		node.outputEdges.get(OUT_FRAME_ID).
	}

}
