package com.niz;

import com.badlogic.gdx.utils.IntArray;

public interface PeriodicUpdate {
	public static final int END_CODE = -1000000;
	public int getPeriodicInterval(IntArray Data);
	public int updatePeriodic(Entity e, IntArray data);
	
}

