package com.niz.compress;

import java.io.IOException;

public class SimpleRLECompressor extends ChunkCompress {
	int value, run;
	boolean first;
	
	
	
	private void writeRunValuePair() {
		data.add(run);
		data.add(value);
	}

	@Override
	public void startCompression(int[] chunk){
		super.startCompression(chunk);
		first = true;
		writeProgress = 0;
	}
	
	@Override
	public void addBlock(int block) {
		if (first){
			run = 1;
			value = block;
		} else {
			
			if (block == value)run++;
			else {
				writeRunValuePair();
				run = 1;
				value = block;
			}
			
		}
	}
	
	
	
	
	@Override
	public void stopCompression() {
		writeRunValuePair();
	}
	
	private int writeProgress;
	
	@Override
	public int processFileWrite() {
		if (data.size == 0) return -1;
		try {
			stream.writeInt(data.get(writeProgress++));
			stream.writeInt(data.get(writeProgress++));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return 0;
	}


}
