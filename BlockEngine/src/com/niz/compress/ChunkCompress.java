package com.niz.compress;

import java.io.DataOutputStream;

import com.badlogic.gdx.utils.IntArray;

public abstract class ChunkCompress {
	
	public IntArray data = new IntArray();
	DataOutputStream stream;
	int[] chunk;
	int compressionProgress;
	public void startCompression(int[] chunk){
		this.chunk = chunk;
		data.clear();
		compressionProgress = 0;
	}
	
	private final static int COMPRESSION_REPS = 8;
	public boolean processCompression(){
		for (int i = 0; i < COMPRESSION_REPS; i++){
			addBlock( chunk[compressionProgress++]);
		}
		
		if (compressionProgress >= chunk.length){
			stopCompression();
			return true;
			
		}
		return false;
	}
	
	public abstract void stopCompression();

	public abstract void addBlock(int block);
	
	public void startFileWrite(DataOutputStream stream){
		this.stream = stream;
	}
	
	public abstract int processFileWrite();
	



}
