package com.niz.compress;

import java.io.DataInputStream;
import java.io.IOException;

public class SimpleRLEDecompressor extends ChunkDecompress {
	private static final int REPETITIONS = 32;
	int value, run, runProgress, processProgress, total_blocks;
	@Override
	public void startReading(DataInputStream stream, int[] chunk, int totalBlocks) {
		run = 0;
		total_blocks = totalBlocks;
	}
	
	@Override
	public boolean processReading() {
		for (int i = 0; i < REPETITIONS; i++){
			if (processProgress >= total_blocks){
				return true;
			}
			
			if (run == 0){//read values
				
				try {
					run = stream.readInt();
					value = stream.readInt();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}//end read values
			chunk[processProgress++] = value;
			run--;
		}

		return false;
	}

}
