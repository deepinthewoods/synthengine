package com.niz.compress;

import java.io.DataInputStream;

public abstract class ChunkDecompress {
protected DataInputStream stream;
protected int[] chunk;
public void startReading(DataInputStream stream, int[] chunk, int totalBlocks){
	this.stream = stream;
	this.chunk = chunk;
}
public abstract boolean processReading();



}