package com.niz;

public abstract class Queue<T> {

public abstract void add(T data);
public abstract T pop();
public abstract T peek();
public abstract boolean isEmpty();
	

}
