package com.niz;

import java.util.concurrent.ConcurrentLinkedQueue;

public class AndroidQueue extends Queue<BlockLoc> {
private ConcurrentLinkedQueue<BlockLoc> list = new ConcurrentLinkedQueue<BlockLoc>();
	@Override
	public void add(BlockLoc data) {
		list.add(data);
		
	}

	@Override
	public BlockLoc pop() {
		return list.remove();
	}

	@Override
	public BlockLoc peek() {
		return list.peek();
	}

	@Override
	public boolean isEmpty() {
		return list.isEmpty();
	}

	

}
