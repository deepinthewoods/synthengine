package com.niz;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.IntArray;
import com.badlogic.gdx.utils.IntIntMap;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.reflect.ClassReflection;
import com.badlogic.gdx.utils.reflect.Method;
import com.niz.components.Components;
import com.niz.threading.Producer;
public abstract class Component{// implements Json.Serializable{
	private static final String TAG = "Component";
	public StateStream state = new ComponentStateStream();
	public String name;
	public int hash;
	public IntIntMap siblingHashes = new IntIntMap();
	public enum ParamType {INT, FIXED, MOB_ID, COMPONENT_HASH};
	public ParamType[] paramTypes;
	public String[] paramNames;
	int overrideIndex, priority;
	public int tickGroupBits = -1;
	public Array<BusMethod> busMethods = new Array<BusMethod>();
	private static ObjectMap<Class<?>, Integer> messageGroups = new ObjectMap<Class<?>, Integer>();
	public static Map map;
	protected static Producer producer;
	public Component(String s){
		name = s;
		hash = Data.hash(name);
		
		Components.add(this);
		setTickGroup();
		
		//Gdx.app.log(TAG, "tickGroup "+name);
	}
	
	
	
	private void setTickGroup() {
		Method[] m = ClassReflection.getDeclaredMethods(getClass());
		int groupBits = 0;
		//c.getClass().getDeclaredMethods();
		for (int i = 0; i < m.length; i++){
			Class<?>[] params = m[i].getParameterTypes();
			if (params.length == 3){
				//Gdx.app.log(TAG, "2  "+params[0].getSuperclass() + params[1] + "  " );
						
				if (params[0].getSuperclass().isAssignableFrom(EntityMessage.class) 
						//||
						//params[0]
						//.isInstance(TouchMessage.class) 
						){
					BusMethod value = new BusMethod();
					value.set(params[0], m[i]);
					busMethods.add(value);
					Gdx.app.log(TAG, "add method "+value.m.getName());
					if (messageGroups.containsKey(params[0])){
						
						Integer groupVal = messageGroups.get(params[0]);
						if (groupVal < 0) continue;
						groupBits |= (1<<(groupVal.intValue()-1));
						Gdx.app.log(TAG, "set tick"+groupVal);
					}
				}
			}
		}
		if (groupBits != 0){
			this.tickGroupBits = groupBits;
			
		}
		
	}

	
	
	public void establishSiblings(){}
	
	protected int addSiblingHash(int hash) {
		siblingHashes.put(hash, hash);
		return hash;
	}
	public void valuesFrom(String vals, IntArray data) {
		char comma = ',';
		int lastStart = 0;
		int total = 0;
		for (int i = 0, t = vals.length(); i < t; i++){
			char ch = vals.charAt(i);
			if (ch == comma){
				String n = vals.substring(lastStart, i);
				lastStart = i+1;
				data.add(getInt(n, total++));
				
			}
		}
		
	}

	private int getInt(String n, int index) {
		int r;
		if (paramTypes == null){
			return Integer.parseInt(n);
		}
		switch (paramTypes[index]){
		default:
		case INT:
			return Integer.parseInt(n);
	
		}
		
		
	}
	public static String separator = ", ";
	
	public String dataToString(IntArray data) {
		String s = "";
		for (int i = 0; i < data.size; i++){
			s += separator;
			if (paramTypes == null){
				s += data.get(i);
				continue;
			}
			switch (paramTypes[i]){
			case INT:
				s += data.get(i);
				break;
			
			}
		}
		return s;
	}



	public void onSubscribe(Entity e, BusData data) {
		// TODO Auto-generated method stub
		
	}
	
	
	
	
	/*public void combineSame(Component c, Entity ce, Entity e){
		IntArray data = e.getIntData(this), addData = ce.getIntData(c);
		for (int i = 0; i < data.size; i++){
			ParamType type; 
			if (paramTypes == null){
				type = ParamType.INT;
			} else {
				type = paramTypes[i];
			}
			switch (type){
			case INT:
			case FIXED:	
				data.set(i, data.get(i)+addData.get(i));
				break;
			}
		}
	}*/

//	/*
//	 * for components on physicsactors
//	 */
//	public abstract void onSpawn(Entity e);
//	
//	/*
//	 * removing a component from a physicsactor/the map
//	 */
//	public abstract void onDespawn(Entity e);
//		
//	
//
//	/*
//	 * for setting up static fields, registering textures, set up panel, inv button
//	 */
//	public void onWorldStart(){
//		
//	}
	
	/*@Override
	public void read(Json json, JsonValue jsonData) {
		name = jsonData.child().name();
		values = json.readValue(IntArray.class, jsonData);
	}
	
	@Override
	public void write(Json json) {
		json.writeValue(c.name, values, IntArray.class);
	}*/

	public static void setStatics(Map mp, Producer prod){
		map = mp;
		producer = prod;
	}



	public static void registerMessageGroup(Class<?> cl, int group){
		if (!messageGroups.containsKey(cl)){
			messageGroups.put(cl, new Integer(group));
	
		Gdx.app.log(TAG, "reg"+messageGroups.get(cl).intValue());
		}
	}



	public static int getMessageGroup(Class<? extends EntityMessage> class1) {
		return messageGroups.get(class1).intValue();
		//return null;
	}
	
}
