package com.niz;

import com.badlogic.gdx.utils.IntArray;
import com.badlogic.gdx.utils.IntMap;

public class BusData {
public Component c;
public IntArray ints;
public Object object;
public IntMap<BusData> siblings = new IntMap<BusData> ();

public int hash;

	public BusData set(Component c, IntArray ints, int hash) {
		this.c = c;
		this.ints = ints;
		this.hash = hash;
		return this;
	}
	public BusData set(BusData bc){
		c = bc.c;
		ints = bc.ints;
		return this;
	}
	public String dataToString() {
		return c.dataToString(ints);
	}
	
	
	public void addSibling(BusData bd){
		
		if (c.siblingHashes.containsKey(bd.c.hash)){
			siblings.put(bd.c.hash, bd);
		}
		
		return;
	}
	public void removeSibling(BusData bd){
		if (siblings.containsKey(bd.c.hash)){
			siblings.remove(bd.c.hash);
		}
	}
	
}


