package com.niz;

import com.badlogic.gdx.utils.Pool;
import com.badlogic.gdx.utils.Pools;

public class ChunkPool extends Pool<Chunk> {

	public Pool<DesktopBlockMap> updatePool = Pools.get(DesktopBlockMap.class);
	
	@Override
	protected Chunk newObject() {
		return new Chunk();
	}
	
	@Override
	public void free(Chunk c){
		c.onFree();
		super.free(c);
	}

	@Override
	public Chunk obtain(){
		Chunk c = super.obtain();	
		return c;
	}
	
	public static int[] obtainBlockArray(){
		return arrayPool.obtain();
	}
	
	private static Pool<int[]> arrayPool = new Pool<int[]>(){

		@Override
		protected int[] newObject() {
			return new int[Chunk.BLOCKS_TOTAL];
		}
		
	};
	
}
