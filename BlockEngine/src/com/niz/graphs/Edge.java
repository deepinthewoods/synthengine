package com.niz.graphs;

import com.badlogic.gdx.utils.Array;

public class Edge {
public int in, out;
private int value;
private Graph g;
public int index;

public void set(int value){
	if (value != this.value){
		this.value = value;
		g.queueNode(out);
	}
}

public void remove() {
	g.getNode(out).removeIn(index);
	g.getNode(in).removeOut(index);
	
}

public void set(Edge e) {
	in = e.in;
	out = e.out;
	value = e.value;
	g = e.g;
	index = e.index;
	
}

}
