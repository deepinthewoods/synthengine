package com.niz.graphs;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.BinaryHeap;
import com.badlogic.gdx.utils.Pool;
import com.badlogic.gdx.utils.Pools;

public class Graph {
	/*
	 * 	divides into sub-graphs based on input
	 * 	
	 *	checks for output changes
	 *
	 *  needs to save the nodes that can change. they can all tick
	 * 
	 * also save output nodes (end nodes) so they can tick
	 */
	//private BinaryHeap<Node> heap = new BinaryHeap<Node>();
	
	private Array<Node> nodes = new Array<Node>();
	private Array<Node> startNodes = new Array<Node>();
	private Array<Edge> edges = new Array<Edge>();
	
	private Pool<Graph> graphPool = Pools.get(Graph.class);
	private Pool<Edge> edgePool = Pools.get(Edge.class);
	private Pool<Node> nodePool = Pools.get(Node.class);
	public void processInputs(){
		//inputs
		
		//add inputs that can change, they will be start nodes.
		
		//loop:
		//tick highest priority added node, queueing new node/s if output has changed. end nodes are ignored
		
		//tick all end nodes
		
		
		
	}
	
	public void topoSort(){
		resetMarks();
		//create copy
		Graph copy = graphPool.obtain();
		copy.set(this);
		copy.topoSortDestructive();
		//set(copy);
		setIndices(copy);
		//setEdges(copy);
	
		
		graphPool.free(copy);
	}


	/*private void setEdges(Graph copy) {
		for (int i = 0; i < copy.nodes.size; i++){
			Node cn = copy.nodes.get(i);//.obtain();
			Node n = nodes.get(cn.mark);
			//n.set(copy.nodes.get(i));
			//nodes.add(n);
		}
		
	}*/

	private void set(Graph graph) {
		clear();
		for (int i = 0; i < graph.nodes.size; i++){
			Node n = nodePool.obtain();
			n.set(graph.nodes.get(i));
			nodes.add(n);
		}
		for (int i = 0; i < graph.edges.size; i++){
			Edge n = edgePool.obtain();
			n.set(graph.edges.get(i));
			edges.add(n);
		}
		
	}
	
	private void setIndices(Graph graph){
		nodes.clear();
		for (int i = 0; i < graph.nodes.size; i++){
			Node n = nodePool.obtain();
			n.setIndex(graph.nodes.get(i));
			nodes.add(n);
		}
	}

	private void clear() {
		for (int i = 0, n = nodes.size; i < n; i++){
			nodePool.free(nodes.removeIndex(0));
		}
		
	}

	/*private void setEdges(Graph copy) {
		for (int i = 0; i < copy.nodes.size; i++){
			Node n = copy.nodes.get(i);
			Node thisnode = nodes.get(n.mark);
			thisnode.setEdges(n);
			
		}
		
	}
	
	private void setIndices(Graph copy) {
		for (int i = 0; i < copy.nodes.size; i++){
			Node n = copy.nodes.get(i);
			Node thisnode = nodes.get(n.mark);
			thisnode.index = n.index;
			
		}
		
	}*/

	private void resetMarks() {
		for (int i = 0; i < nodes.size; i++){
			Node n = nodes.get(i);
			n.mark = i;
		}
		
	}

	Array<Node> s = new Array<Node>(), l = new Array<Node>();
	
	public void topoSortDestructive(){
	//	L ← Empty list that will contain the sorted elements = nodes
	//	S ← Set of all nodes with no incoming edges = heap
		getNodesWithNoInput(s);
		l.clear();
			
			
	//	while S is non-empty do
		while (s.size > 0){
	//	    remove a node n from S
			Node n = s.pop();
	//	    insert n into L
			l.add(n);
	//	    for each node m with an edge e from n to m do
			for (int i = 0; i < n.outputEdges.size; i ++){
				Edge e = edges.get(n.outputEdges.get(i));
				Node m = nodes.get(e.out);
				// remove edge e from the graph
				e.remove();
	//		        if m has no other incoming edges then
				if (m.inputEdges.size == 0)
	//		            insert m into S
					s.add(m);
			}
	//	       
		}
	//	if graph has edges then
	//	    return error (graph has at least one cycle)
	//	else 
	//	    return L (a topologically sorted order)
		
		//set indices to the order from L
		nodes.clear();
		for (int i = 0; i < l.size; i++){
			Node n = l.get(i);
			n.index = i;
			nodes.add(n);
		}
			
	}

	private void getNodesWithNoInput(Array<Node> s) {
		for (int i = 0; i < nodes.size; i ++){
			Node n = nodes.get(i);
			if (n.inputEdges.size == 0)
				s.add(n);
		}
		
	}

	public Node getNode(int index){
		return nodes.get(index);
	}
	
}
