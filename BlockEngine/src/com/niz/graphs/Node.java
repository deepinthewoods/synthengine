package com.niz.graphs;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.BinaryHeap;
import com.badlogic.gdx.utils.IntArray;
import com.badlogic.gdx.utils.Pool;
import com.badlogic.gdx.utils.Pools;
import com.niz.Component;

public class Node extends BinaryHeap.Node{
	public static Pool<Node> pool = Pools.get(Node.class);
	public IntArray inputEdges = new IntArray();
	public IntArray outputEdges = new IntArray();
	public Node() {
		super(1);
	}
	

	public NodeComponent c;
	public IntArray data;
	//public IntArray cachedInputs = new IntArray(), input = new IntArray(), outputIndices = new IntArray();
	//public Array<Node> output = new Array<Node>();
	public String[] inputNames, outputNames;
	//private Graph graph;
	public int index, mark;

	public Node set(Node def){
		c = def.c;
		index = def.index;
		//cachedInputs.set(def.cachedInputs);
		//input.set(def.input);
		inputNames = def.inputNames;
		outputNames = def.outputNames;
		return this;
	}
	
	public void solve(Graph graph){
		c.process(this);
	}

	/*public void removeOut(Edge edge) {
		for (int i = outputEdges.size-1;i>=0; i--){
			if (outputEdges.get(i).hash == edge.hash){
				outputEdges.removeIndex(i);
			}
		}
		
	}

	public void removeIn(Edge edge) {
		for (int i = inputEdges.size-1;i>=0; i--){
			if (inputEdges.get(i).hash == edge.hash){
				inputEdges.removeIndex(i);
			}
		}
		
	}*/

	public void setEdges(Node n) {
		inputEdges.clear();
		inputEdges.addAll(n.inputEdges);
		
		
	}

	public void removeOut(int index2) {
		outputEdges.removeValue(index2);
		
	}

	public void removeIn(int index2) {
		inputEdges.removeValue(index2);
		
	}

	public void setIndex(Node node) {
		index = node.index;
		
	}
	
	
	
	
}
