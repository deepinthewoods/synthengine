package com.niz;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector3;

public class ColorData {
public final static int 
MAX_COLORS = 32;

public final int WHITE = 0;

public final int BLACK = 1;

//public static Color[] c = new Color[MAX_COLORS];


static Color tmpCD = new Color(), tmpCT = new Color(), tmpCN = new Color(), tmpC = new Color(), tmpCC = new Color();
static Color[] c = new Color[MAX_COLORS];
static Color[] compliments = new Color[MAX_COLORS];




	public static void init(){
		
		initFades();
		makeLookup();
	}
	
	

	private static float[][][][] colorIndex;
	public static void makeLookup(){
		for (int i = 0; i < 16; i++){
			//for (int j = 0; j < 16; j++){
			CorneredSprite.cachedColors[i] = Color.toFloatBits(i*16, i*16,i*16, 255);
			CorneredSprite.cachedDayColors[i] = Color.toFloatBits(i*16, i*4,i*2, 255);
				
		}
		initFades();
	}

	//public float[] fadeDay = new float[16], fadeNight = new float[16], fadeTorch = new float[16];

	
	private static void initFades() {
		// TODO Auto-generated method stub
		Pixmap fades = new Pixmap(Gdx.files.internal("data/fade.png"));
		/*for (int i = 0; i < 16; i++){
			Color.rgba8888ToColor(tmpC, fades.getPixel(i, 2));
			fadeTorch[i] = tmpC.toFloatBits();
			Color.rgba8888ToColor(tmpC, fades.getPixel(i, 1));
			fadeNight[i] = tmpC.toFloatBits();
			Color.rgba8888ToColor(tmpC, fades.getPixel(i, 0));
			fadeDay[i] = tmpC.toFloatBits();
		}*/
		
		for (int d = 0; d < 16; d++)
			for (int f = 0; f < 16; f++)//fire
				for (int t = 0; t < MAX_COLORS; t++)//time, for torch flicker
					for (int l = 0; l < 16; l++){//light, for day/night fade
						int flickeredTorch =f;
						if (f < 8) flickeredTorch -= MathUtils.random(1);
						
						Color.rgba8888ToColor(tmpCT, fades.getPixel(f, 0));//torch
						
						Color.rgba8888ToColor(tmpCD, fades.getPixel(d, 2));//Day
						
						Color.rgba8888ToColor(tmpCC, fades.getPixel(t % 16, 3+(t/16)));
						if (d == 0 && f == 0 && l == 0){
							c[t] = new Color(tmpCC);
							//Color comp = new Color(tmpCC);;
							
							//java.awt.Color col = new java.awt.Color(comp.r, comp.g, comp.b);
							
							//comp.set(col.getRed(), col.getGreen(), col.getBlue(), col.getAlpha());
							
							//compliments[t] = comp;
							//java.awt.Color.r
						}
						//tmpCC.lerp(tmpCD, .5f);
						Color.rgba8888ToColor(tmpCN, fades.getPixel(d, 1));//Night
						
						float alpha = Math.min(l/15f, 1);
						tmpC.set(tmpCD);
						tmpC.lerp(tmpCN, alpha);//tmpC is now sunlight level
						tmpC.add(tmpCT);
						
						//colors for clothes, skin etc
						
						//t
						
						
						
						//tmpC.lerp(tmpCC,  .5f);
						
					
						//Gdx.app.log("spritecornered", "megacolorarray");
						CorneredSprite.lightFades[t][l][f][d] = blend(tmpC, tmpCC);
			}
		for (int d = 0; d < 16; d++){
			Color.rgba8888ToColor(tmpCD, fades.getPixel(d, 3));
			CorneredSprite.bgs[d] = tmpCD.toFloatBits();
		}
		CorneredSprite.specialColors[1] = getFade(fades, 4, 0, 16);//new float[32];
		CorneredSprite.specialColors[2] = getFade(fades, 4, 4, 8);
		CorneredSprite.specialColors[3] = getFade(fades, 32, 6, 64);
		CorneredSprite.specialColors[4] = getFade(fades, 8, 8, 16);
		CorneredSprite.specialColors[5] = getFade(fades, 8, 0, 64);
		CorneredSprite.specialColors[6] = getFade(fades, 8, 0, 64);
		
	}

	private static float[] getFade(Pixmap fades, int scale, int index, int size) {
		float[] fade = new float[size];
		int px = index % 16, py = index/16+8;
		
		for (int i = 0; i < size/scale; i++){
			float f = 0;
			Color.rgba8888ToColor(tmpCD, fades.getPixel(px+i, py));
			if (i != size/scale-1)Color.rgba8888ToColor(tmpCT, fades.getPixel(px+i+1, py));
			else Color.rgba8888ToColor(tmpCT, fades.getPixel(px+i-(size/scale)+1, py));
			for (int g = 0; g < scale; g++){
				float alpha = (g+1) / (float)scale;
				tmpC.set(tmpCD).lerp(tmpCT, alpha);
				fade[i*scale+g] = tmpC.toFloatBits();;
			}
			
		}
		return fade;
	}
	
	private static float blend(Color c1, Color c2) {
		float r = (c1.r * c2.r)/1f
		,g = (c1.g * c2.g)/1f
		,b = (c1.b * c2.b)/1f;
		return Color.toFloatBits(r, g, b, 1f);
	}

	public static Color getColor(int colorID) {
		return c[colorID];
	}
	
	public static Color getComplimentaryColor(int colorID) {
		return compliments[colorID];
	}
	
	
	public static Color HSVtoRGB(float hue, float saturation, float value) {

	    int h = (int)(hue * 6);
	    float f = hue * 6 - h;
	    float p = value * (1 - saturation);
	    float q = value * (1 - f * saturation);
	    float t = value * (1 - (1 - f) * saturation);

	    switch (h) {
	      case 0: return new Color(value, t, p, 1f);
	      case 1: return new Color(q, value, p, 1f);
	      case 2: return new Color(p, value, t, 1f);
	      case 3: return new Color(p, q, value, 1f);
	      case 4: return new Color(t, p, value, 1f);
	      case 5: return new Color(value, p, q, 1f);
	      default: throw new RuntimeException("Something went wrong when converting from HSV to RGB. Input was " + hue + ", " + saturation + ", " + value);
	    }
	}
	
	// r,g,b values are from 0 to 1
	// h = [0,360], s = [0,1], v = [0,1]
//			if s == 0, then h = -1 (undefined)
	static void RGBtoHSV( float r, float g, float b, float[] ret)
	{
		float h, s, v;
		float min, max, delta;
		min = Math.min(Math.min( r, g), b );
		max = Math.min(Math.max( r, g), b );
		v = max;				// v
		delta = max - min;
		if( max != 0 )
			s = delta / max;		// s
		else {
			// r = g = b = 0		// s = 0, v is undefined
			s = 0;
			h = -1;
			return;
		}
		if( r == max )
			h = ( g - b ) / delta;		// between yellow & magenta
		else if( g == max )
			h = 2 + ( b - r ) / delta;	// between cyan & yellow
		else
			h = 4 + ( r - g ) / delta;	// between magenta & cyan
		h *= 60;				// degrees
		if( h < 0 )
			h += 360;
		
		ret[0] = h;
		ret[1] = s;
		ret[2] = v;
	}

	


	/*public static String rgbToString(float r, float g, float b) {
	    String rs = Integer.toHexString((int)(r * 256));
	    String gs = Integer.toHexString((int)(g * 256));
	    String bs = Integer.toHexString((int)(b * 256));
	    return rs + gs + bs;
	}*/
	
}
