package com.niz;

import com.badlogic.gdx.utils.Pool;
import com.badlogic.gdx.utils.Pools;
import com.niz.verlet.PhysicsEngine;

public class Vector {
public int x,y, z;
private static final int bits = PhysicsEngine.FIXED_FRACTION_BITS;
	public Vector(int x, int y, int z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	public Vector(){
		
	}
	public Vector mulFixed(int value, int bits){
		x *= value;
		x >>= bits;
		y *= value;
		y >>= bits;
		z *= value;
		z >>= bits;
		return this;
	}
	public Vector add(Vector v){
		x += v.x;
		y += v.y;
		z += v.z;
		return this;
	}
	public Vector sub(Vector v){
		x -= v.x;
		y -= v.y;
		z -= v.z;
		return this;
	}
	public Vector set(int x, int y){
		this.x = x;
		this.y = y;
		z =0;
		return this;
	}
	public Vector set(int x, int y, int z){
		this.x = x;
		this.y = y;
		this.z = z;
		return this;
	}
	
	public class v2Pool extends Pool<Vector>{

		@Override
		protected Vector newObject() {
			return new Vector();
		}
		
	}
	public static Pool<Vector> pool = Pools.get(Vector.class);
	
	public static Vector obtain() {
		return pool.obtain();
	}
	public void free() {
		pool.free(this);
		
	}
	public String toString(){
		return ""+x+", "+y+", "+z;
	}
	public void set(Vector v) {
		x = v.x;
		y = v.y;
		z = v.z;
		
	}
	static long tmp;
	public Vector scl(int i) {
		tmp = i;
		tmp *= x;
		tmp >>= bits;
		x = (int) tmp;
		
		tmp = i;
		tmp *= y;
		tmp >>= bits;
		y = (int) tmp;
		
		tmp = i;
		tmp *= z;
		tmp >>= bits;
		z = (int) tmp;
		
		
		return this;
	}
	
	public static int fixedMul(int i, int j){
		tmp = i;
		tmp *= j;
		tmp >>= bits;
		return (int) tmp;//(tmp & Integer.MAX_VALUE);
	}
	public String toFloatString() {
		return ""+x/(float)PhysicsEngine.ONE+", "+y/(float)PhysicsEngine.ONE+", "+z/(float)PhysicsEngine.ONE;

	}
	
	
}
