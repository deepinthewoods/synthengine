package com.niz;

import com.badlogic.gdx.utils.Pool;

public class EntityPool extends Pool<Entity> {

	@Override
	protected Entity newObject() {
		return new Entity();
	}

	
	public void free(Entity e){
		e.reset();
		super.free(e);
	}
}
