package com.niz.blocks;

import com.niz.CorneredSprite;
import com.niz.verlet.PhysicsEngine;

public class Block {
public static int id(int v){
	v &= 255;
	return v;
}
public static int meta(int v){
	v >>=8;
	v &= 255;
	return v;
}
public static int light(int v){
	v >>=16;
	v &= 15;
	return v;
}
public static int dayLight(int v){
	v >>=20;
	v &= 15;
	return v;
}

public static int bitmaska(int v){
	v >>=24;
	v &= 15;
	return v;
}
public static int bitmaskb(int v){
	v >>=28;
	v &= 15;
	return v;
}
private static final int inv256 = 255 * -1;
public static int setID(int b, int v) {
	b ^= 255;
	v &= 255;
	b += v;
	return b;
}
public static int setMeta(int b, int v) {
	b ^= (255 << 8);
	v &= 255;
	v <<= 8;
	b += v;
	return b;
}
public static int setLight(int b, int v) {
	b ^= (255 << 16);
	v &= 15;
	v <<= 16;
	b += v;
	return b;
}
public static int setDayLight(int b, int v) {
	b ^= (255 << 20);
	v &= 15;
	v <<= 20;
	b += v;
	return b;
}

public static int setBitmaskA(int b, int v) {
	b ^= (255 << 24);
	v &= 15;
	v <<= 24;
	b += v;
	return b;
}

public static int setBitmaskB(int b, int v) {
	b ^= (255 << 28);
	v &= 15;
	v <<= 28;
	b += v;
	return b;
}

private static BlockDef[] defs;
public static BlockDef def(int block){
	return defs[id(block)];
}

public static void setDefs(BlockDef[] v){
	defs = v;
}
public static CorneredSprite sprite(int block) {
	
	return def(block).sprite(block);
}


public static boolean collidesDown(int b) {
	
	switch (bitmaska(b)){
	case 10:
	case 11:
	case 14:
	case 15:
		
		return true;
	default: return false;
	}
}

public static boolean collidesLeft(int b){
	switch (bitmaska(b)){
	case 1:case 4:case 5:case 8:case 9:case 13:
		
		
		return true;
	default: return false;
	}
}

public static boolean collidesRight(int b){
	switch (bitmaska(b)){
	case 1:case 2:case 3:case 4:case 5:case 6:case 7:
		
		
		return true;
	default: return false;
	}
}
public static int height(int b, int side, int x, int y, int z) {
	return def(b).getHeight(side, x, y, z);
	
}

	
}
