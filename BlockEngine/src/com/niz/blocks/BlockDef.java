package com.niz.blocks;

import com.niz.CorneredSprite;
import com.niz.Data;
import com.niz.verlet.JPhysicsEngine;
import com.niz.verlet.PhysicsEngine;

public class BlockDef {
	private static Data data;
	public String name;
	public int index, id, numberOfSprites;
	//flags
	//solid
	// MOVEMENT MODE: solid / slope / ladder / liquid / one-way platform /
	
	int lightLoss = 1, dayLightLoss = 1, minDayLight = 0, minLight = 0,
			slopeType = 0
			
			;
	
	//public static final int PT_BL = 0, PT_B = 1, PT_BR = 2, PT_TR = 3, PT_T = 4, PT_TL = 5;
	//private int[] heights = new int[6*JPhysicsEngine.COLLISION_GRANULARITY*JPhysicsEngine.COLLISION_GRANULARITY];
	public boolean solid = true;
	//public int heightL = PhysicsEngine.ONE, heightR = PhysicsEngine.ONE;

	public CorneredSprite sprite(int meta) {
		return Data.getBlockSprite(index, meta, numberOfSprites);
	}

	public void init(int id){
		this.id = id;
		//index = Data.registerBlockSprite(this, "spritename", 6);
		//index = Data.registerBlockSprite(this, "spritename");
	}
	
	public int getHeight(int side, int x, int y, int z){
		return 0;
	}
	
	
}
