package com.niz;

import com.niz.verlet.PhysicsEngine;

public class Util {
public static boolean bit(int src, int index){
	return (src & (1<<index))>>(index) == 1;
}
public static int bits(int src, int index, int len){
	return (src & ())>>(len);
}

//---

	static public final float PI = 3.1415927f;

	static private final int SIN_BITS = 13; // Adjust for accuracy.
	static private final int SIN_MASK = ~(-1 << SIN_BITS);
	static private final int SIN_COUNT = SIN_MASK + 1;

	static private final float radFull = 1-(-1<<PhysicsEngine.FIXEDBITS);//PI * 2;
	static private final float degFull = 360;
	//static private final int radToIndex = (int)(SIN_COUNT / radFull);
	static private final int degToIndex = PhysicsEngine.FIXEDBITS;//(SIN_COUNT / degFull);

	static public final float radiansToDegrees = 180f / PI;
	static public final float radDeg = radiansToDegrees;
	static public final float degreesToRadians = PI / 180;
	static public final float degRad = degreesToRadians;

	static private class Sin {
		static final int[] table = new int[SIN_COUNT];
		static {
			for (int i = 0; i < SIN_COUNT; i++)
				
				table[i] = (int)(Math.sin((i + 0.5f) / SIN_COUNT * radFull)*SIN_COUNT);//(Math.sin((i + 0.5f) / SIN_COUNT * radFull)*SIN_COUNT);
			//for (int i = 0; i < 360; i += 90)
				//table[(int)(i * degToIndex) & SIN_MASK] = (float)Math.sin(i * degreesToRadians);
		}
	}

	static private class Cos {
		static final int[] table = new int[SIN_COUNT];
		static {
			for (int i = 0; i < SIN_COUNT; i++)
				table[i] = (int)(Math.cos((i + 0.5f) / SIN_COUNT * radFull)*SIN_COUNT);
			//for (int i = 0; i < 360; i += 90)
				//table[(int)(i * degToIndex) & SIN_MASK] = (float)Math.cos(i * degreesToRadians);
		}
	}

	/** Returns the sine in radians. */
	static public final int sin (int radians) {
		return Sin.table[(int)(radians >> degToIndex) & SIN_MASK];
	}

	/** Returns the cosine in radians. */
	static public final int cos (int radians) {
		return Cos.table[(int)(radians >> degToIndex) & SIN_MASK];
	}

	

	// ---

public static int sin(int x, int y) {
	// TODO Auto-generated method stub
	return 0;
}
}
