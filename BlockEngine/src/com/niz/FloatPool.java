package com.niz;

import com.badlogic.gdx.utils.FloatArray;
import com.badlogic.gdx.utils.Pool;

public class FloatPool extends Pool<FloatArray> {

	@Override
	protected FloatArray newObject() {
		
		return new FloatArray();
	}

}
