package com.niz.components.ui;

import com.badlogic.gdx.Gdx;
import com.niz.BusData;
import com.niz.Component;
import com.niz.Entity;
import com.niz.components.CDrawTextButton;
import com.niz.components.Components;
import com.niz.threading.Messages.ClickMessage;

public class NewButton extends Component {
	private static final String TAG = "newBt";
	private static int s_text_button;
	public NewButton() {
		super("new_button");
		
	}
	@Override
	public void establishSiblings(){
		//Object o = new Object();
		//s_text_button = addSiblingHash(Components.cDrawTextButton.hash);
		//s_body = addSiblingHash(Components.cBody.hash);
	}

	public void touch(ClickMessage m, BusData data, Entity e){
		Gdx.app.log(TAG, "click new");
		e.plane.map.startNewPlatformerGame();
		//BusData textB = data.siblings.get(s_text_button);
		//textB.ints.set(CDrawTextButton.COLOR_ID, textB.ints.get(CDrawTextButton.COLOR_ID)+1);
	}
	
}
