package com.niz.components;

import com.badlogic.gdx.utils.IntArray;
import com.niz.BusData;
import com.niz.Component;
import com.niz.Entity;
import com.niz.threading.Messages.DrawUIMessage;

public class CDrawInvButton extends Component {
	private static int s_position;
	public static final int INDEX = 0;
	public CDrawInvButton() {
		super("draw_inv_button");
	}

	@Override
	public void establishSiblings(){
		//Object o = new Object();
		s_position = addSiblingHash(Components.cPosition.hash);
	}
	
	public void draw(DrawUIMessage m, BusData data, Entity e){
		IntArray 
		ints = data.ints,
		pos = data.siblings.get(s_position).ints;
		//m.batch.partialCircle(pos.get(CPosition.X), pos.get(CPosition.Y), ints.get(RADIUS), .9f, Color.WHITE);
		//int maxWidth = data.ints.get(MAX_WIDTH), maxHeight = data.ints.get(MAX_HEIGHT)
		//, style = data.ints.get(STYLE), colorID = data.ints.get(COLOR_ID)
		//, textColorID = data.ints.get(TEXT_COLOR_ID), fontID = data.ints.get(FONT_ID)
		//;
		
		//String string = Data.getString(data.ints.get(STRING_HASH));
		//m.batch.textButton(pos.get(CPosition.X), pos.get(CPosition.Y), string, style
		//		, maxWidth, maxHeight, colorID, textColorID, fontID);
		m.batch.invButton(pos.get(CPosition.X), pos.get(CPosition.Y), data.ints.get(INDEX));
		
		
		// m.batch.partialCircle(0,0,.051135f, .9f, Color.RED);
		//Gdx.app.log("cdraw", "draw circle " + pos.get(CPosition.X) + "  "+ pos.get(CPosition.Y));
	}
}
