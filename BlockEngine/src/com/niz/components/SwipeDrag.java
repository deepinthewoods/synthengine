package com.niz.components;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.IntArray;
import com.niz.BusData;
import com.niz.Component;
import com.niz.Entity;
import com.niz.planes.UIPlane;
import com.niz.threading.Messages.TickMessage;
import com.niz.threading.Messages.TouchDragMessage;
import com.niz.threading.Messages.TouchDragStopMessage;
import com.niz.threading.Messages.TouchFlingMessage;

public class SwipeDrag extends Component {
	private static int s_position;
	public static final int DRAGGING = 0, TARGET_X = 1, INDEX = 2, LIMIT = 3 ;
	public SwipeDrag() {
		super("swipe drag");
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public void establishSiblings(){
		//Object o = new Object();
		s_position = addSiblingHash(Components.cPosition.hash);
		
	}
	
	public void drag(TouchDragMessage m, BusData data, Entity e){
		m.v.sub(m.w).scl(-1);
		
		IntArray pos = data.siblings.get(s_position).ints;
		int x = pos.get(CPosition.X), y = pos.get(CPosition.Y)
				, targetx = data.ints.get(TARGET_X);
		
		int newX = (int)((m.v.x)+targetx), limit = data.ints.get(LIMIT)*(int)(UIPlane.uiXPix);
		newX = Math.max(newX, 0);
		newX = Math.min(newX, limit);
		pos.set( CPosition.X, newX);
		data.ints.set(DRAGGING, 1);
		//pos.set( CPosition.Y, (int)((m.v.y)) );
		Gdx.app.log("swipedrag", "drag");
	}
	public void undrag(TouchDragStopMessage m, BusData data, Entity e){
		IntArray pos = data.siblings.get(s_position).ints;
		int x = pos.get(CPosition.X), limit = data.ints.get(LIMIT)*(int)(UIPlane.uiXPix);
		data.ints.set(DRAGGING, 2);
		boolean neg = false;
		if (x < 0){
			x = -x;
			neg = true;
		}
		int targetx = x / (int)(UIPlane.uiXPix);
		int modx = x %(int)(UIPlane.uiXPix);
		if (modx > (int)(UIPlane.uiXPix)/2)
			targetx++;
		if (neg) targetx *= -1;
		int index = targetx;
		data.ints.set(INDEX, index);
		targetx *=  (int)(UIPlane.uiXPix);
		data.ints.set(TARGET_X, targetx);
		//Gdx.app.log("swipedrag", "un drag"+x + "   "+((int)(UIPlane.uiXRes*PhysicsEngine.ONE)/2) + "   "+modx);
	}
	public void fling(TouchFlingMessage m, BusData data, Entity e){
		IntArray pos = data.siblings.get(s_position).ints;
		int x = pos.get(CPosition.X), limit = data.ints.get(LIMIT);
		data.ints.set(DRAGGING, 2);
		m.v.sub(m.w).scl(-1);
		boolean right = false;
		if (m.v.x < 0){
			right = true;
		}
		int targetx = x / (int)(UIPlane.uiXPix);
		boolean neg = x < 0;
		
		if (right){
			if (neg)
				targetx -= 1;
			else 
				targetx += 0;
		}
		else{
			
			if (neg)
				targetx += 0;
			else 
				targetx += 1;
		}
		
		int index = targetx;
		index = Math.max(0, index);
		index = Math.min(limit, index);
		data.ints.set(INDEX, index);
		Gdx.app.log("swipedrag", "un drag"+x + ", "+targetx+neg+"index"+index);
		targetx = index * (int)(UIPlane.uiXPix);
		data.ints.set(TARGET_X, targetx);
		
	}
	public void tick(TickMessage m, BusData data, Entity e){
		//Gdx.app.log("swipedrag", "tick");
		IntArray pos = data.siblings.get(s_position).ints;
		int x = pos.get(CPosition.X), targetX = data.ints.get(TARGET_X);
		
		if (data.ints.get(DRAGGING) == 2){//moving to target normal speed
			int speed = 32;
			int dx = targetX - x;
			dx /= 4;
			dx = Math.max(dx,  -speed);
			dx = Math.min(dx,  speed);
			
			//Gdx.app.log("swipedrag", "move"+x+","+targetX);
			pos.set( CPosition.X, x + dx );
		} else if (data.ints.get(DRAGGING) == 3){
			
		}
	}

}
