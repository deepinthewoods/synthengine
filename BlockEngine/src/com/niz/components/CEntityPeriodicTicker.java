package com.niz.components;

import com.badlogic.gdx.Gdx;
import com.niz.BusData;
import com.niz.Component;
import com.niz.Entity;
import com.niz.threading.Messages.TickMessage;

public class CEntityPeriodicTicker extends Component {

	public CEntityPeriodicTicker() {
		super("entity duration updater");
		
	}
	
	public void tick(TickMessage m, BusData data, Entity e){
		e.tickPeriodic();
		//Gdx.app.log("ticker", "tick");
		
	}

}
