package com.niz.components;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector3;
import com.niz.BusData;
import com.niz.Component;
import com.niz.Entity;
import com.niz.threading.Messages.CollideQueryMessage;

public class Collide2d extends Component{
	
	private static final String TAG = "collide 2d";
	private static int s_position = 0, s_body = 0;
	
	public Collide2d() {
		super("collide2d");
		
	}

	@Override
	public void establishSiblings(){
		//Object o = new Object();
		s_position = addSiblingHash(Components.cPosition.hash);
		s_body = addSiblingHash(Components.cBody.hash);
		//Gdx.app.log(TAG, "siblings");
	}
	
	
	

	public void collideQuery(CollideQueryMessage m, BusData data, Entity e){
		
		if (m.answered) {
			//Gdx.app.log(TAG, "null");
			//throw new GdxRuntimeException("erp");
			return;
		}
		//position
		BusData position = data.siblings.get(s_position);
		//body size
		BusData body = data.siblings.get(s_body);
		if (position == null || body == null){
			Gdx.app.log(TAG, "null");
			return;
		}
		Vector3 pos = (Vector3) position.object;
		
		int x = position.ints.get(CPosition.X)>>4, y = position.ints.get(CPosition.Y)>>4;
		//float x = pos.x, y = pos.y;
		
		int w = body.ints.get(CBody.BODY_WIDTH), h = body.ints.get(CBody.BODY_HEIGHT);
		//Gdx.app.log(TAG, "collide2d p"+x+", "+y+"  touchx "+m.v.x+" , touchy "+m.v.y+"  w"+w/2+" h"+h+"  "+(m.v.x )+" > "+(x+w/2));
		if (m.v.x > x+w/2 || m.v.x < x-w/2 || m.v.y > y+h/2 || m.v.y < y-h/2) {
			
			Gdx.app.log(TAG, ""+ (m.v.x > x+w/2) +  (m.v.x < x-w/2 )+ m.v.y +">"+ (y)+"+"+(h/2) + (m.v.y < y-h/2));
					return;
		}
		Gdx.app.log(TAG, "collide2d returning "+m.v.x+" , "+m.v.y+"  w"+w+" h"+h+"  " + "tou");
		m.answered = true;
		m.ret = e;
		
		
	}

}
