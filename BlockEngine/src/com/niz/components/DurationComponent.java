package com.niz.components;

import com.badlogic.gdx.utils.IntArray;
import com.niz.Component;
import com.niz.Entity;
import com.niz.PeriodicUpdate;

public class DurationComponent extends Component implements PeriodicUpdate{
	public static final int P_DURATION = 0, P_COMPONENT_HASH = 1, P_COMPONENT_HASH_SELF = 2;
	
	
	public DurationComponent() {
		super("duration");
	}
	
	@Override
	public int getPeriodicInterval(IntArray data) {
		return data.get(P_DURATION);
	}
	
	@Override
	public int updatePeriodic(Entity e, IntArray data) {
		e.bus.remove(data.get(P_COMPONENT_HASH));
		e.bus.remove(data.get(P_COMPONENT_HASH_SELF));
		return END_CODE;
	}

}
