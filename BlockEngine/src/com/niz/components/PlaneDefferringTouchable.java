package com.niz.components;

import com.badlogic.gdx.Gdx;
import com.niz.BusData;
import com.niz.Component;
import com.niz.Entity;
import com.niz.SynthEngine;
import com.niz.planes.UIPlane;
import com.niz.threading.Messages;

public class PlaneDefferringTouchable extends Component {
	public static final int TOUCHED = 0, TOUCH_UI_TICK = 1, DRAGGED =  2, DRAG_UI_TICK = 3, PREV_X = 4, PREV_Y = 5
			, FIRST_X = 6, FIRST_Y = 7
			
			
			, SINGLE_CLICK_DELAY = 48
			, DRAG_CLICK_DELAY = 10
			 ;
	
	private static final String TAG = "touchable"; 
	public PlaneDefferringTouchable() {
		super("touchable");
		//tickGroup = 1;
		
	}

	public void touch(Messages.TouchDownMessage m, BusData data, Entity e){
		//Gdx.app.log(TAG, "touchdown"+data.ints.get(TOUCHED) + data.ints.get(DRAGGED));
		if (data.ints.get(TOUCHED) == 0){
			data.ints.set(TOUCHED, 1);
			data.ints.set(TOUCH_UI_TICK, SynthEngine.uiTick);
			data.ints.set(FIRST_X, m.data.get(0));
			data.ints.set(FIRST_Y, m.data.get(1));
			Gdx.app.log(TAG, "touchdown"+SynthEngine.uiTick);
		}
		
		int prevx = data.ints.get(PREV_X), prevy = data.ints.get(PREV_Y)
				, x = m.data.get(0), y = m.data.get(1);
		
		if (x != prevx || y != prevy){
			data.ints.set(PREV_X, x);
			data.ints.set(PREV_Y, y);
			data.ints.set(DRAG_UI_TICK, SynthEngine.uiTick);
			
			int firstx = data.ints.get(FIRST_X), firsty = data.ints.get(FIRST_Y);
			float xdiff = x-data.ints.get(FIRST_X), ydiff = y-data.ints.get(FIRST_Y);
			if (Math.abs(xdiff) > UIPlane.dragMinDistance || Math.abs(ydiff) > UIPlane.dragMinDistance
					|| data.ints.get(DRAGGED) > 0 
					){
				
				
				switch (data.ints.get(DRAGGED)){
				case 0:
					data.ints.set(DRAGGED, 1);
					break;
					
				case 1:
					if (Math.abs(xdiff) > Math.abs(ydiff)){//dragged x-ways.
						//dragged x-ways.
						data.ints.set(DRAGGED, 2);
					} else {//y-ways
						data.ints.set(DRAGGED, 3);
					}
					break;
				case 2://x-ways
					e.plane.touchDrag(x, y, firstx, firsty);
					break;
				case 3://y-ways
					e.plane.touchDrag(x, y, firstx, firsty);
					break;
				}
				
				
				
				Gdx.app.log(TAG, "dragged "+xdiff + " , "+ydiff);
			}
		}
		
		
		
	}
	
	public void touchup(Messages.TouchUpMessage m, BusData data, Entity e){
		Gdx.app.log(TAG, "touchup");
		
		int x = m.data.get(0), y = m.data.get(1), firstx = data.ints.get(FIRST_X), firsty = data.ints.get(FIRST_Y);
		if (data.ints.get(DRAGGED) > 0){
			data.ints.set(DRAGGED, 0);
			if (SynthEngine.uiTick < data.ints.get(DRAG_UI_TICK)+DRAG_CLICK_DELAY){
				e.plane.fling(x, y, firstx, firsty);
				data.ints.set(TOUCHED, 0);
				Gdx.app.log(TAG, "fling");
				return;
				
			} else {
				e.plane.stopDrag(x, y);
				//m.e.bus.publish(Messages.touchUp.obtain().set(m.e));
				data.ints.set(TOUCHED, 0);
				Gdx.app.log(TAG, "stop drag");
				return;
			}
		}
		
		if (data.ints.get(TOUCHED) == 1)
		{
			data.ints.set(TOUCHED, 0);
			if (SynthEngine.uiTick < data.ints.get(TOUCH_UI_TICK)+SINGLE_CLICK_DELAY){
				
				e.bus.publish(Messages.click.obtain());
				Gdx.app.log(TAG, "click"+e);
				
			}
		}
		
	}
	
	
	
}
