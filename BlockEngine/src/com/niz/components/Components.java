package com.niz.components;

import java.util.Iterator;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.LongMap;
import com.badlogic.gdx.utils.LongMap.Values;
import com.niz.Component;
import com.niz.components.ui.NewButton;

public class Components {
	private static final String TAG = "Components";
	public static Component 
	cPhysics 
	, cPosition
	, cBody
	, cDuration 
	, cTouchablePlaneDeferring 
	, cCollide2d
	, cDrawCircle
	, cDrawTextButton
	, cDrawInvButton
	, cNewGameButton
	, cCameraMover
	, cSwipeDrag
	, cDrawPolygon
	, cChunkController
	, cEntityPeriodicTicker
	//, setting_whatever = new Setting("whatever", "whatevspref", 0,  "foo", "bar");
	;
	public static LongMap<Component> all = new LongMap<Component>();
	public static Array<Component> allStash = new Array<Component>();
	public static void add(Component c) {
		//Component c = get(s);
		
		//if (c == null){
			if (all.containsKey(getHash(c.name))){
				allStash.add(c);
				//Gdx.app.log(TAG, "EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE"+c.name+ "   "+all.get(getHash(c.name)).name);
			}else {
				all.put(getHash(c.name), c);
				//Gdx.app.log(TAG, "put"+c.name);
			}
		//}
		
	}
	
	public static void init(){
		cPosition = new CPosition();
		cPhysics = new CPhysics();
		cBody = new CBody();
		cDuration = new DurationComponent();
		cTouchablePlaneDeferring = new PlaneDefferringTouchable();
		cCollide2d = new Collide2d();
		cDrawCircle = new DrawCircle();
		cDrawTextButton = new CDrawTextButton();
		cDrawInvButton = new CDrawInvButton();
		cNewGameButton = new NewButton();
		cCameraMover = new CCameraMover();
		cSwipeDrag = new SwipeDrag();
		cDrawPolygon = new CDrawPolygon();
		cChunkController = new CChunkLoadController();
		cEntityPeriodicTicker = new CEntityPeriodicTicker();
		Values<Component> vals = all.values();
		while (vals.hasNext()){
			Component c = vals.next();
			c.establishSiblings();
		}
	}
	//private static Pool<DurationComponent> durationPool = Pools.get(DurationComponent.class);
	/*public static Component getDurationComponent(Component c, int duration){
		DurationComponent comp = durationPool.obtain();
		comp.c = c;
		comp.duration = duration;
		
		return comp;
	}
	public static void freeDurationComponent(PeriodicUpdate c) {
		durationPool.free(c);
		
	}*/
	private static long getHash(String s) {
		return s.hashCode();
	}
	
	public static Component get(String name) {
		Component com = all.get(getHash(name));
		Iterator<Component> i = allStash.iterator();
		while (i.hasNext()){
			Component c = i.next();
			if (c.name.equals(name))
				return c;
		}
		return null;
	}
	
	
}
