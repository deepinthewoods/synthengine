package com.niz.components;

public class ChunkOffset {

	private static final int MASK = 0xFFF;
	private static final int Z_BITS = 24;
	private static final int Y_BITS = 12;

	
	public static int x(int offset) {
		
		return offset & MASK;
	}
	
	public static int y(int offset) {
		
		return (offset>>Y_BITS) & MASK;
	}
	
	public static int z(int offset) {
		
		return (offset>>Z_BITS) & MASK;
	}

}
