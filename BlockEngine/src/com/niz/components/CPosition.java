package com.niz.components;

import com.niz.BusData;
import com.niz.Component;
import com.niz.Entity;


public class CPosition extends Component {
	public CPosition() {
		super("position");
		paramNames = new String[]{"x", "y", "z", "offset"};
	}

	public static final int X=0, Y=1, Z = 2, OFFSET = 3, LOCAL_X = 4, LOCAL_Y = 5, LOCAL_Z = 7;
	
	
	
}
