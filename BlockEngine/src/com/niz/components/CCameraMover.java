package com.niz.components;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.utils.IntArray;
import com.niz.BusData;
import com.niz.Component;
import com.niz.Entity;
import com.niz.threading.Messages.TickMessage;

public class CCameraMover extends Component {
	private static int s_position;
	public CCameraMover() {
		super("camera mover");
		
	}
	
	@Override
	public void establishSiblings(){
		//Object o = new Object();
		s_position = addSiblingHash(Components.cPosition.hash);
		//s_body = addSiblingHash(Components.cBody.hash);
		//Gdx.app.log(TAG, "siblings");
	}
	@Override
	public void onSubscribe(Entity e, BusData data){
		data.object = e.plane.getCamera();
	}
	
	public void tick(TickMessage m, BusData data, Entity e){
	//	Gdx.app.log("cam mover", "tick");
		Camera cam = (Camera) data.object;
		IntArray pos = data.siblings.get(s_position).ints;
		cam.position.set(pos.get(CPosition.X), pos.get(CPosition.Y), 0);
		cam.update();
	}

}
