package com.niz.components;

import com.badlogic.gdx.utils.IntArray;
import com.niz.Component;
import com.niz.Entity;
import com.niz.Plane;
import com.niz.Vector;
import com.niz.blocks.Block;
import com.niz.threading.Messages.OnSpawnMessage;
import com.niz.threading.Messages.TickCollMessage;
import com.niz.verlet.PhysicsEngine;
public class CBody extends Component {
	public CBody() {
		super("body");
		
	}

	public static final int 
	BODY_TYPE = 0
	, BODY_WIDTH = 1
	, BODY_HEIGHT = 2
	, COLLIDES_MAP = 3
	, COLLIDES_BODIES = 4
	, COLLISION_DATA = 5//bit field
	
	
	, BODY_MULTIPOINT_TALL = 0
	
	, DATA_ONGROUND = 0
	, DATA_WALLSLIDE = 1
	;
	
	/*public void onSpawn(OnSpawnMessage m){
		m.e.obtainIntData(this, 6);
	}
	
	public void collide(TickCollMessage m) {
		Entity e = m.e;
		Plane plane = e.plane;
		Vector v = Vector.obtain(); 
		IntArray data = e.getIntData(this),
		pos = e.getIntData(Components.cPosition);
				
		if (data.get(COLLIDES_MAP) > 0){
			switch (data.get(BODY_TYPE)){
			case BODY_MULTIPOINT_TALL:
				//vertical
				
				
				int x = pos.get(CPhysics.F_X), y = pos.get(CPhysics.F_Y);
				int bottomy = y-data.get(BODY_HEIGHT);
				int leftx = x-data.get(BODY_WIDTH);
				int rightx = leftx + data.get(BODY_WIDTH);
				int topy = y+data.get(BODY_HEIGHT);
				
				
				v.set(x, bottomy);
				int block_b = plane.getBlockLocal(v);
				int bx = v.x, by = v.y;
				v.set(rightx, bottomy);
				int block_br = plane.getBlockLocal(v);
				
				v.set(leftx, bottomy);
				int block_bl = plane.getBlockLocal(v);
				
				v.set(x, topy);
				int block_t = plane.getBlockLocal(v);
				
				v.set(rightx, topy);
				int block_tr = plane.getBlockLocal(v);
				
				v.set(leftx, bottomy);
				int block_tl = plane.getBlockLocal(v);
				
				int height = 0;
				//B first, if that doesn't collide try the sides
				if (Block.collidesDown(block_b)){
					height = Block.height(block_b, x);
					if ((y & PhysicsEngine.ONE) < height){
						pos.set(x, by+height);
					}
					
					//maybe trigger onCollide on entity here
				} else {
					if (Block.collidesDown(block_bl)){
						height = Block.height(block_bl, x);
					}
					if (Block.collidesDown(block_br)){
						height = Block.height(block_br, x);
					}
				}
				
				
				//T, if no coll TR TL
				
				
				
				//horizontal
				//BL, if none top left
				
				//BR, if none TR
			}
		}
		
		
	}

	*/
	
	
}
