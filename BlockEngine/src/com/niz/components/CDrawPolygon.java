package com.niz.components;

import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.IntArray;
import com.niz.BusData;
import com.niz.Component;
import com.niz.Entity;
import com.niz.threading.Messages.DrawUIMessage;

public class CDrawPolygon extends Component{
	private static int s_position, s_body;
	public static final int STYLE = 0, COLOR_ID = 1;
	public CDrawPolygon() {
		super("draw_polygon");
	}

	@Override
	public void establishSiblings(){
		//Object o = new Object();
		s_position = addSiblingHash(Components.cPosition.hash);
		s_body = addSiblingHash(Components.cBody.hash);
	}
	
	public void draw(DrawUIMessage m, BusData data, Entity e){
		IntArray 
		
		 body = data.siblings.get(s_body).ints
		;
		Vector3 pos = (Vector3) data.siblings.get(s_position).object;
		int style = data.ints.get(STYLE), colorID = data.ints.get(COLOR_ID)
		
		;
		int width = body.get(CBody.BODY_WIDTH)
				, height = body.get(CBody.BODY_HEIGHT)
				
		;
		
		
		 
		//Gdx.app.log("cdraw", "draw text box " + pos.x+pos.y+" w "+width+" h "+height+scale+string);
		m.batch.drawPolygon(pos.x, pos.y, width, height, style, colorID);
		//m.batch.drawButton(pos.get(CPosition.X), pos.get(CPosition.Y), width, height, style, 8, false);
		// m.batch.partialCircle(0,0,.051135f, .9f, Color.RED);
		//Gdx.app.log("cdraw", "draw text box " + width);
	}
	
}
