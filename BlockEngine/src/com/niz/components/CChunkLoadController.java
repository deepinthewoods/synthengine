package com.niz.components;

import com.badlogic.gdx.utils.IntArray;
import com.niz.Component;
import com.niz.Entity;
import com.niz.PeriodicUpdate;

public class CChunkLoadController extends Component implements PeriodicUpdate {
	//queues chunks to load
	public CChunkLoadController() {
		super("chunk_controller");
		
	}

	@Override
	public int getPeriodicInterval(IntArray Data) {
		return 250;
	}

	@Override
	public int updatePeriodic(Entity e, IntArray data) {
		e.plane.queueChunkLoads(4);
		return 0;
	}

}
