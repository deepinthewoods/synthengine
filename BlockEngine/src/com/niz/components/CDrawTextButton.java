package com.niz.components;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.IntArray;
import com.niz.BusData;
import com.niz.Component;
import com.niz.Data;
import com.niz.Entity;
import com.niz.UIBatch;
import com.niz.planes.UIPlane;
import com.niz.threading.Messages.DrawUIMessage;
import com.niz.verlet.JPhysicsEngine;
import com.niz.verlet.PhysicsEngine;

public class CDrawTextButton extends Component {
	private static int s_position, s_body;
	public static final int STRING_HASH = 0
			, STYLE = 1
			, COLOR_ID=2
			, TEXT_COLOR_ID=3
			, FONT_ID = 4
			, SCALE = 5
			;
	public CDrawTextButton() {
		super("draw_text_button");
	}

	@Override
	public void establishSiblings(){
		//Object o = new Object();
		s_position = addSiblingHash(Components.cPosition.hash);
		s_body = addSiblingHash(Components.cBody.hash);
	}
	
	
	
	@Override
	public void onSubscribe(Entity e, BusData data){
		//Gdx.app.log("cdraw", "sub");
		//set width/jheight
		float maxWidth = UIPlane.uiXPix/2;;
		int fontID = data.ints.get(FONT_ID);
		
		Vector3 returnBounds = JPhysicsEngine.Vector3Pool.obtain();
		
		
		String string = Data.getString(data.ints.get(STRING_HASH))
				;
		BusData pos = data.siblings.get(s_position)
		, body = data.siblings.get(s_body);
		
		UIBatch.textButtonBounds( string, maxWidth, fontID, returnBounds);
		int scale = (int) (returnBounds.z);
		body.ints.set(CBody.BODY_WIDTH, (int)returnBounds.x+UIBatch.edgeSize);
		body.ints.set(CBody.BODY_HEIGHT, (int)returnBounds.y+UIBatch.edgeSize);
		Gdx.app.log("cdraw", "set body "+( (int)returnBounds.x+UIBatch.edgeSize) );
		data.ints.set(SCALE, scale);
		JPhysicsEngine.Vector3Pool.free(returnBounds);
		//Gdx.app.log("cdraw", "textButton getBounds"+(returnBounds.x+UIBatch.edgeSize ));
	}
	
	public void draw(DrawUIMessage m, BusData data, Entity e){
		IntArray 
		
		 body = data.siblings.get(s_body).ints
		;
		Vector3 pos = (Vector3) data.siblings.get(s_position).object;
		int style = data.ints.get(STYLE), colorID = data.ints.get(COLOR_ID)
		, textColorID = data.ints.get(TEXT_COLOR_ID), fontID = data.ints.get(FONT_ID)
		;
		int width = body.get(CBody.BODY_WIDTH)
				, height = body.get(CBody.BODY_HEIGHT)
				, scale = data.ints.get(SCALE) 
		;
		
		String string = Data.getString(data.ints.get(STRING_HASH));
		 
		//Gdx.app.log("cdraw", "draw text box " + pos.x+pos.y+" w "+width+" h "+height+scale+string);
		m.batch.drawTextButton(pos.x, pos.y, width, height, scale, string, style, colorID, textColorID, fontID);
		//m.batch.drawButton(pos.get(CPosition.X), pos.get(CPosition.Y), width, height, style, 8, false);
		// m.batch.partialCircle(0,0,.051135f, .9f, Color.RED);
		//Gdx.app.log("cdraw", "draw text box " + width);
	}
}
