package com.niz.components;

import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.utils.IntArray;
import com.niz.Component;
import com.niz.Entity;
import com.niz.threading.Messages.TouchDownMessage;

public class Setting extends Component{
	public String prefName;
public int value;
//public String name;
public String[] names;
public static Preferences prefs;
public final int P_VAL = 0;
public Setting(String name, String pn, int val, String... strings){
	super(name);
	names = strings;
	prefName = pn;
	//value = val;
	getVal();
}

void getVal() {
	value = prefs.getInteger(prefName);
}
private void saveVal(int value){
	prefs.putInteger(prefName, value);
	prefs.flush();
}

public void inc(Entity e, IntArray data){
	// = e.getIntData(this);
	int value = data.get(P_VAL);
	value++;
	value %= names.length;
	data.set(P_VAL, value);
	this.value = value;
	saveVal(value);
}


////////////////////////////////////////////////////////////////////////////////

//EventListener settingIncrementer = new ClickListener(){
	
	public void clicked(TouchDownMessage m){
		
		inc(m.e);
		//setName();
	}
//};
//addListener(settingIncrementer);
//setting.getVal();
//setName();
//}

	public static void initPrefs(Preferences pref){
		prefs = pref;
	}


}
