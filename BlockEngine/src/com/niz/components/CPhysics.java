package com.niz.components;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.IntArray;
import com.niz.BusData;
import com.niz.Component;
import com.niz.Entity;
import com.niz.threading.Messages.PhysicsPostMessage;
import com.niz.threading.Messages.PhysicsPreMessage;

public class CPhysics extends Component {
	private static int s_position;
	public CPhysics() {
		super("Physics");
		
	}
	public static final int BODY_ID = 0, LAST_POS_X = 1, LAST_POS_Y = 2, A_X = 3, A_Y = 4;
	private static final String TAG = "cphysics";
	
	
	
	@Override
	public void establishSiblings(){
		//Object o = new Object();
		s_position = addSiblingHash(Components.cPosition.hash);
		
	}
	
	@Override 
	public void onSubscribe(Entity e, BusData data){
		int x = 0, y = 0;
		int id = e.plane.map.physics.addParticle(x, y);
		data.ints.set(BODY_ID, id);
		data.siblings.get(s_position).object = e.plane.map.physics.getPositionVector(id);
	}
	
	public void pre(PhysicsPreMessage m, BusData data, Entity e){//Entity e, PhysicsEngine physics){
		IntArray pos = data.siblings.get(s_position).ints;
		e.plane.map.physics.updateParticleData(pos, data.ints);
		//Gdx.app.log(TAG, "physics"+pos.get(0)+","+pos.get(1));
		if (e.plane.parent == e.plane){//root plane
			/*int offset = data.ints.get(CPosition.OFFSET);
			
			pos.set(CPosition.X, pos.get(CPosition.LOCAL_X)+ChunkOffset.x(offset));
			pos.set(CPosition.Y, pos.get(CPosition.LOCAL_Y)+ChunkOffset.y(offset));
			pos.set(CPosition.Z, pos.get(CPosition.LOCAL_Z)+ChunkOffset.z(offset));*/
		} else {
			
		}
		
	}
	
	public void post(PhysicsPostMessage m, BusData data, Entity e){
		IntArray a = data.ints, pos = data.siblings.get(s_position).ints;
		e.plane.map.physics.setParticleData(a.get(BODY_ID), pos.get(CPosition.X), pos.get(CPosition.Y), a.get(A_X), a.get(A_Y));
	}
	
}
