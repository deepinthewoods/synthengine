package com.niz;



import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.PixmapIO;
import com.badlogic.gdx.math.MathUtils;

public class TileGenerator {

	public void makeCementUneven(Pixmap pixmap, int xdivs, int ydivs, int tileSize){
		
	}
	
	public void makeCementEven(Pixmap pixmap, int xdivs, int ydivs, int tileSize, boolean touchx, boolean touchy, int c){
		//put extra pixels in middle. start from bottom
		int xSize = tileSize/xdivs, ySize = tileSize/ydivs;
		int yprogress = 0
				, xextras = tileSize % xdivs, yextras = tileSize % ydivs 
				;
		for (int y = 0; y < ydivs; y++){
			//horiz line
			//extras if middle
			boolean middley = (yextras != 0);
			middley = (y == ydivs/2 && y%(ydivs/2) == 0);
			
			int prevprogress = yprogress;
			//line
			for (int i = 0; i < tileSize; i++){
				pixmap.drawPixel(i, yprogress, c);
			}
			
			if (middley){
				//padding
				int start = MathUtils.random(0, tileSize-1);
				int length = MathUtils.random(0, tileSize-1);
				for (int i = start; i < tileSize && i < length; i++){
					pixmap.drawPixel(i,  yprogress+1, c);
				}
				
				yprogress += yextras;
				
				if (yextras > 1){
					start = MathUtils.random(0, tileSize-1);
					length = MathUtils.random(0, tileSize-1);
					for (int i = start; i < tileSize && i < length; i++){
						pixmap.drawPixel(i,  yprogress-1, c);
					}
				}
			}	
			yprogress += ySize;
			int xprogress = 0;
			if (touchx){
				xprogress += xSize;
			}
			for (int x = (xprogress==0?0:1); x < xdivs; x++){
				
				for (int ty = prevprogress; ty <= yprogress; ty++){
					pixmap.drawPixel(xprogress, ty, c);
					
				}
				
				boolean middlex = (xextras != 0 && x == xdivs/2 && x%xdivs == 0);
				if (middlex){
					xprogress += xextras;
				}
				//TODO fill in/randomize middle columns
				xprogress += xSize;
			}
		}
		
	}
	
	
	public Pixmap tmpPix = new Pixmap(256, 256, Format.RGB888);
	
	public void blurPixmap(Pixmap pixmap){
		int width = pixmap.getWidth(), height = pixmap.getHeight();;
		tmpPix.fillRectangle(0, 0, width+2, height+2);
		tmpPix.drawPixmap(pixmap, 1, 1);
		for (int x = 1; x < width+2; x++){
			
		}
	}
	public void clearPixmap(Pixmap pixmap){
		clearPixmap(pixmap, pixmap.getWidth(), pixmap.getHeight());
	}
	public void clearPixmap(Pixmap pixmap, int width, int height){
		
	}
	
	
	public void generateBricks(){
		int tileSize = 16, pixCount = 10;
		//blank
		Pixmap[] pixs = new Pixmap[pixCount];
		for (int i = 0; i < pixCount; i++){
			pixs[i] = new Pixmap(tileSize, tileSize, Format.RGBA8888);
		}
		for (int i = 0; i < pixCount; i++){
			this.makeCementEven(pixs[i]
					, 2//xdivs
					, 4//ydivs
					, tileSize, 
					false, false, 
					Color.BLACK.toIntBits()//c
					);
		}
		//add cement
		
		//save
		PixmapIO.writePNG(file, joinedPixmap);
		//blur with imagemagick
		
		//noise
		
		//limit colors/dither with imagemagick
		
		//maybe add back cement, blood drips, moss, gems inside, 
	}
	
	//dirt/grass
	
	//battlements
	
	//stairs, 2 gradients
	
	//half blocks
	
	//doorways
	
	
	
	/*
	 	stone stuff
	 	stairs, 2 steepnesses
	 	half blocks
	 	2x3 bricks, messy, 2 blurs
	 	2x4 bricks, neat, 1 blur?
	 	3x4 bricks, messy, 1 blur
	 	
	 	mossy, blood-spattered, etc.
	 	cobblestones
	 	gravel
	 	
	 	fence
	 	pole
	 	
	 	
	 	
	 	sand/sandstone - top face is cement color, bricks on side
	 	stone/cobblestone/gravel/stonebricks/sand
	 	
	 */
	
}
