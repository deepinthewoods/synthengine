package com.niz;

public class Draw {
	//will contain the list of Decals, or Meshes for 3d stuff or LayeredAnimations
	Model model;
	
	public void draw(Plane p, Entity e){
		model.draw(p, e);
	}
	
	
}
