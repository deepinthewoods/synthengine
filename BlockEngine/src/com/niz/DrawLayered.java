package com.niz;

import com.badlogic.gdx.utils.Array;
import com.niz.decals.Decal;

public class DrawLayered extends Draw{
	public Array<Decal> decals = new Array<Decal>();
	@Override
	public void draw(Plane p, Entity e){
		((LayeredModel)(model)).draw(p, e, decals);
	}
	
	
	
	//stages
	
	//layers: update when inv changes
	
	//colors: will be overwritten sometimes
	
	//frame: calculate ever tick.update decals on change else reposition
	
	
}
