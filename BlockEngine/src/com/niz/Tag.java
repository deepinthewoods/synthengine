package com.niz;

import com.badlogic.gdx.utils.IntArray;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;


public class Tag extends Component implements Json.Serializable{
	/*
	 * subject
	 * 
	 * tag or value
	 * 
	 * value (optional)
	 * 
	 * 
	 * tags
	 * 
	 * item abilities
	 * magic school
	 * artifact
	 	properties ie color, composition, visual effects/
	 	
	 	--memory stuff--
	  saw MOBTYPE at TIME COUNT (only store last and count)
	  saw MOB use ITEM successfully at TIME COUNT
	  saw ITEM used successfully COUNT
	  saw MOB attack MOB2 at TIME COUNT
	  
	  heard "ominous shuffling" in Deep Caverns Annex B TIME COUNT
	  
	  was hit by MOB for D damage TIME TOTAL
	  quaffed POTION / used ITEM  
	  
	  
	  
	  
	  for items
	  tags - fire, necromancy, dark magic
	  protects against / counters : warriors, cold,
	  symbiotic with :   
	  
	 */
	
	/*
	subject:item                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
	counters: fire
	symbiotic: cold, charge
	
	*/
	
	//Symbiotic:
	//Array<Tag> symbiotic;
	//counters:
	//Array<Tag> counters;
	
	
	//key-value pairs, sorted by key
	
	public Tag(String s) {
		super(s);
		
	}

	String key;
	IntArray values;
	
	@Override
	public void read(Json json, JsonValue jsonData) {
		key = jsonData.child().name();
		values = json.readValue(IntArray.class, jsonData);
	}
	
	@Override
	public void write(Json json) {
		json.writeValue(key, values, IntArray.class);
	}
	
	
	//
}
