package com.niz;

import java.io.DataInputStream;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector3;
import com.niz.blocks.Block;

public class Chunk {
	public static final int BLOCKS_TOTAL = 64*64;
	private static String TAG = "chunk";
	public boolean modified, saving, needsMesh;
	//public static final int SIZE = 64, MASK = 63, BITS = 6;
	public int xSize, ySize, zSize, xBits, yBits, zBits, xMask, yMask, zMask;
	private int[] blocks;
	private Plane plane;
	public int x, y, z;
	public Mesh[] mesh;
	private BlockPositionMap updates , BMUpdates;
	static float[] tmpVerts = new float[100000];
	int[] surrounds, surrounds6 = new int[6], surrounds4 = new int[4];
	
	public int getBlock(int x, int y, int z){
		x &= xMask;
		y &= yMask;
		z &= zMask;
		return blocks[x+(y<<xBits)+(z<<xBits<<yBits)];
	}
	public void setBlock(int x, int y, int z, int val){
		x &= xMask;
		y &= yMask;
		z &= zMask;
		int b = blocks[x+(y<<xBits)+(z<<xBits<<yBits)];
		if (b != val){
			modified = true;
			if (saving) cancelSave();
			blocks[x+(y<<xBits)+(z<<xBits<<yBits)] = val;
		}
		
	}
	private void cancelSave() {
		
		
	}
	private void startSave() {
		
		
	}
	private void updateSave() {
		
		
	}
	public boolean tickSave(){
		if (saving){
			updateSave();
			return false;
		}
		if (modified){
			startSave();
			return false;
		}
		return true;
	}
	
	public boolean isFinished() {
		return !saving && !modified && !needsMesh;
	}
	
	public synchronized boolean updateProcessing(){//returns true if finished
		if (needsMesh)
			calculate2dMesh(tmpVerts);
		
		
		return false;
	}
	
	private void calculate2dMesh(float[] verts) {
		int total = 0;
		int vertsPerPoint = 6;
		for (int bx = 0; bx < xSize; bx++)
			for (int by = 0; by < ySize; by++)
				for (int bz = 0; bz < zSize; bz++){
					int b = getBlock(bx, by, bz);
					if (Block.id(b) != 0){
						CorneredSprite s = Block.sprite(b);
						float[] v = s.getVertices();
						int verttotal = 0;
						for (int dx = 0; dx < 2; dx++)
						for (int dy = 0; dy < 2; dy++){
							verts[total++] = bx+dx;
							verts[total++] = by+dy;
							verts[total++] = bz;
							for (int i = 2; i < vertsPerPoint; i++)
								verts[total++] = v[i];
							
							
						}
						
					}
				}
		Gdx.app.log(TAG, "mesh");
		mesh[0].setVertices(verts, 0, total*vertsPerPoint);
		
	}
	
	public void setBitsXYZ(int x, int y, int z){
		xBits = x;
		yBits = y;
		zBits = z;
		xSize = 1<<xBits;
		xMask = xSize-1;
		ySize = 1<<yBits;
		yMask = ySize-1;
		zSize = 1<<zBits;
		zMask = zSize-1;
	}
	
	public void setID(int x, int y, int z, int val){
		setBlock(x,y,z, Block.setID(getBlock(x,y,z), val));
	}
	
	public void setMeta(int x, int y, int z, int val){
		setBlock(x,y,z, Block.setMeta(getBlock(x,y,z), val));
	}

	public void setLight(int x, int y, int z, int val){
		setBlock(x,y,z, Block.setLight(getBlock(x,y,z), val));
	}

	public void setDayLight(int x, int y, int z, int val){
		setBlock(x,y,z, Block.setDayLight(getBlock(x,y,z), val));
	}

	public void draw2d(SpriteBatch batch, Camera camera, int x1, int x2, int y1,
			int y2, int z1, int z2) {
		x1 = Math.max(x<<xBits, x1);
		x2 = Math.min(((x+1)<<xBits)-1, x2);
		y1 = Math.max(y<<yBits, y1);
		y2 = Math.min(((y+1)<<yBits)-1, y2);
		z1 = Math.max(z<<zBits, z1);
		z2 = Math.min(((z+1)<<zBits)-1, z2);
		
		for (int bx = x1; bx < x2; bx++)
			for (int by = y2; by < y1; by++){
				int block = blocks[bx+by<<xBits];
				Block.sprite(block).draw(batch);
			}
	}
	
	public boolean mesh(){//returns true if finished
		
		return false;
	}
	public boolean hasNoUpdates() {
		
		return updates.isEmpty() && BMUpdates.isEmpty();
	}
	public void setIDAll(int i) {
		for (int bx = 0; bx < xSize; bx++)
			for (int by = 0; by < ySize; by++)
				for (int bz = 0; bz < zSize; bz++){
					setID(bx, by, bz, i);
				}
		
	}
	
	public void setTestA() {
		for (int bx = 0; bx < xSize; bx++)
			for (int by = 0; by < ySize/2; by++)
				for (int bz = 0; bz < zSize; bz++){
					setID(bx, by, bz, 1);
				}
		
	}
	public int distanceFrom(Vector3 point) {
		return MathUtils.floor(Math.max(Math.max(point.x-x*xSize, point.y-y*ySize), point.z - z*zSize));
		
	}
	public int distanceFromCamera() {
		return distanceFrom(plane.getCamera().position);
	}
	public boolean needsUpdate() {
		
		return !hasNoUpdates();
	}
	public void onFree() {
		if (updates != null){
			updates.clear();
			updates.free();
			updates = null;
		}
		if (BMUpdates != null){
			BMUpdates.clear();
			BMUpdates.free();
			BMUpdates = null;
		}
	}
	public long getHash() {
		return getHash(x, y, z);
	}
	public static long getHash(int x, int y, int z) {
		return x+(y<<20)+(z<<40);
	}
	public boolean processLoad(DataInputStream stream) {
		//TODO
		
		return false;
	}
	
	public String chunkSavePath(StringBuilder builder) {
		builder.setLength(0);
		builder.append(plane.savePath);
		return builder.toString();
	}
	
	
	public int[] getBlockArray(){
		return blocks;
	}
	
	
}
