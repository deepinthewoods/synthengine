package com.niz;

import static com.badlogic.gdx.graphics.g2d.SpriteBatch.C1;
import static com.badlogic.gdx.graphics.g2d.SpriteBatch.C2;
import static com.badlogic.gdx.graphics.g2d.SpriteBatch.C3;
import static com.badlogic.gdx.graphics.g2d.SpriteBatch.C4;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.MathUtils;

public class CorneredSprite extends Sprite {
	static final int COLOR_TOTAL = 80;
public int[] cornerBits = new int[4];//start at bottom left
public float xOff, yOff, sizeY, sizeX;
public static final float pixelSize = 1f/16f;


public static float[] cachedColors = new float[16], cachedDayColors = new float[16], bgs = new float[16];
public static float[][][][] lightFades = new float[ColorData.MAX_COLORS][16][16][16];
public static int t, l = 0;
public static float[][] specialColors = new float[20][];


public CorneredSprite(com.badlogic.gdx.graphics.g2d.CorneredTextureAtlas.AtlasRegion region){
	this(region, 0, 0);
}
public CorneredSprite(com.badlogic.gdx.graphics.g2d.CorneredTextureAtlas.AtlasRegion region, float xx, float yy){
	super(region);
	
	sizeY = region.packedHeight*pixelSize;
	sizeX = region.packedWidth*pixelSize;
	xOff = region.offsetX*pixelSize-xx;
	yOff = yy-region.offsetY*pixelSize-sizeY;
	setSize(sizeX, sizeY);
	//setOrigin(xOff/2f, yOff/2f);
	setOrigin(0,0);
	//Gdx.app.log("cornsor�", ""+xOff);
}

public CorneredSprite(Texture tex, float sizeX, float sizeY, float xOff, float yOff, float u, float v, float u2, float v2){
	super(tex);
	this.setU(u);
	setV(v);
	setU2(u2);
	setV2(v2);
}

public CorneredSprite(CorneredSprite s) {
	super(s);
	sizeX = s.sizeX;
	sizeY = s.sizeY;
	xOff = s.xOff;
	yOff = s.yOff;
	setSize(sizeX, sizeY);
	setOrigin(0,0);
}






private static float blendsmall(Color Color1, Color Color2) {
	float r = (float) (1f - Math.sqrt(((1f-Color1.r)*(1f-Color1.r) + (1f-Color2.r)*(1f-Color2.r))/2));
	float g = (float) (1f - Math.sqrt(((1f-Color1.g)*(1f-Color1.g) + (1f-Color2.g)*(1f-Color2.g))/2));;
	float b = (float) (1f - Math.sqrt(((1f-Color1.b)*(1f-Color1.g) + (1f-Color2.b)*(1f-Color2.b))/2));
	return Color.toFloatBits(r, g, b, 1f);
}

private static float blendAdd(Color tmpC, Color tmpCC) {
	float R = tmpC.r, G = tmpC.g, B = tmpC.b, A = tmpC.a, r = tmpCC.r, g = tmpCC.g, b = tmpCC.b, a = tmpCC.a;
	float ax = 1 - (1 - a) * (1 - A);
	float rx = r * a / ax + R * A * (1 - a) / ax;
	float gx = g * a / ax + G * A * (1 - a) / ax;
	float bx = b * a / ax + B * A * (1 - a) / ax;
	return Color.toFloatBits(rx, gx, bx, ax);
}

public void setCorners(int bits){
	//bits &= (-1-2-4-8-16-32-64-128);
	//Gdx.app.log("spritecornered", "set corners: "+bits);
	float[] v = getVertices();
	v[C1] = cachedColors[bits & 0x0f];
    v[C2] = cachedColors[bits >>> 4 & 0x0f];
    v[C3] = cachedColors[bits >>> 8 & 0x0f];
    v[C4] = cachedColors[bits >>> 12 & 0x0f];
}



public CorneredSprite setCorners(int bits, int daybits){
	//setCorners(daybits);
	float[] v = getVertices();
	v[C1] = lightFades[t][l][bits & 0x0f][ daybits & 0x0f];
    v[C2] = lightFades[t][l][bits >>> 4 & 0x0f][ daybits >>> 4 & 0x0f];
    v[C3] = lightFades[t][l][bits >>> 8 & 0x0f][ daybits >>> 8 & 0x0f];
    v[C4] = lightFades[t][l][bits >>> 12 & 0x0f][ daybits >>> 12 & 0x0f];
    return this;
}





public void setBackgroundCorners(int bits, int daybits) {
	float[] v = getVertices();
	v[C1] = bgs[Math.max(bits & 0x0f, daybits & 0x0f)];
    v[C2] = bgs[Math.max(bits >>> 4 & 0x0f, daybits >>> 4 & 0x0f)];
    v[C3] = bgs[Math.max(bits >>> 8 & 0x0f, daybits >>> 8 & 0x0f)];
    v[C4] = bgs[Math.max(bits >>> 12 & 0x0f, daybits >>> 12 & 0x0f)];
	
}

public void setCornersSimple(byte li, byte dl) {
	float[] v = getVertices();
	v[C1] = lightFades[0][l][li][dl];
    v[C2] = lightFades[0][l][li][dl];
    v[C3] = lightFades[0][l][li][dl];
    v[C4] = lightFades[0][l][li][dl];
	
}

public void setColorID(int color, int li, int dl, int index) {
	if (color < 0){
		setSpecialColorID(color, index);
		return;
	}
	float[] v = getVertices();
	v[C1] = lightFades[color][l][li][dl];
    v[C2] = lightFades[color][l][li][dl];
    v[C3] = lightFades[color][l][li][dl];
    v[C4] = lightFades[color][l][li][dl];
	//setColor(lightFades[color][l][li][dl]);
	//this.setColor(Color.ORANGE);
}

private void setSpecialColorID(int color, int index) {
	color = -color;
	byte li = 15, dl = 15;
	int inc = 0;
	switch (color){
	default:
	case 1:
	{
		inc = index*12+accum16;
		inc %= specialColors[color].length;
		float[] v = getVertices();
		v[C1] = specialColors[color][inc];
	    v[C2] = specialColors[color][inc];
	    v[C3] = specialColors[color][inc];
	    v[C4] = specialColors[color][inc];
	}
		break;
	case 2:
	{
			inc = index+accum30;
			inc %= specialColors[color].length;
			float[] v = getVertices();
			v[C1] = specialColors[color][inc];
		    v[C2] = specialColors[color][inc];
		    v[C3] = specialColors[color][inc];
		    v[C4] = specialColors[color][inc];
	}
		break;
	case 3:
	{
			inc = index*9+accum30;
			inc %= specialColors[color].length;
			int inc2 = (13+ inc)%specialColors[color].length,
					inc3 = (36+ inc)%specialColors[color].length,
							inc4 = (49+ inc)%specialColors[color].length;
			float[] v = getVertices();
			v[C1] = specialColors[color][inc];
		    v[C2] = specialColors[color][inc2];
		    v[C3] = specialColors[color][inc3];
		    v[C4] = specialColors[color][inc4];
	}
		break;
	}
	
	
}

}
