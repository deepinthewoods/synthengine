package com.niz;



import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.IntArray;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.Pool;
import com.badlogic.gdx.utils.reflect.ClassReflection;
import com.badlogic.gdx.utils.reflect.Method;
import com.badlogic.gdx.utils.reflect.ReflectionException;

public class CopyOfBus {
private ObjectMap<Class<?>, Array<Method>> map = new ObjectMap<Class<?>, Array<Method>>();
private ObjectMap<Class<?>, Array<Object>> components = new ObjectMap<Class<?>, Array<Object>>();

//link components to intarrays
private ObjectMap<Object, Array<IntArray>> dataInt = new ObjectMap<Object,  Array<IntArray>>();



	public void publish(BaseMessage message){
		Class<?> cl = message.getClass();
		Array<Method> m = map.get(cl);
		Array<Object> o = components.get(cl);
		Array<IntArray> data = dataInt.get(cl);
		try 
		{
			for (int i = 0; i < m.size; i++){
					m.get(i).invoke(o.get(i), message, data.get(i));
			}
				
		}  catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (ReflectionException ex){
			
		}
			
		//}
	}
	
	public void unsubscribe(Component c){
		Method[] m = ClassReflection.getDeclaredMethods(c.getClass());//c.getClass().getDeclaredMethods();
		
		for (int i = 0; i < m.length; i++){
			Class<?>[] params = m[i].getParameterTypes();
			if (params.length == 1){
				if (params[0].isInstance(BaseMessage.class)){
					remove(params[0], m[i], c);
				}
			}
		}
	}
	public void subscribe(Component c){
		subscribe(c, dataPool.obtain());
	}
	//public static BaseMessage baseMessage = new BaseMessage("com.niz.Bus.BaseMessage");
	public void subscribe(Component c, IntArray data){
		Method[] m = ClassReflection.getDeclaredMethods(c.getClass());
		//c.getClass().getDeclaredMethods();
		for (int i = 0; i < m.length; i++){
			Class<?>[] params = m[i].getParameterTypes();
			if (params.length == 2){
				if (params[0].isInstance(BaseMessage.class) && params[1].isInstance(IntArray.class)){
					add(params[0], m[i], c, data);
				}
			}
		}
	}
	
	private void remove(Class<?> class1, Method method, Object c) {
		free(map.remove(class1));
		freeo(components.remove(class1));
		
	}
	
	private void freeo(Array<Object> remove) {
		remove.clear();
		objectArrayPool.free(remove);
		
	}

	private void free(Array<Method> remove) {
		remove.clear();
		methodArrayPool.free(remove);
		
	}

	private void add(Class<?> messageClass, Method method, Object c, IntArray data) {
		if (map.containsKey(messageClass)){
			
		}else {
			map.put(messageClass, getMethodArray());
			components.put(messageClass, getObjectArray());
			dataInt.put(messageClass, getIntArray());
		}
		map.get(messageClass).add(method);
		components.get(messageClass).add(c);
		dataInt.get(messageClass).add(data);
	}
	private Array<IntArray> getIntArray() {
		return dataArrayPool.obtain();
	}
	private static class objectArrayPool extends Pool<Array<Object>>{

		@Override
		protected Array<Object> newObject() {
			return new Array<Object>();
		}
		
	}
	
	private static class methodArrayPool extends Pool<Array<Method>>{

		@Override
		protected Array<Method> newObject() {
			return new Array<Method>();
		}
		
	}
	private static class DataArrayPool extends Pool<Array<IntArray>>{

		@Override
		protected Array<IntArray> newObject() {
			return new Array<IntArray>();
		}
		
	}
	
	private static objectArrayPool objectArrayPool = new objectArrayPool();
	private static methodArrayPool methodArrayPool = new methodArrayPool();
	private static DataArrayPool dataArrayPool = new DataArrayPool();

	IntPool dataPool = new IntPool();

	private Array<Object> getObjectArray() {
		return objectArrayPool.obtain();
	}

	private Array<Method> getMethodArray() {
		return methodArrayPool.obtain();
	}

	public static class BaseMessage{
		//String className;
		//Class cl;
		/*public BaseMessage(String className){
			try {
				cl = ClassReflection.forName(className);
			} catch (ReflectionException e) {
				
				e.printStackTrace();
			}
		}*/
	}

	public IntArray getIntData(Component c) {
		return dataInt.get(c.getClass())
	}


}
