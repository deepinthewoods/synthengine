package com.niz.verlet;

import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.IntArray;

public interface IPhysics {
	public void updateParticleData(IntArray pos, IntArray phys);
	public void setParticleData(int id, int x, int y, int fx, int fy);
	public void step();
	public int addParticle(int x, int y);
	public Vector3 getPositionVector(int id);
	
}
