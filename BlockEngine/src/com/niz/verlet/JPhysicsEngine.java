package com.niz.verlet;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.IntArray;
import com.badlogic.gdx.utils.Pool;
import com.badlogic.gdx.utils.Pools;
import com.niz.components.CPhysics;
import com.niz.components.CPosition;

public class JPhysicsEngine implements IPhysics{
	private static final int MAX_PARTICLES = 1000;
	private static final String TAG = "JPhysicsEngine";
	public static final int COLLISION_GRANULARITY = 16;
	public Vector3[] particles = new Vector3[MAX_PARTICLES*3];
	private Vector3 temp = new Vector3(), gravity = new Vector3(0,-100f, 0);;
	private int particleTotal = 0;
	private float fTimeStep =  (1f/120f ), fTimeStep2 = fTimeStep*fTimeStep;//Vector3.fixedMul(fTimeStep, fTimeStep);
	public JPhysicsEngine() {
		
		
	}
	float xLimit1 = -1, xLimit2 = 1, yLimit1 = -200.5f, yLimit2 = 1;
	
	// Verlet integration step
	void Verlet() {
		for(int i=0; i<particleTotal; i++) {
			Vector3 x = particles[i*4];
			temp.set(x);
			Vector3 oldx = particles[i*4+1];
			Vector3 a = particles[i*4+2];
			
			//x += x-oldx+a*fTimeStep*fTimeStep;
			x.sub(oldx);
			//Gdx.app.log(TAG, "sub "+x.toFloatString() + "   "+x);
			x.add(a.scl(fTimeStep).scl(fTimeStep)).add(temp);
			//Gdx.app.log(TAG, "test "+ x.toFloatString() + "   timestep"+fTimeStep + "   vecmul" + Vector3.fixedMul(fTimeStep, 100) + "  " + 1f/120f);
			oldx.set(temp);
		}
	}
	// This function should accumulate forces for each particle
	void AccumulateForces()
	{
		// All particles are influenced by gravity
		for(int i=0; i<particleTotal; i++) 
			particles[i*4+2].set( gravity );
	}
	// Here constraints should be satisfied
	void SatisfyConstraints() {
		for(int i=0; i<particleTotal; i++) {
			Vector3 pos = particles[i*4], prev = particles[1*4+1];
			pos.x = Math.min(Math.max(pos.x,  xLimit1), xLimit2);
			
			if (pos.y < yLimit1){
				//temp.set(pos);
				pos.y = yLimit1;
				//prev.set(temp);
			}
			//pos.y = Math.min(Math.max(pos.y,  yLimit1), yLimit2);
		}
	}

	
	
	

	@Override
	public void updateParticleData(IntArray pos, IntArray phys) {
		int id = phys.get(CPhysics.BODY_ID);
		Vector3 posV = particles[id*4];//, prev = particles[id*4+1];
		pos.set(CPosition.X, MathUtils.floor(posV.x*COLLISION_GRANULARITY));
		pos.set(CPosition.Y, MathUtils.floor(posV.y*COLLISION_GRANULARITY));
		//phys.set(CPhysics.LAST_POS_X, (int)(prev.x*PhysicsEngine.ONE));
		//phys.set(CPhysics.LAST_POS_Y, (int)(prev.y*PhysicsEngine.ONE));
		//phys.set(CPhysics.A_X, 0);
		//phys.set(CPhysics.A_Y, 0);
	}

	public void collide(int side, int depth){
		switch (side){
		case 0:
		}
	}
	
	@Override
	public void setParticleData(int id, int x, int y, int fx, int fy) {
		//Vector3 posV = particles[id*4], prev = particles[id*4+1];
		//posV.x = x/(float)PhysicsEngine.ONE;
		//posV.y = y/(float)PhysicsEngine.ONE;
		//prev.x = fx/(float)PhysicsEngine.ONE;;
		//prev.y = fy/(float)PhysicsEngine.ONE;;
		
	}

	@Override
	public void step() {
		AccumulateForces();
		Verlet();
		SatisfyConstraints();
		
	}
	
	public static Pool<Vector3> Vector3Pool = Pools.get(Vector3.class);
	@Override
	public synchronized int addParticle(int x, int y) {
		int id = particleTotal;
		particles[id*4] = Vector3Pool.obtain().set(x,y,0);
		particles[id*4+1] = Vector3Pool.obtain().set(x,y,0);
		particles[id*4+2] = Vector3Pool.obtain();
		particles[id*4+3] = Vector3Pool.obtain();
		
		particleTotal++;
		return id;
	}
	@Override
	public Vector3 getPositionVector(int id) {
		return particles[id*4];
	}

}
