package com.niz;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

public class Main {
	public static void main(String[] args) {
		LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
		cfg.title = "BlockEngine";
		cfg.useGL20 = true;
		cfg.width = 640	;
		cfg.height = 480;
		int cores = Runtime.getRuntime().availableProcessors();
		new LwjglApplication(new SynthEngine(cores), cfg);
	}
}
